import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { environment } from '../environments/environment';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { EmailParametros } from '../models/emailparametros.model';
import { EmailContactenos } from '../models/emailparametros.model';

@Injectable({
    providedIn: 'root'
})

export class EmailService {
    private url: string;
    private token: string;
    
    public headers: any;

    constructor(private httpClient: HttpClient) {
        this.url = environment.apiUrlGestor;
        this.token = localStorage.getItem('token') ?? '';

        this.headers = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                Accept: 'application/json',
                'Authorization': `Bearer ${this.token}`
            })
        };
    }

    EnviarEmail(emailParametros: EmailParametros): Observable<any> {
        return this.httpClient.post<any>(`${this.url}/email/enviarEmail`, emailParametros, this.headers).pipe(
            catchError((error: HttpErrorResponse) => {
                console.error('Error en EnviarEmail:', error);
                return throwError(() => new Error(error.error?.message || 'Error al enviar email'));
            })
        );
    }/*

    EnviarEmailRecuperacionContrasena(email: string): Observable<any> {
        debugger;
        return this.httpClient.post<any>(`${this.url}/email/enviarEmailRecuperacionContrasena`, email, this.headers).pipe(
            catchError((error: HttpErrorResponse) => {
                console.error('Error en EnviarEmail:', error);
                return throwError(() => new Error(error.error?.message || 'Error al enviar email'));
            })
        );
    }
*/




    
    EnviarEmailRecuperacionContrasena(emailParametros: EmailContactenos): Observable<EmailResponse> {
    return this.httpClient.post<EmailResponse>(`${this.url}/email/enviarEmailRecuperacionContrasena?token=${this.token}`, emailParametros)
        .pipe(
            map((response: EmailResponse) => {
                debugger;
                // Validar si la operación fue exitosa
                if (response.OperacionExitosa) {
              
                    console.log("Respuesta exitosa:", response);
                } else {
                    console.warn("Operación fallida:", response.Mensaje);
                }
                return response; // Retorna la respuesta completa para que el componente la use
            }),
            catchError((error: HttpErrorResponse) => {
                // Manejo de errores
                let errorMsg = 'Error desconocido';
                if (error.error instanceof ErrorEvent) {
                    errorMsg = `Error del cliente: ${error.error.message}`;
                } else {
                    errorMsg = `Error del servidor: ${error.message}`;
                }
                console.error("Error capturado en el servicio:", errorMsg);
                return throwError(() => new Error(errorMsg));
            })
        );
}

    EnviarEmailActivacionCuenta(emailParametros: EmailParametros): Observable<any> {
        return this.httpClient.post<any>(`${this.url}/email/enviarEmailActivarCuenta`, emailParametros, this.headers).pipe(
            catchError((error: HttpErrorResponse) => {
                console.error('Error en EnviarEmailActivacionCuenta:', error);
                return throwError(() => new Error(error.error?.message || 'Error al enviar email de activación de cuenta'));
            })
        );
    }

    
}

export interface EmailResponse {
    OperacionExitosa: boolean;
    Mensaje: string;
  }
