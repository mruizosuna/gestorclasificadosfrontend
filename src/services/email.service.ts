import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { environment } from '../environments/environment';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { EmailParametros } from '../models/emailparametros.model';

@Injectable({
    providedIn: 'root'
})

export class EmailService {
    private url: string;
    private token: string;
    
    public headers: any;

    constructor(private httpClient: HttpClient) {
        this.url = environment.apiUrlGestor;
        this.token = localStorage.getItem('token') ?? '';

        this.headers = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                Accept: 'application/json',
                'Authorization': `Bearer ${this.token}`
            })
        };
    }

    EnviarEmail(emailParametros: EmailParametros): Observable<any> {
        return this.httpClient.post<any>(`${this.url}/email/enviarEmail`, emailParametros, this.headers).pipe(
            catchError((error: HttpErrorResponse) => {
                console.error('Error en EnviarEmail:', error);
                return throwError(() => new Error(error.error?.message || 'Error al enviar email'));
            })
        );
    }

    EnviarEmailActivacionCuenta(emailParametros: EmailParametros): Observable<any> {
        return this.httpClient.post<any>(`${this.url}/email/enviarEmailActivarCuenta`, emailParametros, this.headers).pipe(
            catchError((error: HttpErrorResponse) => {
                console.error('Error en EnviarEmailActivacionCuenta:', error);
                return throwError(() => new Error(error.error?.message || 'Error al enviar email de activación de cuenta'));
            })
        );
    }
}
