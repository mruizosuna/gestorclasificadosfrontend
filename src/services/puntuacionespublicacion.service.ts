import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { environment } from '../environments/environment';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Publicaciones } from '../models/publicaciones.model';
import { PuntuacionVendedor } from '../models/puntuacionvendedor.model';

@Injectable({
    providedIn: 'root'
})
export class PuntuacionesPublicacionService {
    private url: string;
    private token: string;
    
    public headers: any;

    constructor(private httpClient: HttpClient) {
        this.url = environment.apiUrlGestor;
        this.token = localStorage.getItem('token') ?? '';

        this.headers = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                Accept: 'application/json',
                'Authorization': `Bearer ${this.token}`
            })
        };
    }

    GetPuntuacionPublicacionesById(id: number, usuarioId: number): Observable<any> {
        return this.httpClient.get(`${this.url}/puntuaciones/getPuntuacionesClasificadoById?id=${id}&usuarioIdComento=${usuarioId}`, this.headers)
            .pipe(catchError(this.handleError));
    }

    GetListaPuntuacionPublicacion(id: number): Observable<any> {
        return this.httpClient.get(`${this.url}/puntuaciones/getListaPuntuacionesClasificado?id=${id}`, this.headers)
            .pipe(catchError(this.handleError));
    }

    PostAddPuntuacionPublicacion(puntuacionesClasificado: any): Observable<any> {
        return this.httpClient.post<any>(`${this.url}/puntuaciones/addPuntuacionesClasificado`, puntuacionesClasificado, this.headers)
            .pipe(catchError(this.handleError));
    }

    PutUpdPuntuacionPublicacion(puntuacionesClasificado: any): Observable<any> {
        return this.httpClient.post<any>(`${this.url}/puntuaciones/updPuntuacionesClasificado`, puntuacionesClasificado, this.headers)
            .pipe(catchError(this.handleError));
    }

    GetPuntuacionVendedorById(id: number, usuarioId: number): Observable<any> {
        return this.httpClient.get(`${this.url}/puntuaciones/getPuntuacionesVendedorById?id=${id}&usuarioIdComento=${usuarioId}`, this.headers)
            .pipe(catchError(this.handleError));
    }

    GetListaPuntuacionVendedor(id: number): Observable<any> {
        return this.httpClient.get(`${this.url}/puntuaciones/getListaPuntuacionesVendedor?id=${id}`, this.headers)
            .pipe(catchError(this.handleError));
    }

    PostAddPuntuacionVendedor(puntuacionesVendedor: any): Observable<any> {
        return this.httpClient.post<any>(`${this.url}/puntuaciones/addPuntuacionesVendedor`, puntuacionesVendedor, this.headers)
            .pipe(catchError(this.handleError));
    }

    PutUpdPuntuacionVendedor(puntuacionesVendedor: any): Observable<any> {
        return this.httpClient.post<any>(`${this.url}/puntuaciones/updPuntuacionesVendedor`, puntuacionesVendedor, this.headers)
            .pipe(catchError(this.handleError));
    }

    private handleError(error: HttpErrorResponse) {
        console.error('Error en la solicitud:', error);
        return throwError(() => new Error(error.error?.message || 'Error en la operación solicitada'));
    }
}
