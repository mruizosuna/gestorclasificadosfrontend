import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { environment } from '../environments/environment';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class ZonasService {
    private url: string;
    private token: string;
    public headers: any;

    constructor(private httpClient: HttpClient) {
        this.url = environment.apiUrlGestor;
        this.token = localStorage.getItem('token') ?? '';

        this.headers = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                Accept: 'application/json',
                Authorization: `Bearer ${this.token}`
            })
        };
    }

    PutZonaById(zona: any): Observable<any> {
        return this.httpClient.post<any>(`${this.url}/parametricas/updZonasLocalidad`, zona, this.headers)
            .pipe(catchError(this.handleError));
    }

    PostZona(zona: any): Observable<any> {
        return this.httpClient.post<any>(`${this.url}/parametricas/addZonasLocalidad`, zona, this.headers)
            .pipe(catchError(this.handleError));
    }

    GetZonaById(id: number): Observable<any> {
        return this.httpClient.get(`${this.url}/parametricas/getZonasLocalidadById?id=${id}`, this.headers)
            .pipe(catchError(this.handleError));
    }

    GetListaZonas(): Observable<any> {
        return this.httpClient.get(`${this.url}/parametricas/getListaZonasLocalidad`, this.headers)
            .pipe(catchError(this.handleError));
    }

    DelZonaById(id: any): Observable<any> {
        return this.httpClient.get<any>(`${this.url}/parametricas/delZonasLocalidad?id=${id}`, this.headers)
            .pipe(catchError(this.handleError));
    }

    private handleError(error: HttpErrorResponse) {
        console.error('Error en la solicitud:', error);
        return throwError(() => new Error(error.error?.message || 'Error en la operación solicitada'));
    }
}
