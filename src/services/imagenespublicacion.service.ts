import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { environment } from '../environments/environment';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ImagenesPublicacion } from '../models/imagenespublicacion.model';

@Injectable({
    providedIn: 'root'
})

export class ImagenesPublicacionService {
    private url: string;
    private token: string;
    
    public headers: any;

    constructor(private httpClient: HttpClient) {
        this.url = environment.apiUrlGestor;
        this.token = localStorage.getItem('token') ?? '';

        this.headers = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                Accept: 'application/json',
                'Authorization': `Bearer ${this.token}`
            })
        };
    }

    GetImagenesPublicacionesById(id: number): Observable<any> {
        return this.httpClient.get(`${this.url}/publicaciones/getImagenesPublicacionById?id=${id}`, this.headers).pipe(
            catchError((error: HttpErrorResponse) => {
                console.error('Error en GetImagenesPublicacionesById:', error);
                return throwError(() => new Error(error.error?.message || 'Error al obtener imágenes de publicación por ID'));
            })
        );
    }

    GetListaImagenesPublicacion(id: number): Observable<any> {
        return this.httpClient.get(`${this.url}/publicaciones/getListaImagenesPublicacion?id=${id}`, this.headers).pipe(
            catchError((error: HttpErrorResponse) => {
                console.error('Error en GetListaImagenesPublicacion:', error);
                return throwError(() => new Error(error.error?.message || 'Error al obtener lista de imágenes de publicación'));
            })
        );
    }

    PostAddImagenesPublicacion(publicacion: ImagenesPublicacion): Observable<any> {
        return this.httpClient.post<any>(`${this.url}/publicaciones/addImagenesPublicacion`, publicacion, this.headers).pipe(
            catchError((error: HttpErrorResponse) => {
                console.error('Error en PostAddImagenesPublicacion:', error);
                return throwError(() => new Error(error.error?.message || 'Error al agregar imágenes de publicación'));
            })
        );
    }

    PutUpdImagenesPublicacion(publicacion: ImagenesPublicacion): Observable<any> {
        return this.httpClient.post<any>(`${this.url}/publicaciones/updImagenesPublicacion`, publicacion, this.headers).pipe(
            catchError((error: HttpErrorResponse) => {
                console.error('Error en PutUpdImagenesPublicacion:', error);
                return throwError(() => new Error(error.error?.message || 'Error al actualizar imágenes de publicación'));
            })
        );
    }

    DelImagenPublicacionById(id: any): Observable<any> {
        return this.httpClient.get<any>(`${this.url}/publicaciones/delImagenPublicacion?id=${id}`, this.headers).pipe(
            catchError((error: HttpErrorResponse) => {
                console.error('Error en DelImagenPublicacionById:', error);
                return throwError(() => new Error(error.error?.message || 'Error al eliminar imagen de publicación'));
            })
        );
    }
}
