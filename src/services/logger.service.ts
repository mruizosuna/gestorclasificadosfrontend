import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LoggerService {
//  private apiUrl = 'https://localhost:44381/publicaciones/guardarLog'; // URL del backend C#
  private apiUrl = 'https://backend.vecinoemprende.com/publicaciones/guardarLog'; // URL del backend C#


  constructor(private http: HttpClient) {}

  log(message: string): void {
    const logMessage = `[${new Date().toISOString()}] ${message}`;
    
    this.http.post(this.apiUrl, { log: logMessage }).subscribe({
      error: (err) => console.error('Error al guardar log:', err)
    });
  }
}
