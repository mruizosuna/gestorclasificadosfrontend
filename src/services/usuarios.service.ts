import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from '../environments/environment';
import { AutenticacionUsuario, UsuarioData, UsuarioLogin } from '../models/usuario.model';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class UsuarioService {
    private apiUrl = environment.apiUrlGestor;
    private token = localStorage.getItem('token') ?? '';
    private url: string;
    public headers: any;


    private httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json',
            Accept: 'application/json'
        })
    };

    constructor(private httpClient: HttpClient) {

        this.url = environment.apiUrlGestor;
        this.headers = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                Accept: 'application/json',
                Authorization: `Bearer ${this.token}`
            })
        };
    }

    private handleError(error: any) {
        console.error('Error en UsuarioService:', error);
        return throwError(() => new Error('Error en la petición, por favor intente nuevamente.'));
    }

    // Autenticación de usuario
    PostLoginUsuario(usuarioLogin: UsuarioLogin): Observable<UsuarioData> {
        usuarioLogin.token = 'qVZXZPB5oOvLEhDFgU2yL3iWrzfJ5yEbXnMYcwhzOHw=';
        return this.httpClient
            .post<UsuarioData>(`${this.apiUrl}/usuarios/getUsuariosByLogin`, usuarioLogin, this.httpOptions)
            .pipe(catchError(this.handleError));
    }

    
    //Creación de Usuario
    PostUsuarioGeneral(usuario: UsuarioData): Observable<any> {
        return this.httpClient.post<any>(this.url + '/usuarios/addUsuarios' ,usuario, this.headers)
        .pipe(catchError(this.handleError));
    }

  
    //Consulta de Disponibilidad por Id
    GetUsuarioGeneralById(id: number): Observable<any> {
        return this.httpClient.get(this.url + '/usuarios/getUsuariosById?id=' + id , this.headers)
        .pipe(catchError(this.handleError));
    }

    GetUsuarioGeneralByCorreo(correo: string): Observable<any> {
        return this.httpClient.get(this.url + '/usuarios/getUsuariosByCorreo?correo=' + correo , this.headers)
        .pipe(catchError(this.handleError));
     }
    //Actualización de Usuario
    PutUsuarioGeneralById(usuario: UsuarioData): Observable<any> {
        return this.httpClient.post<any>(this.url + '/usuarios/updUsuarios' , usuario, this.headers)
        .pipe(catchError(this.handleError));
    }
     //Lista de Usuarios por Perfil
     GetListaUsuarioGeneral(): Observable<any> {
        return this.httpClient.get(this.url + '/usuarios/getListaUsuarios', this.headers)
        .pipe(catchError(this.handleError));
    }
    //Consulta de Disponibilidad por Id
    DelUsuarioGeneralById(id: number): Observable<any> {
        return this.httpClient.get(this.url + '/usuarios/delUsuarios?id=' + id , this.headers)
        .pipe(catchError(this.handleError));
    }


    PutUsuarioContrasenaById(usuario: UsuarioData): Observable<any> {
        return this.httpClient.post<any>(this.url + '/usuarios/updUsuariosContrasena' , usuario, this.headers)
        .pipe(catchError(this.handleError));
    }
}
