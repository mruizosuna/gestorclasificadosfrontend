import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { environment } from '../environments/environment';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class ParametricasService {
    private url: string;
    private token: string;
    
    public headers: any;

    constructor(private httpClient: HttpClient) {
        this.url = environment.apiUrlGestor;
        this.token = localStorage.getItem('token') ?? '';

        this.headers = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                Accept: 'application/json',
                'Authorization': `Bearer ${this.token}`
            })
        };
    }

    GetListaParametrosById(tipo: string): Observable<any> {
        return this.httpClient.get(`${this.url}/parametricas/getParametrosById?tipo=${tipo}&token=${this.token}`, this.headers).pipe(
            catchError(this.handleError)
        );
    }

    GetListaLocalidad(): Observable<any> {
        return this.httpClient.get(`${this.url}/api/usuarios/api/v1/Localidad/`, this.headers).pipe(
            catchError(this.handleError)
        );
    }

    GetListaPerfil(): Observable<any> {
        return this.httpClient.get(`${this.url}/api/usuarios/api/v1/Perfil/`, this.headers).pipe(
            catchError(this.handleError)
        );
    }

    private handleError(error: HttpErrorResponse) {
        console.error('Error en la solicitud:', error);
        return throwError(() => new Error(error.error?.message || 'Error en la operación solicitada'));
    }
}
