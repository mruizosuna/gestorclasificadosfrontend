import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../environments/environment';
import { Injectable } from '@angular/core';
import { catchError, Observable, throwError } from 'rxjs';
import { DuplicacionPublicacion, Publicaciones } from '../models/publicaciones.model';

@Injectable({
    providedIn: 'root'
})
export class PublicacionesService {
    private url: string;
    private urlPayu: string;
    private token: string;

    constructor(private httpClient: HttpClient) {
        this.url = environment.apiUrlGestor;
        this.token = localStorage.getItem('token') ?? '';
    }

    private getHeaders(): HttpHeaders {
        const token = localStorage.getItem('token');
        return new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        });
    }

    PutPublicacionesById(publicacion: any): Observable<any> {
        return this.httpClient.post<any>(`${this.url}/publicaciones/updPublicaciones`, publicacion, { headers: this.getHeaders() }).pipe(
            catchError(error => throwError(() => new Error('Error al actualizar publicación')))
        );
    }

    PutPublicacionEstado(publicacion: any): Observable<any> {
        return this.httpClient.post<any>(`${this.url}/publicaciones/updPublicacioestado`, publicacion, { headers: this.getHeaders() }).pipe(
            catchError(error => throwError(() => new Error('Error al actualizar estado de publicación')))
        );
    }

    PostPublicaciones(publicacion: any): Observable<any> {
        return this.httpClient.post<any>(`${this.url}/publicaciones/addPublicaciones`, publicacion, { headers: this.getHeaders() }).pipe(
            catchError(error => throwError(() => new Error('Error al agregar publicación')))
        );
    }

    GetPublicacionesById(id: number): Observable<any> {
        return this.httpClient.get<any>(`${this.url}/publicaciones/getPublicacionesById?id=${id}`, { headers: this.getHeaders() }).pipe(
            catchError(error => throwError(() => new Error('Error al obtener publicación por ID')))
        );
    }

    GetListaPublicaciones(): Observable<any> {
        return this.httpClient.get<any>(`${this.url}/publicaciones/getListaPublicaciones`, { headers: this.getHeaders() }).pipe(
            catchError(error => throwError(() => new Error('Error al obtener lista de publicaciones')))
        );
    }

    GetListaPublicacionesByUser(id: number): Observable<any> {
        return this.httpClient.get<any>(`${this.url}/publicaciones/getListaPublicacionesByUser?id=${id}`, { headers: this.getHeaders() }).pipe(
            catchError(error => throwError(() => new Error('Error al obtener publicaciones por usuario')))
        );
    }

    DelPublicacionById(id: any): Observable<any> {
        return this.httpClient.get<any>(`${this.url}/publicaciones/delPublicacion?id=${id}`, { headers: this.getHeaders() }).pipe(
            catchError(error => throwError(() => new Error('Error al eliminar publicación')))
        );
    }

    GetListaBuscadorPublicaciones(categoriaid: number, subcategoriaid: number, zonalocalidadid: number, conjuntoid: number, palabrasclave: string, valordesde: number = 0, valorhasta: number = 0): Observable<any> {
        if (valordesde == undefined) valordesde = 0;
        if (valorhasta == undefined) valorhasta = 0;

        return this.httpClient.get<any>(`${this.url}/publicaciones/getListaBuscadorPublicacion?categoriaid=${categoriaid}&subcategoriaid=${subcategoriaid}&zonalocalidadid=${zonalocalidadid}&conjuntoid=${conjuntoid}&palabrasclave=${palabrasclave}&valordesde=${valordesde}&valorhasta=${valorhasta}`, { headers: this.getHeaders() }).pipe(
            catchError(error => throwError(() => new Error('Error en búsqueda de publicaciones')))
        );
    }

    GetListaBuscadorPublicacionesDestacadas(categoriaid: number, subcategoriaid: number, zonalocalidadid: number, conjuntoid: number, palabrasclave: string): Observable<any> {
        return this.httpClient.get<any>(`${this.url}/publicaciones/getListaBuscadorPublicacionDestacadas?categoriaid=${categoriaid}&subcategoriaid=${subcategoriaid}&zonalocalidadid=${zonalocalidadid}&conjuntoid=${conjuntoid}&palabrasclave=${palabrasclave}`, { headers: this.getHeaders() }).pipe(
            catchError(error => throwError(() => new Error('Error en búsqueda de publicaciones destacadas')))
        );
    }

    PostPagoPayu(datospago: any): Observable<any> {
        return this.httpClient.post<any>(this.urlPayu, datospago).pipe(
            catchError(error => throwError(() => new Error('Error en pago PayU')))
        );
    }

    PutVencimientosPublicacion(vecimiento: any): Observable<any> {
        return this.httpClient.post<any>(`${this.url}/publicaciones/updVencimientosPublicacion`, vecimiento, { headers: this.getHeaders() }).pipe(
            catchError(error => throwError(() => new Error('Error al actualizar vencimiento de publicación')))
        );
    }

    PutRenovacionesFinVigenciaPublicacion(renovacion: any): Observable<any> {
        return this.httpClient.post<any>(`${this.url}/publicaciones/updRenovacionesFinVigenciaPublicacion`, renovacion, { headers: this.getHeaders() }).pipe(
            catchError(error => throwError(() => new Error('Error al actualizar renovación de publicación')))
        );
    }

    AddDuplicacionPublicacion(publicacion: DuplicacionPublicacion): Observable<any> {
        return this.httpClient.post<any>(`${this.url}/publicaciones/addDuplicacionPublicacion`, publicacion, { headers: this.getHeaders() }).pipe(
            catchError(error => throwError(() => new Error('Error al duplicar publicación')))
        );
    }

    private encodeBase64(text: string): string {
        return btoa(unescape(encodeURIComponent(text)));
    }
}
