import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { environment } from '../environments/environment';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Publicaciones } from '../models/publicaciones.model';

@Injectable({
    providedIn: 'root'
})
export class VistasPublicacionService {
    private url: string;
    private token: string;
    public headers: any;

    constructor(private httpClient: HttpClient) {
        this.url = environment.apiUrlGestor;
        this.token = localStorage.getItem('token') ?? '';

        this.headers = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                Accept: 'application/json',
                'Authorization': `Bearer ${this.token}`
            })
        };
    }

    GetVistaPublicacionesById(UsuarioIDVio: number, PublicacionId: number): Observable<any> {
        return this.httpClient.get(`${this.url}/puntuaciones/getVistaClasificadoById?id=${PublicacionId}&usuarioIdVio=${UsuarioIDVio}`, this.headers)
            .pipe(catchError(this.handleError));
    }

    GetListaVistaPuntuacionPublicacion(id: number): Observable<any> {
        return this.httpClient.get(`${this.url}/puntuaciones/getListaVistaClasificado?id=${id}`, this.headers)
            .pipe(catchError(this.handleError));
    }

    PostAddVistaPublicacion(vistaClasificado: any): Observable<any> {
        return this.httpClient.post<any>(`${this.url}/puntuaciones/addVistaClasificado`, vistaClasificado, this.headers)
            .pipe(catchError(this.handleError));
    }

    PutUpdVistaPublicacion(vistaClasificado: any): Observable<any> {
        return this.httpClient.post<any>(`${this.url}/puntuaciones/updVistaClasificado`, vistaClasificado, this.headers)
            .pipe(catchError(this.handleError));
    }

    private handleError(error: HttpErrorResponse) {
        console.error('Error en la solicitud:', error);
        return throwError(() => new Error(error.error?.message || 'Error en la operación solicitada'));
    }
}
