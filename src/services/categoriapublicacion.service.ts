import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../environments/environment';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})

export class CategoriaPublicacionService {
    private url: string;
    private token: string;
    
    public headers: any;

    constructor(private httpClient: HttpClient) {
        this.url = environment.apiUrlGestor;
        this.token = localStorage.getItem('token') ?? '';

        this.headers = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                Accept: 'application/json',
                'Authorization': `Bearer ${this.token}`
            })
        };
    }

    GetListaCategoriaPublicacionesById(id: number): Observable<any> {
        return this.httpClient.get<any>(`${this.url}/publicaciones/getListaPublicacionSubCategoriasById?id=${id}`, this.headers).pipe(
            catchError(error => {
                console.error('Error en GetListaCategoriaPublicacionesById:', error);
                return throwError(() => new Error('Error al obtener lista de categorías de publicación por ID'));
            })
        );
    }

    GetListaCategoriaPublicacion(id: number): Observable<any> {
        return this.httpClient.get<any>(`${this.url}/publicaciones/getListaPublicacionSubCategorias?id=${id}`, this.headers).pipe(
            catchError(error => {
                console.error('Error en GetListaCategoriaPublicacion:', error);
                return throwError(() => new Error('Error al obtener lista de categorías de publicación'));
            })
        );
    }

    PostAddCategoriaPublicacion(publicacion: any): Observable<any> {
        return this.httpClient.post<any>(`${this.url}/publicaciones/addPublicacionSubCategorias`, publicacion, this.headers).pipe(
            catchError(error => {
                console.error('Error en PostAddCategoriaPublicacion:', error);
                return throwError(() => new Error('Error al agregar categoría de publicación'));
            })
        );
    }

    DelCategoriaPublicacionById(id: any): Observable<any> {
        return this.httpClient.get<any>(`${this.url}/publicaciones/delPublicacionSubCategorias?id=${id}`, this.headers).pipe(
            catchError(error => {
                console.error('Error en DelCategoriaPublicacionById:', error);
                return throwError(() => new Error('Error al eliminar categoría de publicación'));
            })
        );
    }
}
