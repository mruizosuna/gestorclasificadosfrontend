import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { environment } from '../environments/environment';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Publicaciones } from '../models/publicaciones.model';

@Injectable({
    providedIn: 'root'
})
export class VideosPublicacionService {
    private url: string;
    private token: string;
    public headers: any;

    constructor(private httpClient: HttpClient) {
        this.url = environment.apiUrlGestor;
        this.token = localStorage.getItem('token') ?? '';

        this.headers = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                Accept: 'application/json',
                'Authorization': `Bearer ${this.token}`
            })
        };
    }

    GetVideoPublicacionesById(id: number): Observable<any> {
        return this.httpClient.get(`${this.url}/publicaciones/getVideosPublicacionById?id=${id}`, this.headers)
            .pipe(catchError(this.handleError));
    }

    GetListaVideosPublicacion(id: number): Observable<any> {
        return this.httpClient.get(`${this.url}/publicaciones/getListaVideosPublicacion?id=${id}`, this.headers)
            .pipe(catchError(this.handleError));
    }

    GetListaVideosPublicacionById(id: number): Observable<any> {
        return this.httpClient.get(`${this.url}/publicaciones/getListaVideosPublicacionById?id=${id}`, this.headers)
            .pipe(catchError(this.handleError));
    }

    PostAddVideoPublicacion(publicacion: any): Observable<any> {
        return this.httpClient.post<any>(`${this.url}/publicaciones/addVideosPublicacion`, publicacion, this.headers)
            .pipe(catchError(this.handleError));
    }

    PutUpdVideoPublicacion(publicacion: any): Observable<any> {
        return this.httpClient.post<any>(`${this.url}/publicaciones/updVideosPublicacion`, publicacion, this.headers)
            .pipe(catchError(this.handleError));
    }

    DelVideoPublicacionById(id: any): Observable<any> {
        return this.httpClient.get<any>(`${this.url}/publicaciones/delVideoPublicacion?id=${id}`, this.headers)
            .pipe(catchError(this.handleError));
    }

    private handleError(error: HttpErrorResponse) {
        console.error('Error en la solicitud:', error);
        return throwError(() => new Error(error.error?.message || 'Error en la operación solicitada'));
    }
}
