import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../environments/environment';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})

export class CategoriasPrincipalesService {
    private url: string;
    private token: string;
    
    public headers: any;

    constructor(private httpClient: HttpClient) {
        this.url = environment.apiUrlGestor;
        this.token = localStorage.getItem('token') ?? '';

        this.headers = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                Accept: 'application/json',
                'Authorization': `Bearer ${this.token}`
            })
        };
    }

    PutCategoriasPrincipalesById(categoriaprincipal: any): Observable<any> {
        return this.httpClient.post<any>(`${this.url}/categorias/updCategoriasPrincipales`, categoriaprincipal, this.headers).pipe(
            catchError(error => {
                console.error('Error en PutCategoriasPrincipalesById:', error);
                return throwError(() => new Error('Error al actualizar categoría principal'));
            })
        );
    }

    PostCategoriasPrincipales(categoriaprincipal: any): Observable<any> {
        return this.httpClient.post<any>(`${this.url}/categorias/addCategoriasPrincipales`, categoriaprincipal, this.headers).pipe(
            catchError(error => {
                console.error('Error en PostCategoriasPrincipales:', error);
                return throwError(() => new Error('Error al agregar categoría principal'));
            })
        );
    }

    GetCategoriasPrincipalesById(id: number): Observable<any> {
        return this.httpClient.get<any>(`${this.url}/categorias/getCategoriasPrincipalesById?id=${id}`, this.headers).pipe(
            catchError(error => {
                console.error('Error en GetCategoriasPrincipalesById:', error);
                return throwError(() => new Error('Error al obtener categoría principal por ID'));
            })
        );
    }

    GetListaCategoriasPrincipales(): Observable<any> {
        return this.httpClient.get<any>(`${this.url}/categorias/getListaCategoriasPrincipales`, this.headers).pipe(
            catchError(error => {
                console.error('Error en GetListaCategoriasPrincipales:', error);
                return throwError(() => new Error('Error al obtener lista de categorías principales'));
            })
        );
    }
    
    DelCategoriasPrincipalesById(id: any): Observable<any> {
        return this.httpClient.get<any>(`${this.url}/categorias/delCategoriasPrincipales?id=${id}`, this.headers).pipe(
            catchError(error => {
                console.error('Error en DelCategoriasPrincipalesById:', error);
                return throwError(() => new Error('Error al eliminar categoría principal'));
            })
        );
    }
}
