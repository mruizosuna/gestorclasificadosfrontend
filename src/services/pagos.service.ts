import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { environment } from '../environments/environment';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class PagosService {
    private url: string;
    private token: string;
    
    public headers: any;

    constructor(private httpClient: HttpClient) {
        this.url = environment.apiUrlGestor;
        this.token = localStorage.getItem('token') ?? '';

        this.headers = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                Accept: 'application/json',
                'Authorization': `Bearer ${this.token}`
            })
        };
    }

    PutPagosById(pagos: any): Observable<any> {
        return this.httpClient.post<any>(`${this.url}/parametricas/updPagos`, pagos, this.headers).pipe(
            catchError(this.handleError)
        );
    }

    PostPagos(pagos: any): Observable<any> {
        return this.httpClient.post<any>(`${this.url}/parametricas/addPagos`, pagos, this.headers).pipe(
            catchError(this.handleError)
        );
    }

    GetPagosById(id: number): Observable<any> {
        return this.httpClient.get(`${this.url}/parametricas/getPagosById?id=${id}`, this.headers).pipe(
            catchError(this.handleError)
        );
    }

    GetListaPagos(): Observable<any> {
        return this.httpClient.get(`${this.url}/parametricas/getListaPagos`, this.headers).pipe(
            catchError(this.handleError)
        );
    }

    DelPagosById(id: any): Observable<any> {
        return this.httpClient.get<any>(`${this.url}/parametricas/delPagos?id=${id}`, this.headers).pipe(
            catchError(this.handleError)
        );
    }
    
    GetDescuentoById(id: string, userid: number): Observable<any> {
        return this.httpClient.get(`${this.url}/parametricas/getDescuentoById?id=${id}&userid=${userid}`, this.headers).pipe(
            catchError(this.handleError)
        );
    }

    GetDescuentoBienvenidaById(id: string, userid: number): Observable<any> {
        return this.httpClient.get(`${this.url}/parametricas/getDescuentoBienvenidaById?id=${id}&userid=${userid}`, this.headers).pipe(
            catchError(this.handleError)
        );
    }

    GetDescuentoBienvenidaByUser(userid: number): Observable<any> {
        return this.httpClient.get(`${this.url}/parametricas/getDescuentoBienvenidaByUser?userid=${userid}`, this.headers).pipe(
            catchError(this.handleError)
        );
    }
    
    PutDescuentoById(descuento: any): Observable<any> {
        return this.httpClient.post<any>(`${this.url}/parametricas/updDescuento`, descuento, this.headers).pipe(
            catchError(this.handleError)
        );
    }

    PutDescuentoBienvenidaById(descuento: any): Observable<any> {
        return this.httpClient.post<any>(`${this.url}/parametricas/updDescuentoBienvenida`, descuento, this.headers).pipe(
            catchError(this.handleError)
        );
    }
    
    private handleError(error: HttpErrorResponse) {
        console.error('Error en la solicitud:', error);
        return throwError(() => new Error(error.error?.message || 'Error en la operación solicitada'));
    }
}
