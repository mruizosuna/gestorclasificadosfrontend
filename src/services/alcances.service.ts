import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../environments/environment';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})

export class AlcancesService {
    private url: string;
    private token: string;

    public headers: any;

    constructor(private httpClient: HttpClient) {
        this.url = environment.apiUrlGestor;
        this.token = localStorage.getItem('token') ?? '';


        this.headers = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                Accept: 'application/json',
                'Authorization': `Bearer ${this.token}`
            })
        };
    }

    PutAlcancesById(alcnce: any): Observable<any> {
        return this.httpClient.post<any>(`${this.url}/parametricas/updAlcance`, alcnce, this.headers).pipe(
            catchError(error => {
                console.error('Error en PutAlcancesById:', error);
                return throwError(() => new Error('Error al actualizar alcance'));
            })
        );
    }

    PostAlcances(alcnce: any): Observable<any> {
        return this.httpClient.post<any>(`${this.url}/parametricas/addAlcance`, alcnce, this.headers).pipe(
            catchError(error => {
                console.error('Error en PostAlcances:', error);
                return throwError(() => new Error('Error al agregar alcance'));
            })
        );
    }

    GetAlcancesById(id: number): Observable<any> {
        return this.httpClient.get<any>(`${this.url}/parametricas/getAlcanceById?id=${id}`, this.headers).pipe(
            catchError(error => {
                console.error('Error en GetAlcancesById:', error);
                return throwError(() => new Error('Error al obtener alcance por ID'));
            })
        );
    }

    GetListaAlcances(): Observable<any> {
        return this.httpClient.get<any>(`${this.url}/parametricas/getListaAlcances`, this.headers).pipe(
            catchError(error => {
                console.error('Error en GetListaAlcances:', error);
                return throwError(() => new Error('Error al obtener lista de alcances'));
            })
        );
    }

    DelAlcancesById(id: any): Observable<any> {
        return this.httpClient.get<any>(`${this.url}/parametricas/delAlcance?id=${id}`, this.headers).pipe(
            catchError(error => {
                console.error('Error en DelAlcancesById:', error);
                return throwError(() => new Error('Error al eliminar alcance'));
            })
        );
    }
}
