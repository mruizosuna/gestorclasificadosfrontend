import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../environments/environment';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})

export class CategoriasSecundariasService {
    private url: string;
    private token: string;
    
    public headers: any;

    constructor(private httpClient: HttpClient) {
        this.url = environment.apiUrlGestor;
        this.token = localStorage.getItem('token') ?? '';
        
        this.headers = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                Accept: 'application/json',
                'Authorization': `Bearer ${this.token}`
            })
        };
    }

    PutCategoriasSecundariasById(categoriasecundaria: any): Observable<any> {
        return this.httpClient.post<any>(`${this.url}/categorias/updCategoriasSecundarias`, categoriasecundaria, this.headers).pipe(
            catchError(error => {
                console.error('Error en PutCategoriasSecundariasById:', error);
                return throwError(() => new Error('Error al actualizar categoría secundaria'));
            })
        );
    }

    PostCategoriasSecundarias(categoriasecundaria: any): Observable<any> {
        return this.httpClient.post<any>(`${this.url}/categorias/addCategoriasSecundarias`, categoriasecundaria, this.headers).pipe(
            catchError(error => {
                console.error('Error en PostCategoriasSecundarias:', error);
                return throwError(() => new Error('Error al agregar categoría secundaria'));
            })
        );
    }

    GetCategoriasSecundariasById(id: number): Observable<any> {
        return this.httpClient.get<any>(`${this.url}/categorias/getCategoriasSecundariasById?id=${id}`, this.headers).pipe(
            catchError(error => {
                console.error('Error en GetCategoriasSecundariasById:', error);
                return throwError(() => new Error('Error al obtener categoría secundaria por ID'));
            })
        );
    }

    GetListaCategoriasSecundarias(): Observable<any> {
        return this.httpClient.get<any>(`${this.url}/categorias/getListaCategoriasSecundarias`, this.headers).pipe(
            catchError(error => {
                console.error('Error en GetListaCategoriasSecundarias:', error);
                return throwError(() => new Error('Error al obtener lista de categorías secundarias'));
            })
        );
    }

    GetListaCategoriasSecundariasById(id: number): Observable<any> {
        return this.httpClient.get<any>(`${this.url}/categorias/getListaCategoriasSecundariasById?id=${id}`, this.headers).pipe(
            catchError(error => {
                console.error('Error en GetListaCategoriasSecundariasById:', error);
                return throwError(() => new Error('Error al obtener lista de categorías secundarias por ID'));
            })
        );
    }

    DelCategoriasSecundariasById(id: any): Observable<any> {
        return this.httpClient.get<any>(`${this.url}/categorias/delCategoriasSecundarias?id=${id}`, this.headers).pipe(
            catchError(error => {
                console.error('Error en DelCategoriasSecundariasById:', error);
                return throwError(() => new Error('Error al eliminar categoría secundaria'));
            })
        );
    }
}
