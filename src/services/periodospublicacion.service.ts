import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { environment } from '../environments/environment';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ListaPeriodosPublicacion } from '../models/periodospublicacion.model';

@Injectable({
    providedIn: 'root'
})
export class PeriodosPublicacionService {
    private url: string;
    private token: string;
    
    public headers: any;

    constructor(private httpClient: HttpClient) {
        this.url = environment.apiUrlGestor;
        this.token = localStorage.getItem('token') ?? '';

        this.headers = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                Accept: 'application/json',
                'Authorization': `Bearer ${this.token}`
            })
        };
    }

    PutPeriodosPublicacionById(periodo: any): Observable<any> {
        return this.httpClient.post<any>(`${this.url}/publicaciones/updPeriodosPublicacion`, periodo, this.headers).pipe(
            catchError(this.handleError)
        );
    }

    PostPeriodosPublicacion(periodo: any): Observable<any> {
        return this.httpClient.post<any>(`${this.url}/publicaciones/addPeriodosPublicacion`, periodo, this.headers).pipe(
            catchError(this.handleError)
        );
    }

    GetPeriodosPublicacionById(id: number): Observable<any> {
        return this.httpClient.get(`${this.url}/publicaciones/getPeriodosPublicacionById?id=${id}`, this.headers).pipe(
            catchError(this.handleError)
        );
    }

    GetListaPeriodosPublicacion(): Observable<any> {
        return this.httpClient.get<ListaPeriodosPublicacion>(
            `${this.url}/publicaciones/getListaPeriodosPublicacion`, this.headers)
                .pipe(catchError(this.handleError));
        }
    
    
    DelPeriodosPublicacionById(id: any): Observable<any> {
        return this.httpClient.get<any>(`${this.url}/publicaciones/delPeriodosPublicacion?id=${id}`, this.headers).pipe(
            catchError(this.handleError)
        );
    }

    private handleError(error: HttpErrorResponse) {
        console.error('Error en la solicitud:', error);
        return throwError(() => new Error(error.error?.message || 'Error en la operación solicitada'));
    }
}
