import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { environment } from '../environments/environment';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class ValorPublicacionService {
    private url: string;
    private token: string;
    public headers: any;

    constructor(private httpClient: HttpClient) {
        this.url = environment.apiUrlGestor;
        this.token = localStorage.getItem('token') ?? '';

        this.headers = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                Accept: 'application/json',
                'Authorization': `Bearer ${this.token}`
            })
        };
    }

    PutValorPublicacionById(valorpublicacion: any): Observable<any> {
        return this.httpClient.post<any>(`${this.url}/publicaciones/updValorPublicacion`, valorpublicacion, this.headers)
            .pipe(catchError(this.handleError));
    }

    PostValorPublicacion(valorpublicacion: any): Observable<any> {
        return this.httpClient.post<any>(`${this.url}/publicaciones/addValorPublicacion`, valorpublicacion, this.headers)
            .pipe(catchError(this.handleError));
    }

    GetValorPublicacionById(id: number): Observable<any> {
        return this.httpClient.get(`${this.url}/publicaciones/getValorPublicacionById?id=${id}`, this.headers)
            .pipe(catchError(this.handleError));
    }

    GetListaValorPublicacion(): Observable<any> {
        return this.httpClient.get(`${this.url}/publicaciones/getListaValorPublicacion`, this.headers)
            .pipe(catchError(this.handleError));
    }

    DelValorPublicacionById(id: any): Observable<any> {
        return this.httpClient.get<any>(`${this.url}/publicaciones/delValorPublicacion?id=${id}`, this.headers)
            .pipe(catchError(this.handleError));
    }

    private handleError(error: HttpErrorResponse) {
        console.error('Error en la solicitud:', error);
        return throwError(() => new Error(error.error?.message || 'Error en la operación solicitada'));
    }
}
