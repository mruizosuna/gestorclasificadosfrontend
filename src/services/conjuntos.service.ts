import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../environments/environment';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})

export class ConjuntosService {
    private url: string;
    private token: string;
    
    public headers: any;

    constructor(private httpClient: HttpClient) {
        this.url = environment.apiUrlGestor;
        this.token = localStorage.getItem('token') ?? '';

        this.headers = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                Accept: 'application/json',
                'Authorization': `Bearer ${this.token}`
            })
        };
    }

    PutConjuntosById(conjunto: any): Observable<any> {
        return this.httpClient.post<any>(`${this.url}/parametricas/updConjuntos`, conjunto, this.headers).pipe(
            catchError(error => {
                console.error('Error en PutConjuntosById:', error);
                return throwError(() => new Error('Error al actualizar conjunto por ID'));
            })
        );
    }

    PostConjuntos(conjunto: any): Observable<any> {
        return this.httpClient.post<any>(`${this.url}/parametricas/addConjuntos`, conjunto, this.headers).pipe(
            catchError(error => {
                console.error('Error en PostConjuntos:', error);
                return throwError(() => new Error('Error al agregar conjunto'));
            })
        );
    }

    GetConjuntosById(id: number): Observable<any> {
        return this.httpClient.get<any>(`${this.url}/parametricas/getConjuntosById?id=${id}`, this.headers).pipe(
            catchError(error => {
                console.error('Error en GetConjuntosById:', error);
                return throwError(() => new Error('Error al obtener conjunto por ID'));
            })
        );
    }

    GetListaConjuntos(): Observable<any> {
        return this.httpClient.get<any>(`${this.url}/parametricas/getListaConjuntos`, this.headers).pipe(
            catchError(error => {
                console.error('Error en GetListaConjuntos:', error);
                return throwError(() => new Error('Error al obtener lista de conjuntos'));
            })
        );
    }

    GetListaConjuntosById(id: number): Observable<any> {
        return this.httpClient.get<any>(`${this.url}/parametricas/getListaConjuntosById?id=${id}`, this.headers).pipe(
            catchError(error => {
                console.error('Error en GetListaConjuntosById:', error);
                return throwError(() => new Error('Error al obtener lista de conjuntos por ID'));
            })
        );
    }

    DelConjuntosById(id: any): Observable<any> {
        return this.httpClient.get<any>(`${this.url}/parametricas/delConjuntos?id=${id}`, this.headers).pipe(
            catchError(error => {
                console.error('Error en DelConjuntosById:', error);
                return throwError(() => new Error('Error al eliminar conjunto por ID'));
            })
        );
    }
}
