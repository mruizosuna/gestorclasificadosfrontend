import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { environment } from '../environments/environment';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ListaTipoPublicacion } from '../models/tipospublicacion.model';

@Injectable({
    providedIn: 'root'
})
export class TiposPublicacionService {
    private url: string;
    private token: string;
    public headers: any;

    constructor(private httpClient: HttpClient) {
        this.url = environment.apiUrlGestor;
        this.token = localStorage.getItem('token') ?? '';

        this.headers = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                Accept: 'application/json',
                Authorization: `Bearer ${this.token}`
            })
        };
    }

    // Actualizar tipo de publicación
    PutTiposPublicacionById(tipoPublicacion: any): Observable<any> {
        return this.httpClient.post<any>(`${this.url}/publicaciones/updTiposPublicacion`, tipoPublicacion, this.headers)
            .pipe(catchError(this.handleError));
    }

    // Crear nuevo tipo de publicación
    PostTiposPublicacion(tipoPublicacion: any): Observable<any> {
        return this.httpClient.post<any>(`${this.url}/publicaciones/addTiposPublicacion`, tipoPublicacion, this.headers)
            .pipe(catchError(this.handleError));
    }

    // Obtener tipo de publicación por ID
    GetTiposPublicacionById(id: number): Observable<any> {
        return this.httpClient.get<any>(`${this.url}/publicaciones/getTiposPublicacionById?id=${id}`, this.headers)
            .pipe(catchError(this.handleError));
    }

    // Obtener lista de tipos de publicación


    GetListaTiposPublicacion(): Observable<any> {
        return this.httpClient.get<ListaTipoPublicacion>(`${this.url}/publicaciones/getListaTiposPublicacion`, this.headers)
            .pipe(catchError(this.handleError));
    }

    
    // Eliminar tipo de publicación por ID
    DelTiposPublicacionById(id: number): Observable<any> {
        return this.httpClient.delete<any>(`${this.url}/publicaciones/delTiposPublicacion?id=${id}`, this.headers)
            .pipe(catchError(this.handleError));
    }

    // Manejo de errores
    private handleError(error: HttpErrorResponse) {
        console.error('Error en la solicitud:', error);
        return throwError(() => new Error(error.error?.message || 'Error en la operación solicitada'));
    }
}
