import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../environments/environment';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})

export class ConjuntoPublicacionService {
    private url: string;
    private token: string;
    
    public headers: any;

    constructor(private httpClient: HttpClient) {
        this.url = environment.apiUrlGestor;
        this.token = localStorage.getItem('token') ?? '';

        this.headers = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                Accept: 'application/json',
                'Authorization': `Bearer ${this.token}`
            })
        };
    }

    GetListaConjuntoPublicacionesById(id: number): Observable<any> {
        return this.httpClient.get<any>(`${this.url}/publicaciones/getListaPublicacionesConjuntosById?id=${id}`, this.headers).pipe(
            catchError(error => {
                console.error('Error en GetListaConjuntoPublicacionesById:', error);
                return throwError(() => new Error('Error al obtener lista de conjunto de publicaciones por ID'));
            })
        );
    }

    GetListaConjuntoPublicacion(id: number): Observable<any> {
        return this.httpClient.get<any>(`${this.url}/publicaciones/getListaPublicacionesConjuntos?id=${id}`, this.headers).pipe(
            catchError(error => {
                console.error('Error en GetListaConjuntoPublicacion:', error);
                return throwError(() => new Error('Error al obtener lista de conjunto de publicaciones'));
            })
        );
    }

    PostAddConjuntoPublicacion(publicacion: any): Observable<any> {
        return this.httpClient.post<any>(`${this.url}/publicaciones/addPublicacionesConjuntos`, publicacion, this.headers).pipe(
            catchError(error => {
                console.error('Error en PostAddConjuntoPublicacion:', error);
                return throwError(() => new Error('Error al agregar conjunto de publicaciones'));
            })
        );
    }

    DelConjuntoPublicacionById(id: any): Observable<any> {
        return this.httpClient.get<any>(`${this.url}/publicaciones/delPublicacionConjuntos?id=${id}`, this.headers).pipe(
            catchError(error => {
                console.error('Error en DelConjuntoPublicacionById:', error);
                return throwError(() => new Error('Error al eliminar conjunto de publicaciones por ID'));
            })
        );
    }
}
