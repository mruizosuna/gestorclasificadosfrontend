import { Component, HostBinding, Inject } from '@angular/core';
import { AuthService, ScreenService, AppInfoService } from './shared/services';
import { environment } from '../environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

 @HostBinding('class') 
  get getClass() {
    interface ScreenSizes {
      'screen-x-small': boolean;
      'screen-small': boolean;
      'screen-medium': boolean;
      'screen-large': boolean;
    }


    return Object.keys(this.screen.sizes)
      .filter((cl: string) => this.screen.sizes[cl as keyof ScreenSizes])
      .join(' ');
  }

  constructor(
    private authService: AuthService,
    private screen: ScreenService,
    public appInfo: AppInfoService
  ) {}

  isAuthenticated() {
    return this.authService.loggedIn;
  }

  
  ngOnInit() {
/*    const currentHostname = window.location.href;

    // Redirige si el dominio es vecinoemprende.com y la ruta es '/'
    if ((currentHostname === environment.urlOrigen1 || currentHostname === environment.urlOrigen2) && window.location.pathname === '/') {
      window.location.href = environment.urlRedireccion1;
    }*/
  }
}