import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import notify from 'devextreme/ui/notify';
import { UsuarioData } from '../../../models/usuario.model';
import { Perfiles } from '../../../models/perfiles.model';
import { UsuarioService } from '../../../services/usuarios.service';
import { PerfilesService } from '../../../services/perfiles.service';
import { tags } from '@angular-devkit/core';
import { Parametros } from '../../../models/parametros.model';
import { ParametricasService } from '../../../services/parametricas.service';
import { environment } from '../../../environments/environment';
import { LoggerService } from '../../../services/logger.service';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.scss']
})
export class PerfilComponent implements OnInit {
  @ViewChild('delete') delete: any;
  @ViewChild('seleccionAvatar') seleccionAvatar: any;
  selectUsuarios: Array<UsuarioData> = new Array<UsuarioData>;
  usuarios: UsuarioData = new UsuarioData();
  estado: string = 'Add';


  selectPerfil: Array<Perfiles> = [];
  idRecordDelete: number = 0; //Id del Registro a Borrar
  ValorServicio: any; //Valores devueltos por el servicio web

  selectListaTipoDocumento: Array<Parametros> = new Array<Parametros>;

  displayGuardarImagen: boolean = false;

  value: any[] = [];

  urlBlob: string;

  modificarImagen: boolean = false;

  selectedAvatar: string = '';  // Variable para almacenar la imagen seleccionada

  avatars: string[] = [
    "Avatar1.jpg", "Avatar2.jpg", "Avatar3.jpg", "Avatar4.jpg", "Avatar5.jpg",
    "Avatar6.jpg", "Avatar7.jpg", "Avatar8.jpg", "Avatar9.jpg", "Avatar10.jpg",
    "Avatar11.jpg", "Avatar12.jpg", "Avatar13.jpg", "Avatar14.jpg", "Avatar15.jpg",
    "Avatar16.jpg", "Avatar17.jpg", "Avatar18.jpg", "Avatar19.jpg", "Avatar20.jpg",
    "Avatar21.jpg", "Avatar22.jpg", "Avatar23.jpg", "Avatar24.jpg", "Avatar25.jpg",
    "Avatar26.jpg", "Avatar27.jpg", "Avatar28.jpg", "Avatar29.jpg", "Avatar30.jpg",
    "Avatar31.jpg", "Avatar32.jpg", "Avatar33.jpg", "Avatar34.jpg", "Avatar35.jpg",
    "Avatar36.jpg", "Avatar37.jpg", "Avatar38.jpg", "Avatar39.jpg", "Avatar40.jpg",
    "Avatar41.jpg", "Avatar42.jpg", "Avatar43.jpg", "Avatar44.jpg", "Avatar45.jpg",
    "Avatar46.jpg", "Avatar47.jpg", "Avatar48.jpg",
    "Avatar49.jpg", "Avatar50.jpg", "Avatar51.jpg", "Avatar52.jpg", "Avatar53.jpg",
    "Avatar54.jpg", "Avatar55.jpg", "Avatar56.jpg", "Avatar57.jpg", "Avatar58.jpg",
    "Avatar59.jpg", "Avatar60.jpg", "Avatar61.jpg", "Avatar62.jpg", "Avatar63.jpg",
    "Avatar64.jpg", "Avatar65.jpg", "Avatar66.jpg", "Avatar67.jpg", "Avatar68.jpg",
    "Avatar69.jpg", "Avatar70.jpg", "Avatar71.jpg", "Avatar72.jpg", "Avatar73.jpg",
    "Avatar74.jpg", "Avatar75.jpg", "Avatar76.jpg"
  ];



  constructor(

    private usuariosservice: UsuarioService, private modalService: NgbModal,
    private perfilesservice: PerfilesService, private parametricasservice: ParametricasService,
    private logger: LoggerService

  ) {
    this.urlBlob = environment.urlBlob;
  }

  async ngOnInit() {

    await this.CargarPerfiles();
    await this.CargarTiposDocumento();
    await this.InicializarFormulario();
    await this.CargarPerfilUsuario();
  }

  seleccionarAvatar(avatar: string) {

    this.selectedAvatar = avatar; // Almacena la imagen seleccionada

  }


  MostrarAvatares() {
    this.open(this.seleccionAvatar);
  }


  cerrarModales() {
    this.modalService.dismissAll();

  }

  limpiarAvatar() {
    this.selectedAvatar = '';
  }

  public async CargarPerfiles() {
    await this.GetListaPerfiles();
  }

  InicializarFormulario() {
    this.limpiarCampos();
    this.estado = 'Add';
  }

  GetListaPerfilesResult(): any {
    return this.perfilesservice.GetListaPerfiles().toPromise();
  }

  async GetListaPerfiles() {
    try {

      this.ValorServicio = await this.GetListaPerfilesResult();
      this.selectPerfil = this.ValorServicio.Lista;
    }
    catch (error) {
      console.error('[error en GetListaPerfiles] : ' + error);
    }
  }

  ValidarUsuarios() {

    if (this.usuarios.PerfilUsuarioID === 0) {
      notify("Debe seleccionar un valor de la casilla de perfil", "warning", 4000);
      return false;
    }
    else if (this.usuarios.Nombre === '') {
      notify("Debe ingresar un valor en la casilla nombre", "warning", 4000);
      return false;
    }
    else if (this.usuarios.CorreoElectronico === '') {
      notify("Debe ingresar un valor de la casilla de correo electrónico", "warning", 4000);
      return false;
    }
    else if (this.usuarios.Contrasena === '') {
      notify("Debe ingresar un valor de la casilla de contraseña", "warning", 4000);
      return false;
    }

    else
      return true;

  }

  public async AsignarUsuarios() {
    if (this.ValidarUsuarios()) {

 
      if (this.estado === 'Add') {
        this.usuarios.TipoDocumentoID = Number(this.usuarios.TipoDocumentoIDS);

        // Si el usuario seleccionó un avatar, convertirlo a base64
        if (this.selectedAvatar) {
          this.usuarios.ImagenUsuario = this.selectedAvatar;
          this.usuarios.Imagen64 = await this.convertAvatarToBase64(this.selectedAvatar);
       
        }
        else if (this.value.length > 0) {
          // Si el usuario subió una imagen, convertirla a base64
          const files = this.value;
          for (let file of files) {
            const resizedImage = await this.resizeImage(file, 600, 400);
            this.usuarios.Imagen64 = await this.convertFileToBase64(resizedImage);
          
          }
        }


        await this.usuariosservice.PostUsuarioGeneral(this.usuarios).subscribe(async result => {
          if (result.success == false) {
            notify("Error al asignar usuario", "error", 4000);
          } else {
            notify("Se ha asignado el usuario correctamente", "success", 4000);
            await this.CargarPerfilUsuario();
          }
        }, error => {
          console.error(error);
        });

      } else if (this.estado === 'Upd') {
        this.usuarios.TipoDocumentoID = Number(this.usuarios.TipoDocumentoIDS);

        if (this.selectedAvatar) {
          this.usuarios.ImagenUsuario = this.selectedAvatar;
          this.usuarios.Imagen64 = await this.convertAvatarToBase64(this.selectedAvatar);
        }
        else if (this.value.length > 0) {
          const files = this.value;
          for (let file of files) {
            const resizedImage = await this.resizeImage(file, 128, 128);
            this.usuarios.Imagen64 = await this.convertFileToBase64(resizedImage);
          }
        }


        await this.usuariosservice.PutUsuarioGeneralById(this.usuarios).subscribe(async result => {
          if (result.success == false) {
            notify("Error al actualizar el perfil", "error", 4000);
          } else {
            notify("Se ha actualizado el perfil correctamente", "success", 4000);
            await this.CargarPerfilUsuario();
          }
        }, error => {
          console.error(error);
        });
      }
    }
  }


  private async convertAvatarToBase64(avatarPath: string): Promise<string> {
    return new Promise((resolve, reject) => {
      const img = new Image();
  
      img.src = `assets/images/Avatares/${avatarPath}`; // Ruta relativa sin "../.."

      img.onload = () => {
        const canvas = document.createElement("canvas");
        const ctx = canvas.getContext("2d");

        if (!ctx) {
          reject("No se pudo obtener el contexto del canvas");
          return;
        }

        // Ajustar tamaño del canvas a la imagen
        canvas.width = img.width;
        canvas.height = img.height;
        ctx.drawImage(img, 0, 0);

        // Convertir la imagen en base64 con formato JPG
        resolve(canvas.toDataURL("image/jpeg"));
      };

      img.onerror = () => reject("Error al cargar la imagen");
    });
  }


  /*
    public async AsignarUsuarios() {
  
      if (this.ValidarUsuarios()) {
  
  
        if (this.estado === 'Add') {
  
          this.usuarios.TipoDocumentoID = Number(this.usuarios.TipoDocumentoIDS);
  
          if (this.usuarios.ImagenUsuario == 'SinImagen.png')
            this.usuarios.ImagenUsuario = null;
          else {
            //Guardado de la fotografia
            const files = this.value;
  
            for (let file of files) {
              const resizedImage = await this.resizeImage(file, 600, 400); // Redimensiona la imagen a 800x600
              const base64Content = await this.convertFileToBase64(resizedImage);
              this.usuarios.Imagen64 = base64Content;
            }
  
            this.displayGuardarImagen = false;
            this.value = [];
          }
  
  
          if (this.usuarios.NumeroDocumento === null || this.usuarios.NumeroDocumento === '' || this.usuarios.TipoDocumentoID === null || this.usuarios.TipoDocumentoID === 0 || this.usuarios.TelefonoCelular === null || this.usuarios.TelefonoCelular === '') {
            localStorage.setItem('perfilActualizado', "false");
          }
          else
          {
            localStorage.setItem('perfilActualizado', "true");
          }
  
          await this.usuariosservice.PostUsuarioGeneral(this.usuarios).subscribe(async result => {
  
            if (result.success == false) {
              notify("Error al asignar usuario", "error", 4000);
            }
            else {
              notify("Se ha asignado el usuario correctamente ", "success", 4000);
            await this.CargarPerfilUsuario();
            }
          },
            error => {
              console.error(error);
            }
          );
        }
        else if (this.estado === 'Upd') {
  
          this.usuarios.TipoDocumentoID = Number(this.usuarios.TipoDocumentoIDS);
  
          const files = this.value;
  
            for (let file of files) {
              const resizedImage = await this.resizeImage(file, 128, 128); // Redimensiona la imagen a 800x600
              const base64Content = await this.convertFileToBase64(resizedImage);
              this.usuarios.Imagen64 = base64Content;
            }
         
  
  
  
          await this.usuariosservice.PutUsuarioGeneralById(this.usuarios).subscribe(async result => {
  
            if (result.success == false) {
              notify("Error al actualizar el perfil", "error", 4000);
            }
            else {
              notify("Se ha actualizado el perfil correctamente ", "success", 4000);
              await this.CargarPerfilUsuario();
            }
          },
            error => {
              console.error(error);
            }
          );
        }
      }
    }*/

  GetUsuarioDataResult(): any {
    return this.usuariosservice.GetListaUsuarioGeneral().toPromise();
  }

  async GetUsuarioData() {
    try {
      this.ValorServicio = await this.GetUsuarioDataResult();


      this.selectUsuarios = this.ValorServicio.Lista;
    }
    catch (error) {
      console.error('[error en GetUsuarioData] : ' + error);
    }
  }

  GetUsuariosByIdResult(id: number) {
    return this.usuariosservice.GetUsuarioGeneralById(id).toPromise();
  }

  DelUsuariosByIdResult(id: number) {
    return this.usuariosservice.DelUsuarioGeneralById(id).toPromise();
  }
  async DelUsuariosById(id: number) {
    await this.DelUsuariosByIdResult(id);
  }

  async GetUsuariosById(id: number) {
    try {

      this.ValorServicio = await this.GetUsuariosByIdResult(id);
      return this.ValorServicio;


    }
    catch (error) {
      console.error('[error en GetUsuarioData] : ' + error);
    }
  }

  async CargarPerfilUsuario() {

    let id: number;

    id = Number(localStorage.getItem('userid'));


    let usuarios: any;

    usuarios = await this.GetUsuariosById(id);

    this.usuarios.PerfilUsuarioID = usuarios.PerfilUsuarioID;
    this.usuarios.UserID = usuarios.UserID;
    this.usuarios.Nombre = usuarios.Nombre;
    this.usuarios.CorreoElectronico = usuarios.CorreoElectronico;
    this.usuarios.Contrasena = usuarios.Contrasena;
    this.usuarios.TipoDocumentoID = usuarios.TipoDocumentoID;
    this.usuarios.RazonSocial = usuarios.RazonSocial;
    this.usuarios.NombreSocial = usuarios.NombreSocial;
    this.usuarios.TipoDocumentoIDS = usuarios.TipoDocumentoID.toString();
    this.usuarios.NumeroDocumento = usuarios.NumeroDocumento;
    this.usuarios.TelefonoCelular = usuarios.TelefonoCelular;
    this.usuarios.EsCuentaVerificada = usuarios.EsCuentaVerificada;


    if (usuarios.ImagenUsuario == null)
      this.usuarios.ImagenUsuario = 'SinImagen.png';
    else
      this.usuarios.ImagenUsuario = usuarios.ImagenUsuario;

    this.usuarios.TipoDocumentoID = 1;

    this.estado = 'Upd';

  }

  open(content: any) {
    this.modalService.open(content, { backdrop: 'static', keyboard: false, size: 'lg', windowClass: 'modal-dialog-centered' });
  }

  limpiarCampos() {
    this.usuarios.PerfilUsuarioID = 0;
    this.usuarios.Nombre = "";
    this.usuarios.CorreoElectronico = "";
    this.usuarios.Contrasena = "";
    this.usuarios.TipoDocumentoID = 0;
    this.usuarios.TipoDocumentoIDS = '';
    this.usuarios.NumeroDocumento = '';
    this.usuarios.RazonSocial = '';
    this.usuarios.NombreSocial = '';
    this.usuarios.TelefonoCelular = '';
    this.usuarios.ImagenUsuario = 'SinImagen.png';
    this.usuarios.Imagen64 = '';
  }


  GetListaParametrosResult(tipo: string): any {
    return this.parametricasservice.GetListaParametrosById(tipo).toPromise();
  }

  async GetListaTipoDocumento() {
    try {

      this.ValorServicio = await this.GetListaParametrosResult("TIPODOCUMENTO");
      this.selectListaTipoDocumento = this.ValorServicio.Lista;
    }
    catch (error) {
      console.error('[error en GetListaAlcances] : ' + error);
    }
  }

  public async CargarTiposDocumento() {
    await this.GetListaTipoDocumento();
  }


  public async resizeImage(file: File, width: number, height: number): Promise<File> {
    return new Promise<File>((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        const img = new Image();
        img.src = reader.result as string;
        img.onload = () => {
          const canvas = document.createElement('canvas');
          const ctx = canvas.getContext('2d');
          canvas.width = width;
          canvas.height = height;
          ctx!.drawImage(img, 0, 0, width, height);
          canvas.toBlob((blob) => {
            if (blob) {
              resolve(new File([blob], file.name, { type: file.type }));
            } else {
              reject(new Error('Image resizing failed.'));
            }
          }, file.type);
        };
      };
      reader.onerror = (error) => reject(error);
    });
  }

  public async convertFileToBase64(file: File): Promise<string> {
    return new Promise<string>((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        const result = reader.result as string;
        const base64Content = result.split(',')[1];
        resolve(base64Content);
      };
      reader.onerror = (error) => reject(error);
    });
  }



  subidaCompletaImagen(e: {
    value: null; request: { response: string; status: number; };
  }) {

    try {
      if (e.value == null || e.value == undefined) {
        this.displayGuardarImagen = false;
        notify("Error cargando el archivo", "error", 4000);
        this.modificarImagen = false;
      }
      else {

        this.displayGuardarImagen = true;
        this.modificarImagen = true;

      }
    }
    catch (error) {
      console.error('[error en Cargue de Imagenes] : ' + error);
    }
  }

}