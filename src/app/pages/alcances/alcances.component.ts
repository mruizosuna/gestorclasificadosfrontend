import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Alcances } from '../../../models/alcances.model';

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import notify from 'devextreme/ui/notify';
import { AlcancesService } from '../../../services/alcances.service';



@Component({
  selector: 'app-alcances',
  templateUrl: './alcances.component.html',
  styleUrls: ['./alcances.component.scss']
})
export class AlcancesComponent implements OnInit {
  @ViewChild('delete') delete: any;
  selectAlcances: Array<Alcances> = new Array<Alcances>;
  alcances: Alcances = new Alcances();
  estado: string = 'Add';
  idRecordDelete : number = 0; //Id del Registro a Borrar
  ValorServicio: any; //Valores devueltos por el servicio web

  constructor(

    private alcancesservice: AlcancesService,private modalService: NgbModal


  ) { }


  
  ngOnInit(){
    this.CargarAlcances();
    this.InicializarFormulario();
  }
  public async CargarAlcances() {


    



    await this.GetListaAlcances();
    
  }
  InicializarFormulario()
  {
    this.alcances.NombreAlcance  = '';
    this.alcances.CantidadAlcance = 0;
    this.estado = 'Add';
  }


  ValidarAlcances() {

    if (this.alcances.NombreAlcance === '') {
      notify("Debe ingresar un valor en la casilla nombre", "warning", 4000);
      return false;
    }
    else if(this.alcances.CantidadAlcance === 0){
      notify("Debe ingresar un valor en la casilla Cantidad", "warning", 4000);
      return false;
    }
    else
      return true;

  }

  public async AsignarAlcance() {

    if (this.ValidarAlcances()) {
      

      if (this.estado === 'Add') {

        await this.alcancesservice.PostAlcances(this.alcances).subscribe(async result => {

          
          if (result.success == false) { 
            notify("Error al asignar alcances", "error", 4000);
          }
          else 
          {
            notify("Se ha asignado la alcances correctamente ", "success", 4000);
            this.InicializarFormulario();
            await this.GetListaAlcances();
          }
        },
          error => {
            console.error(error);
          }
        );
      }
      else if (this.estado === 'Upd') {

        await this.alcancesservice.PutAlcancesById(this.alcances).subscribe(async result => {

          if (result.success == false) { 
            notify("Error al actualizar la alcances", "error", 4000);
          }
          else 
          {
            notify("Se ha actualizado alcances correctamente ", "success", 4000);
            this.InicializarFormulario();
            await this.GetListaAlcances();
          }
        },
          error => {
            console.error(error);
          }
        );
      }




    }
  }
  GetListaAlcancesResult(): any {
    return this.alcancesservice.GetListaAlcances().toPromise();
  }

  async GetListaAlcances() {
    try {
      this.ValorServicio = await this.GetListaAlcancesResult();
      
     
      this.selectAlcances = this.ValorServicio.Lista;
    }
    catch (error) {
      console.error('[error en GetListaAlcances] : ' + error);
    }
  }
  GetAlcancesByIdResult(id: number) {
    return this.alcancesservice.GetAlcancesById(id).toPromise();
  }
  DelAlcancesByIdResult(id: number) {
    return this.alcancesservice.DelAlcancesById(id).toPromise();
  }
  async DelAlcancesById(id: number){
    await this.DelAlcancesByIdResult(id);
  }

  async GetAlcancesById(id: number) {
    try {
      
      this.ValorServicio = await this.GetAlcancesByIdResult(id);
      return this.ValorServicio;


    }
    catch (error) {
      console.error('[error en GetListaAlcances] : ' + error);
    }
  }

  async Editar(id: number) {

    let alcances: any;
    
    alcances = await this.GetAlcancesById(id);

    
    this.alcances.AlcanceID = alcances.AlcanceID;
    this.alcances.NombreAlcance = alcances.NombreAlcance;
    this.alcances.CantidadAlcance = alcances.CantidadAlcance;
   

    this.estado = 'Upd';

    //await this.AsignarDispositivoUsuario(); //Se elimina el llamado a esta función
    await this.GetListaAlcances();

  }

  open(content: any) {
    this.modalService.open(content, { backdrop: 'static', keyboard: false, size: 'lg', windowClass: 'modal-dialog-centered'  });
  }

  ConfirmacionEliminar(id: number)
  {
    this.open(this.delete);
    this.idRecordDelete = id;
  }


    async Eliminar() {

    await this.DelAlcancesById(this.idRecordDelete);
    
    await this.GetListaAlcances();

  }
}