import { ChangeDetectorRef, Component, ElementRef, OnInit, Renderer2, ViewChild } from '@angular/core';
import { DuplicacionPublicacion, Publicaciones, PublicacionesBuscador, PublicacionRenovacionDetalle } from '../../../models/publicaciones.model';
import { PublicacionesService } from '../../../services/publicaciones.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import notify from 'devextreme/ui/notify';
import { UsuarioService } from '../../../services/usuarios.service';
import { ListaTipoPublicacion } from '../../../models/tipospublicacion.model';
import { Alcances, ListaAlcances } from '../../../models/alcances.model';
import { ListaPeriodosPublicacion, PeriodosPublicacion } from '../../../models/periodospublicacion.model';
import { TiposPublicacionService } from '../../../services/tipospublicacion.service';
import { AlcancesService } from '../../../services/alcances.service';
import { PeriodosPublicacionService } from '../../../services/periodospublicacion.service';
import { ValorPublicacion } from '../../../models/valorpublicacion.model';
import { ValorPublicacionService } from '../../../services/valorpublicacion.service';
import { ImagenesPublicacion } from '../../../models/imagenespublicacion.model';
import { ImagenesPublicacionService } from '../../../services/imagenespublicacion.service';
import { environment } from '../../../environments/environment';
import { VideosPublicacion } from '../../../models/videospublicacion.model';
import { VideosPublicacionService } from '../../../services/videospublicacion.service';
import { CategoriasPrincipales } from '../../../models/categoriasprincipales.model';
import { CategoriaPublicacionService } from '../../../services/categoriapublicacion.service';
import { CategoriasPrincipalesService } from '../../../services/categoriasprincipales.service';
import { CategoriasSecundariasService } from '../../../services/categoriassecundarias.service';
import { CategoriaPublicacion } from '../../../models/categoriapublicacion.model';
import { CategoriasSecundarias } from '../../../models/categoriassecundarias.model';
import { Zonas } from '../../../models/zonas.model';
import { Conjuntos } from '../../../models/conjuntos.model';
import { ConjuntoPublicacion } from '../../../models/conjuntopublicacion.model';
import { ConjuntosService } from '../../../services/conjuntos.service';
import { ZonasService } from '../../../services/zonas.service';
import { ConjuntoPublicacionService } from '../../../services/conjuntopublicacion.service';
import { Parametros } from '../../../models/parametros.model';
import { ParametricasService } from '../../../services/parametricas.service';
import { Pagos } from '../../../models/pagos.model';
import * as CryptoJS from 'crypto-js';

import { Router } from '@angular/router';
import { PagosService } from '../../../services/pagos.service';
import { Descuentos } from '../../../models/descuentos.model';
import { EmailService } from '../../../services/email.service';
import { EmailParametros } from '../../../models/emailparametros.model';
import { convertTypeAcquisitionFromJson } from 'typescript';
import { PuntuacionesPublicacionService } from '../../../services/puntuacionespublicacion.service';
import { VistasPublicacion } from '../../../models/vistaspublicacion.model';
import { PuntuacionPublicacion } from '../../../models/puntuacionpublicacion.model';
import { PuntuacionVendedor } from '../../../models/puntuacionvendedor.model';
import { VistasPublicacionService } from '../../../services/vistaspublicacion.service';
import { LoggerService } from '../../../services/logger.service';



@Component({
  selector: 'app-publicaciones',
  templateUrl: './publicaciones.component.html',
  styleUrls: ['./publicaciones.component.scss']
})
export class PublicacionesComponent implements OnInit {

  @ViewChild('deleteImagen') deleteImagen: any;
  @ViewChild('deleteVideo') deleteVideo: any;
  @ViewChild('deleteCategoria') deleteCategoria: any;
  @ViewChild('deleteConjunto') deleteConjunto: any;
  @ViewChild('deletePublicacion') deletePublicacion: any;
  @ViewChild('duplicatePublicacion') duplicatePublicacion: any;
  @ViewChild('renovarPublicacion') renovarPublicacion: any;
  @ViewChild('codigoBienvenida') codigoBienvenida: any;
  @ViewChild('detallePublicacion') detallePublicacion: any;
  @ViewChild('establecerImagenPrincipal') establecerImagenPrincipal: any;
  @ViewChild('mensajeRecordatorioPerfil') mensajeRecordatorioPerfil: any;





  @ViewChild('formularioPago') formularioPago: any;
  @ViewChild('formularioPagoRenovacion') formularioPagoRenovacion: any;

  @ViewChild('mensajePago') mensajePago: any;
  @ViewChild('creacionPublicacion') creacionPublicacion: any;

  @ViewChild('DescuentoCodigo') DescuentoCodigo: any;
  selectListaTipoPublicacion: Array<ListaTipoPublicacion> = [];
  selectListaTipoPublicacionRenovacion: Array<ListaTipoPublicacion> = [];

  selectListaAlcances: Array<ListaAlcances> = [];
  selectListaPeriodosPublicacion: Array<PeriodosPublicacion> = [];
  selectPeriodo: Array<PeriodosPublicacion> = new Array<PeriodosPublicacion>;
  selectListaPublicacion: Array<Publicaciones> = new Array<Publicaciones>;
  selectListaPublicacionOriginal: Array<Publicaciones> = new Array<Publicaciones>;
  selectListaPublicacionBienvenida: Array<Publicaciones> = new Array<Publicaciones>;

  selectListaTipoDocumento: Array<Parametros> = new Array<Parametros>;
  selectListaValorPublicacion: Array<ValorPublicacion> = new Array<ValorPublicacion>;
  selectCategoriasPrincipales: Array<CategoriasPrincipales> = new Array<CategoriasPrincipales>;
  selectCategoriasSecundarias: Array<CategoriasSecundarias> = new Array<CategoriasSecundarias>;
  selectZonas: Array<Zonas> = new Array<Zonas>;
  selectConjuntos: Array<Conjuntos> = new Array<Conjuntos>;
  tipoPublicacionEncontrado: ListaTipoPublicacion | undefined;

  selectListaImagenesPublicacion: Array<ImagenesPublicacion> = new Array<ImagenesPublicacion>;
  selectListaImagenesDetallePublicacion: Array<ImagenesPublicacion> = new Array<ImagenesPublicacion>;

  selectListaVideosPublicacion: Array<VideosPublicacion> = new Array<VideosPublicacion>;
  selectListaCategoriasPublicacion: Array<CategoriaPublicacion> = new Array<CategoriaPublicacion>;
  selectListaConjuntoPublicacion: Array<ConjuntoPublicacion> = new Array<ConjuntoPublicacion>;

  publicaciones: Publicaciones = new Publicaciones();
  publicacionesRenovacion: Publicaciones = new Publicaciones();
  publicacionRenovacionDetalle: PublicacionRenovacionDetalle = new PublicacionRenovacionDetalle();
  descuentos: Descuentos = new Descuentos();
  imagenPublicacion: ImagenesPublicacion = new ImagenesPublicacion();
  videoPublicacion: VideosPublicacion = new VideosPublicacion();
  categoriaPublicacion: CategoriaPublicacion = new CategoriaPublicacion();
  conjuntoPublicacion: ConjuntoPublicacion = new ConjuntoPublicacion();

  pagos: Pagos = new Pagos();

  estado: string = 'Add';

  selectUsuarios: Array<Publicaciones> = [];
  idRecordDelete: number = 0; //Id del Registro a Borrar
  idRecordDuplicate: number = 0; //Id del Registro a Borrar
  idRecordRenovate: number = 0; //Id del Registro a Renovar

  idRecordImagenPrincipal: number = 0; //Id del Registro a Establecer Imagen Principal

  indexImagen: number = 0; //Indice de la imagen
  ValorServicio: any; //Valores devueltos por el servicio web

  infoPublicacion: string = '';
  infoPublicacionRenovacion: string = '';
  infoPublicacionActual: string = '';
  idPublicacion: number = 0;

  displayGuardarImagen: boolean = false;
  displayGuardarVideo: boolean = false;
  displayGuardarCategoria: boolean = false;
  displayGuardarConjunto: boolean = false;


  displayCrearPublicacion: boolean = true;
  displayCalculoPublicacion: boolean = false;
  displayPagarPublicacion: boolean = false;//OJO se debe dejar en false
  displayCalculoPublicacionRenovacion: boolean = false;
  displayPagarPublicacionRenovacion: boolean = false;//OJO se debe dejar en false
  displayInformacionRenovacion: boolean = false;

  displayPasarelaPagos: boolean = false;

  value: any[] = [];

  urlBlob: string;

  usuario: number;
  perfil: number;

  displayImagenesPublicacion: boolean = false;
  displayVideosPublicacion: boolean = false;
  displayCategoriasPublicacion: boolean = false;
  displayCoberturaNacional: boolean = false;
  displayConjuntosPublicacion: boolean = false;
  displayDepartamentoLocalizacion: boolean = false;
  displayCiudadLocalizacion: boolean = false;
  displayDatosBasicosPublicacion: boolean = false;

  alcanceTipo: string = '';
  cantidadAlcance: number = 0;


  textoBtn: string = 'Guardar Publicación';


  descripcionCuenta: string = '';
  correoRecepcionPago: string = '';
  whatsappRecepcionPago: string = '';

  title = 'sha256-example';
  hash: string;

  valorPublicacion: number = 0;
  valorIVA: number = 0;
  valorNeto: number = 0;
  valorBase: number = 0;
  valorNetoPrevio: number = 0;
  valorDescuento: number = 0;



  selectedHouse: PublicacionesBuscador;
  currentHouse: PublicacionesBuscador;

  selectedHousePublicacion: ImagenesPublicacion;
  currentHousePublicacion: ImagenesPublicacion;

  popupVisible = false;

  activeSlideIndex: number = 0; // Índice de la imagen activa en el carrusel
  carouselInterval: number = 3000; // Intervalo de tiempo entre cada imagen (en milisegundos)

  esEditable: boolean = true;
  esEditableVideo: boolean = true;


  mensajedetalle: string = 'Datos Básicos de la Publicación';

  popupPosition: any;

  datosPagoIguales: boolean = false;
  valorAsignado: string = '';

  radioOptions = [
    { text: 'Sí', value: true },
    { text: 'No', value: false }
  ];

  radioOptionsRenovacion = [
    { text: 'Aplicar cambios inmediatamente', value: 'opcion1' },
    { text: 'Aplicar al finalizar la vigencia del Plan Actual', value: 'opcion2' }
  ];

  radioOptionsTipoRenovacion = [
    { text: 'Renovar mi Plan Actual', value: 'opcion1' },
    { text: 'Cambiar a otro Plan', value: 'opcion2' }
  ];

  // Propiedad para almacenar las opciones filtradas
  filteredRadioOptions: any[] = [];

  // Lista filtrada que mostrará solo los planes superiores
  filteredListaTipoPublicacionRenovacion: any[] = [];

  usarPlanActual: boolean = true;
  tiporenovacion: string = '';
  aplicarcambiosrenovacion: string = '';
  formaaplicarcambioplan: string = '';

  showMessage = false;
  message = '';

  displayPublicaciones: boolean = true;

  consecutivoPublicacion: string = '';

  nuevoIDPublicacion: number = 0;

  numeroImagenesPublicacion: number = 0;

  tipoCoberturaPublicacion: string = '';


  showPopup: boolean = false;


  currentPage = 0;
  imagesPerPage = 5;

  periodoPublicacionBienvenida: boolean = false;

  mensajebienvenidapublicacion: string = '';

  mostrarBotonActualizarSecundario: number = 0;

  numeroVistas: number = 0;
  puntuacionPublicacion: number = 0;
  puntuacionVendedor: number = 0;

  vistaspublicacion: VistasPublicacion = new VistasPublicacion();

  selectListaVistasPublicacion: Array<VistasPublicacion> = new Array<VistasPublicacion>;
  selectListaPuntuacionPublicacion: Array<PuntuacionPublicacion> = new Array<PuntuacionPublicacion>;
  selectListaPuntuacionVendedor: Array<PuntuacionVendedor> = new Array<PuntuacionVendedor>;

  loading = false;

  longPressTimeout: any; // Almacena el temporizador de la pulsación prolongada
  longPressDuration: number = 1000; // Duración en ms para considerar una pulsación prolongada

  isMobileDevice: boolean = false;

  usarRangoPrecios: boolean = false; // Estado inicial del checkbox

  format = (value: unknown) => `${value}`;

  tooltip = {
    enabled: true,
    format: (value: unknown) => this.format(value),
    showMode: 'always',
    position: 'bottom',
  };

  tooltipEnabled = {
    enabled: true,
  };


  dashboardCounts = {
    total: 0,
    aprobadas: 0,
    vencidas: 0,
    borrador: 0,
    rechazadas: 0
  };

  mostrarDashBoardPublicaciones: boolean = false;

  motivoPago: string = 'PagoPublicacion';
  codigoDescuentoAplicado: boolean = false;

  constructor(

    private publicacionesservice: PublicacionesService, private modalService: NgbModal,
    private tipoPublicacionservice: TiposPublicacionService, private alcancesservice: AlcancesService,
    private periodosPublicacionservice: PeriodosPublicacionService, private valorPublicacionService: ValorPublicacionService,
    private categoriasprincipalesservice: CategoriasPrincipalesService, private categoriassecundariasservice: CategoriasSecundariasService,
    private imagenesPublicacionservice: ImagenesPublicacionService, private videosPublicacionservice: VideosPublicacionService,
    private categoriaPublicacionservice: CategoriaPublicacionService,
    private zonaservice: ZonasService, private conjuntosservice: ConjuntosService,
    private conjuntoPublicacionservice: ConjuntoPublicacionService, private parametricasservice: ParametricasService,
    private puntuacionPublicacionservice: PuntuacionesPublicacionService,
    private usuariosservice: UsuarioService,
    private router: Router,
    private pagosservice: PagosService,
    private tipospublicacionservice: TiposPublicacionService,
    private emailservice: EmailService,
    private cdr: ChangeDetectorRef,
    private vistaspublicacionservice: VistasPublicacionService,
    private renderer: Renderer2, private el: ElementRef,
    private logger: LoggerService

  ) {

    this.urlBlob = environment.urlBlob;
    this.perfil = Number(localStorage.getItem('perfilUsuario'));
    this.usuario = Number(localStorage.getItem('userid'));

    this.popupPosition = {
      my: 'center',
      at: 'center',
      of: window
    };

  }

  async onOptionTipoRenovacionChange(value: string) {

    // Define el mensaje según la opción seleccionada
    if (value === 'opcion1') {

      this.tiporenovacion = 'opcion1';
      this.formaaplicarcambioplan = '';
      let publicaciones: Publicaciones;

      publicaciones = await this.GetPublicacionesById(this.idRecordRenovate);

      this.publicaciones = publicaciones;
      this.publicacionesRenovacion = publicaciones;

      if (this.publicaciones.TipoPublicacionID === 1)
        this.publicaciones.TipoPublicacionID = 0;



      const fecha = new Date(publicaciones.FechaCreacion.substring(0, 10));
      const year = fecha.getFullYear();
      const month = String(fecha.getMonth() + 1).padStart(2, '0'); // Aseguramos que el mes sea de 2 dígitos
      const day = String(fecha.getDate()).padStart(2, '0'); // Aseguramos que el día sea de 2 dígitos

      this.consecutivoPublicacion = year + month + day + this.publicaciones.PublicacionID.toString();

      await this.MostrarInformacionPlanActual();


      this.usarPlanActual = true;
      this.displayInformacionRenovacion = false;

      await this.CalcularCostoPublicacionRenovacion();
      this.displayCalculoPublicacionRenovacion = true;


    }
    else if (value === 'opcion2') {

      this.tiporenovacion = 'opcion2';
      this.formaaplicarcambioplan = '';
      await this.CargarTipoPublicacionSinBienvenida();

      let publicaciones: Publicaciones;

      publicaciones = await this.GetPublicacionesById(this.idRecordRenovate);

      this.publicaciones = publicaciones;
      this.publicacionesRenovacion = publicaciones;

      this.publicaciones.TipoPublicacionID = 0;

      const fecha = new Date(publicaciones.FechaCreacion.substring(0, 10));
      const year = fecha.getFullYear();
      const month = String(fecha.getMonth() + 1).padStart(2, '0'); // Aseguramos que el mes sea de 2 dígitos
      const day = String(fecha.getDate()).padStart(2, '0'); // Aseguramos que el día sea de 2 dígitos


      this.consecutivoPublicacion = year + month + day + this.publicaciones.PublicacionID.toString();

      await this.MostrarInformacionPlanActual();

      this.usarPlanActual = false;
      this.displayInformacionRenovacion = true;
      this.displayCalculoPublicacionRenovacion = false;


    }

  }

  onOptionChange(value: string) {
    this.aplicarcambiosrenovacion = value;

    // Define el mensaje según la opción seleccionada
    if (value === 'opcion1') {
      this.message = 'Importante!!! Al seleccionar esta opción, la nueva fecha de vigencia regirá a partir de hoy y hasta el tiempo del plan solicitado. Los dias que tenga en su plan anterior se perderan';
      this.formaaplicarcambioplan = 'opcion1';
    } else if (value === 'opcion2') {
      this.message = 'Importante !!! Al seleccionar esta opción, su nuevo plan empezará a regir a partir de la finalización del Plan Anterior. ';
      this.formaaplicarcambioplan = 'opcion2';
    }

    // Muestra el mensaje
    this.showMessage = true;

    // Oculta el mensaje después de 3 segundos
    /* setTimeout(() => {
       this.showMessage = false;
     }, 5000);*/
  }


  onCheckBoxChange(event: any) {
    this.usarRangoPrecios = event.value; // Actualiza el valor del checkbox
    if (this.usarRangoPrecios) {

      this.usarRangoPrecios = true;
      this.publicaciones.RangoPrecios = true;
      // Agregar la lógica que desees cuando el checkbox esté marcado
    } else {

      this.usarRangoPrecios = false;
      this.publicaciones.RangoPrecios = false;
      // Agregar la lógica para cuando el checkbox esté desmarcado
    }
  }

  onInputChange(event: any): void {
    this.pagos.RazonSocial = event.value.toUpperCase(); // Usar event.value en lugar de event.target.value
  }

  copyToClipboard(text: string): void {
    navigator.clipboard.writeText(text).then(() => {
      notify("¡URL copiada al portapapeles!", "success", 2000);
    }).catch(err => {
      console.error('Error al copiar al portapapeles:', err);
    });
  }

  // En tu componente TypeScript
  async ejecutarNgOnInit() {
    await this.ngOnInitCreate(); // Espera a que ngOnInit se complete

  }


  async ngOnInit() {

    this.isMobileDevice = this.detectMobileDevice();

    localStorage.removeItem('pagoPayu');
    await this.CargarTipoPublicacion();
    await this.CargarValorPublicacion();
    await this.CargarCategoriasPrincipales();
    await this.CargarZonas();
    await this.CargarTiposDocumento();
    await this.CargarInfoPagoTransferencia();

    if (this.perfil === 1) //Administrador (puede ver todas las publicaciones)
      await this.CargarPublicaciones();
    else //Usuario Publicador
      await this.CargarPublicacionesUser(this.usuario);

    this.selectListaPublicacionOriginal = [...this.selectListaPublicacion];
    debugger;
    //Se valida si el usuario tiene actualmente un código de Bienvenida sin aplicar
    await this.ConsultarCodigoDescuentoBienvenida();


    this.actualizarConteos();



  }

  detectMobileDevice(): boolean {
    const userAgent = navigator.userAgent || navigator.vendor;
    return /android|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(userAgent.toLowerCase());
  }

  adjustHeight(event: any): void {

    this.publicaciones.Descripcion = event.target.value;
    const textarea = event.target;  // Obtiene el elemento del textarea desde el evento
    textarea.style.height = 'auto';  // Resetea la altura antes de calcularla
    textarea.style.height = `${textarea.scrollHeight}px`;  // Ajusta la altura según el contenido



  }

  initializeTextArea(event: any): void {
    this.adjustHeight(event);
  }

  async ngOnInitCreate() {

    localStorage.removeItem('pagoPayu');


    await this.GetListaPublicacionesUsuarioBienvenida(this.usuario);


    if (this.selectListaPublicacionBienvenida.length > 0) {
      await this.CargarTipoPublicacionSinBienvenida();

    }
    else {
      await this.CargarTipoPublicacion();

    }



    await this.CargarValorPublicacion();
    await this.CargarCategoriasPrincipales();
    await this.CargarZonas();
    await this.CargarTiposDocumento();
    await this.CargarInfoPagoTransferencia();

    if (this.perfil === 1) //Administrador (puede ver todas las publicaciones)
      await this.CargarPublicaciones();
    else //Usuario Publicador
      await this.CargarPublicacionesUser(this.usuario);

    //Se valida si el usuario tiene actualmente un código de Bienvenida sin aplicar
    await this.ConsultarCodigoDescuentoBienvenida();

    if (this.periodoPublicacionBienvenida === false)
      await this.CrearPublicacion();


  }

  public async CargarPublicaciones() {
    await this.GetListaPublicaciones();
  }


  public async CargarPublicacionesUser(usuario: number) {
    await this.GetListaPublicacionesUsuario(usuario);
  }

  public async CargarTipoPublicacion() {
    await this.GetListaTiposPublicacion();
  }

  public async CargarTipoPublicacionSinBienvenida() {
    await this.GetListaTiposPublicacionSinBienvenida();
  }

  public async CargarAlcances() {
    await this.GetListaAlcances();
  }

  public async CargarPeriodosPublicacion() {
    await this.GetListaPeriodosPublicacion();
  }

  public async CargarPeriodosPublicacionRenovacion() {
    await this.GetListaPeriodosPublicacionRenovacion();
  }
  public async CargarValorPublicacion() {
    await this.GetListaValorPublicacion();
  }


  public async CargarCategoriasPrincipales() {
    await this.GetListaCategoriasPrincipales();
  }


  public async CargarZonas() {
    await this.GetListaZonas();
  }


  public async CargarTiposDocumento() {
    await this.GetListaTipoDocumento();
  }

  public async CargarInfoPagoTransferencia() {
    await this.GetInfoPagoTransferencia();
  }
  public async CargarCategoriasSecundarias(id: number) {
    await this.GetListaCategoriasSecundarias(id);
  }

  public async CargarConjuntos(id: number) {
    await this.GetListaConjuntos(id);
  }


  InicializarFormulario() {
    this.publicaciones = new Publicaciones();
    this.displayImagenesPublicacion = false;
    this.displayVideosPublicacion = false;
    this.displayCategoriasPublicacion = false;
    this.displayConjuntosPublicacion = false;
    this.displayDatosBasicosPublicacion = false;

    this.selectListaImagenesPublicacion = new Array<ImagenesPublicacion>;
    this.selectListaVideosPublicacion = new Array<VideosPublicacion>;
    this.selectListaCategoriasPublicacion = new Array<CategoriaPublicacion>;
    this.selectListaConjuntoPublicacion = new Array<ConjuntoPublicacion>;

    this.estado = 'Add';
  }

  InicializarGrillas() {
    this.selectListaImagenesPublicacion = new Array<ImagenesPublicacion>;
    this.selectListaVideosPublicacion = new Array<VideosPublicacion>;
    this.selectListaCategoriasPublicacion = new Array<CategoriaPublicacion>;
    this.selectListaConjuntoPublicacion = new Array<ConjuntoPublicacion>;


  }

  GetListaTipoPublicacionResult(): any {
    return this.tipoPublicacionservice.GetListaTiposPublicacion().toPromise();
  }
  async GetListaTiposPublicacion() {
    try {

      this.ValorServicio = await this.GetListaTipoPublicacionResult();
      this.selectListaTipoPublicacion = this.ValorServicio.Lista;

    }
    catch (error) {
      console.error('[error en GetListaTipoPublicacion] : ' + error);
    }
  }


  GetListaTipoPublicacionRenovacionResult(): any {
    return this.tipoPublicacionservice.GetListaTiposPublicacion().toPromise();
  }
  async GetListaTiposPublicacionRenovacion() {
    try {

      this.ValorServicio = await this.GetListaTipoPublicacionResult();
      this.selectListaTipoPublicacionRenovacion = this.ValorServicio.Lista;

    }
    catch (error) {
      console.error('[error en GetListaTipoPublicacion] : ' + error);
    }
  }

  GetListaTipoPublicacionSinBienvenidaResult(): any {
    return this.tipoPublicacionservice.GetListaTiposPublicacion().toPromise();
  }
  async GetListaTiposPublicacionSinBienvenida() {
    try {
      this.ValorServicio = await this.GetListaTipoPublicacionSinBienvenidaResult();
      this.selectListaTipoPublicacion = this.ValorServicio.Lista;

      // Eliminar el registro donde TipoPublicacionID = 1
      this.selectListaTipoPublicacion = this.selectListaTipoPublicacion.filter(tipo => tipo.TipoPublicacionID !== 1);

    } catch (error) {
      console.error('[error en GetListaTipoPublicacion] : ' + error);
    }
  }



  GetListaAlcancesResult(): any {
    return this.alcancesservice.GetListaAlcances().toPromise();
  }

  async GetListaAlcances() {
    try {

      this.ValorServicio = await this.GetListaAlcancesResult();
      this.selectListaAlcances = this.ValorServicio.Lista.filter((item: any) => item.TipoPublicacionID === this.publicaciones.TipoPublicacionID);


    }
    catch (error) {
      console.error('[error en GetListaAlcances] : ' + error);
    }
  }

  GetListaPeriodosPublicacionResult(): any {
    return this.periodosPublicacionservice.GetListaPeriodosPublicacion().toPromise();
  }

  async GetListaPeriodosPublicacion() {
    try {

      this.ValorServicio = await this.GetListaPeriodosPublicacionResult();

      // Primer filtro: filtra por TipoPublicacionID
      this.selectListaPeriodosPublicacion = this.ValorServicio.Lista.filter((item: any) => item.TipoPublicacionID === this.publicaciones.TipoPublicacionID);

      // Segundo filtro: si es periodo de bienvenida, aplicar filtro adicional
      if (this.periodoPublicacionBienvenida === true) {
        this.selectListaPeriodosPublicacion = this.selectListaPeriodosPublicacion.filter((item: any) => item.NombrePeriodo === "3 Meses");
      }



    }
    catch (error) {
      console.error('[error en GetListaPeriodosPublicacion] : ' + error);
    }
  }


  GetListaPeriodosPublicacionRenovacionResult(): any {
    return this.periodosPublicacionservice.GetListaPeriodosPublicacion().toPromise();
  }

  async GetListaPeriodosPublicacionRenovacion() {
    try {

      this.ValorServicio = await this.GetListaPeriodosPublicacionRenovacionResult();

      // Primer filtro: filtra por TipoPublicacionID
      this.selectListaPeriodosPublicacion = this.ValorServicio.Lista.filter((item: any) => item.TipoPublicacionID === this.publicacionesRenovacion.TipoPublicacionID);

      // Segundo filtro: si es periodo de bienvenida, aplicar filtro adicional
      if (this.periodoPublicacionBienvenida === true) {
        this.selectListaPeriodosPublicacion = this.selectListaPeriodosPublicacion.filter((item: any) => item.NombrePeriodo === "3 Meses");
      }



    }
    catch (error) {
      console.error('[error en GetListaPeriodosPublicacion] : ' + error);
    }
  }

  GetListaValorPublicacionResult(): any {
    return this.valorPublicacionService.GetListaValorPublicacion().toPromise();
  }

  async GetListaValorPublicacion() {
    try {

      this.ValorServicio = await this.GetListaValorPublicacionResult();
      this.selectListaValorPublicacion = this.ValorServicio.Lista;
    }
    catch (error) {
      console.error('[error en GetListaAlcances] : ' + error);
    }
  }



  GetListaCategoriasPrincipalesResult(): any {
    return this.categoriasprincipalesservice.GetListaCategoriasPrincipales().toPromise();
  }

  async GetListaCategoriasPrincipales() {
    try {

      this.ValorServicio = await this.GetListaCategoriasPrincipalesResult();
      this.selectCategoriasPrincipales = this.ValorServicio.Lista;

    }
    catch (error) {
      console.error('[error en GetListaCategoriaPrincipal] : ' + error);
    }
  }


  GetListaZonasResult(): any {
    return this.zonaservice.GetListaZonas().toPromise();
  }

  async GetListaZonas() {
    try {

      this.ValorServicio = await this.GetListaZonasResult();
      this.selectZonas = this.ValorServicio.Lista;

    }
    catch (error) {
      console.error('[error en GetListaZona] : ' + error);
    }
  }



  GetListaCategoriasSecundariasResult(id: number): any {
    return this.categoriassecundariasservice.GetListaCategoriasSecundariasById(id).toPromise();
  }

  async GetListaCategoriasSecundarias(id: number) {
    try {

      this.ValorServicio = await this.GetListaCategoriasSecundariasResult(id);
      this.selectCategoriasSecundarias = this.ValorServicio.Lista;

    }
    catch (error) {
      console.error('[error en GetListaCategoriaSecundaria] : ' + error);
    }
  }



  GetListaConjuntosResult(id: number): any {
    return this.conjuntosservice.GetListaConjuntosById(id).toPromise();
  }

  async GetListaConjuntos(id: number) {
    try {

      this.ValorServicio = await this.GetListaConjuntosResult(id);
      this.selectConjuntos = this.ValorServicio.Lista;

    }
    catch (error) {
      console.error('[error en GetListaConjuntos] : ' + error);
    }
  }



  async GetInfoPagoTransferencia() {
    try {


      this.ValorServicio = await this.GetListaParametrosResult("CUENTA");
      this.descripcionCuenta = this.ValorServicio.Lista[0].ParametroContenido;

      this.ValorServicio = await this.GetListaParametrosResult("CORREOPAGOS");
      this.correoRecepcionPago = this.ValorServicio.Lista[0].ParametroContenido;


      this.ValorServicio = await this.GetListaParametrosResult("WHATSAPPPAGOS");
      this.whatsappRecepcionPago = this.ValorServicio.Lista[0].ParametroContenido;


    }
    catch (error) {
      console.error('[error en GetListaAlcances] : ' + error);
    }
  }


  GetListaParametrosResult(tipo: string): any {
    return this.parametricasservice.GetListaParametrosById(tipo).toPromise();
  }

  async GetListaTipoDocumento() {
    try {

      this.ValorServicio = await this.GetListaParametrosResult("TIPODOCUMENTO");
      this.selectListaTipoDocumento = this.ValorServicio.Lista;

    }
    catch (error) {
      console.error('[error en GetListaAlcances] : ' + error);
    }
  }

  ValidarPublicaciones() {


    if (this.publicaciones.TipoPublicacionID === 0) {
      notify("Debe seleccionar un valor de la casilla Tipo Publicación", "warning", 4000);
      return false;
    }
    else if (this.publicaciones.AlcanceID === 0) {
      notify("Debe seleccionar un valor de la casilla Alcance", "warning", 4000);
      return false;
    }
    else if (this.publicaciones.PeriodoID === 0) {
      notify("Debe seleccionar un valor de la casilla Periodo", "warning", 4000);
      return false;
    }
    else if (this.selectCategoriasPrincipales && this.selectCategoriasPrincipales.length === 0) {
      notify("Debe seleccionar mínimo una categoría y subcategoría de la publicación", "warning", 4000);
      return false;
    }
    else if (this.publicaciones.MostrarUrlSitioWeb && this.publicaciones.UrlSitioWeb === '') {
      notify("Debe ingresar una dirección URL de su sitio web", "warning", 4000);
      return false;
    }



    //Se valida que al menos haya registrado una imagen asociada a la publicación
    /*  else if (this.selectListaImagenesPublicacion.length === 0) {
        notify("Debe adicionar mínimo una imagen asociada a la publicación", "warning", 4000);
        return false;
      }*/
    else
      return true;

  }



  public async CrearPublicacion() {

    this.consecutivoPublicacion = "";
    await this.InicializarFormulario();
    this.estado = 'Add';
    this.textoBtn = "Guardar Publicación"
    this.displayDatosBasicosPublicacion = true;
    this.displayPublicaciones = false;
    this.displayCrearPublicacion = false;
  }



  public async CrearPublicacionBienvenida() {

    this.periodoPublicacionBienvenida = true;

    //Se debe actualizar el codigo de bienvenida e indicar que ya fue utilizado OJOOOOOOOOOOOOO



    await this.CrearPublicacion();



  }

  public async GenerarIDPublicacion() {

  }


  public async AsignarPublicaciones() {

    // this.mostrarBotonActualizarSecundario++;

    if (this.ValidarPublicaciones()) {

      this.displayCrearPublicacion = true;
      this.displayPagarPublicacion = true;

      if (this.estado === 'Add') {


        if (this.publicaciones.MostrarValorProductoServicio === false)
          this.publicaciones.ValorProductoServicio = 0;

        this.publicaciones.PublicacionID = 0;
        this.publicaciones.FechaCreacion = this.obtenerFechaFormato();

        // Si la publicación es de Bienvenida, no se cobra , en caso contrario si

        if (this.publicaciones.TipoPublicacionID === 1) //Bienvenida
        {
          this.publicaciones.EstadoAprobacionID = 2;
          this.publicaciones.PagoRealizado = true;
          this.displayPagarPublicacion = false;

        }
        else {
          this.publicaciones.EstadoAprobacionID = 1;
          this.publicaciones.PagoRealizado = false;
          this.displayPagarPublicacion = true;

        }



        this.publicaciones.UsuarioID = Number(localStorage.getItem('userid'));

        this.publicaciones.UrlPublicacion = environment.urlDetallePublicacion + this.GenerarIDPublicacion();


        //Si aplica codigo de descuento de bienvenida se aprueba la publicación

        if (this.periodoPublicacionBienvenida === true) {
          this.publicaciones.AplicoDescuento = true;
          this.publicaciones.TipoDescuento = "Bienvenida";
          this.publicaciones.PagoRealizado = true;
          this.publicaciones.EstadoAprobacionID = 2;
          this.displayPagarPublicacion = false;
        }
        else {
          this.publicaciones.AplicoDescuento = false;
          this.publicaciones.TipoDescuento = '';
          // this.displayPagarPublicacion = true;
        }
        await this.publicacionesservice.PostPublicaciones(this.publicaciones).subscribe(async result => {

          this.nuevoIDPublicacion = result.PublicacionID;
          if (result.success == false) {
            notify("Error al crear publicación", "error", 4000);
          }
          else {
            //mostrar modal


            const fechaN = new Date(this.publicaciones.FechaCreacion);
            const yearN = fechaN.getFullYear();
            const monthN = String(fechaN.getMonth() + 1).padStart(2, '0'); // Aseguramos que el mes sea de 2 dígitos
            const dayN = String(fechaN.getDate()).padStart(2, '0'); // Aseguramos que el día sea de 2 dígitos


            this.consecutivoPublicacion = yearN + monthN + dayN + this.nuevoIDPublicacion.toString();
            this.publicaciones.UrlPublicacion = environment.urlDetallePublicacion + this.consecutivoPublicacion;
            this.publicaciones.PublicacionID = result.PublicacionID;


            await this.publicacionesservice.PutPublicacionesById(this.publicaciones).subscribe(async result => {

              if (result.success == false) {
                notify("Error al actualizar la publicación", "error", 4000);
              }
              else {

                this.open(this.creacionPublicacion);
                notify("Se ha creado la publicación con el ID : " + this.consecutivoPublicacion + " correctamente ", "success", 6000);

                //Se actualiza el codigo de descuento una vez ha sido creada la publicación SI APLICA
                if (this.periodoPublicacionBienvenida === true) {
                  await this.pagosservice.PutDescuentoById(this.descuentos).subscribe(async result => {

                    if (result.success == false) {
                      notify("Error al actualizar descuento", "error", 4000);
                    }

                  },
                    error => {
                      console.error(error);
                    }
                  );

                }

                this.estado = 'Upd';

                //Se obtiene la información de la publicación que se acabó de crear
                this.publicaciones.PublicacionID = this.nuevoIDPublicacion;

                this.InicializarGrillas();

                await this.VisualizarImagenesPublicacion();
                await this.VisualizarVideosPublicacion();
                await this.VisualizarCategoriasPublicacion();
                await this.VisualizarConjuntosPublicacion();

                if (this.perfil === 1) //Administrador (puede ver todas las publicaciones)
                  await this.GetListaPublicaciones();
                else
                  await this.GetListaPublicacionesUsuario(this.usuario);

                this.textoBtn = "Actualizar Publicación";

                this.displayPublicaciones = true;

                let emailParametros = new EmailParametros();
                emailParametros.PublicacionID = this.publicaciones.PublicacionID;
                emailParametros.Consecutivo = this.consecutivoPublicacion;

                if (this.periodoPublicacionBienvenida === true)
                  emailParametros.Tipo = 2; //Correo de Creación Publicación Gratuita
                else
                  emailParametros.Tipo = 1; //Correo de Creación Publicación con Pago

                //Enviar Correo de Bienvenida Creación de Publicación
                await this.emailservice.EnviarEmail(emailParametros).subscribe({
                  next: (result) => {
                    if (result.Exito) {

                    } else {
                      //notify(result.Mensaje, "error", 8000);
                      this.logger.log(result.Mensaje);
                      this.logger.log(result.Error);
                    }
                  },
                  error: (error) => {
                    //notify("Error al enviar correo: " + error.message, "error", 8000);
                    console.error("Error al enviar correo:", error);
                    console.error("Error al enviar correo:", error.message);

                  }
                });
              }
            },
              error => {
                console.error(error);
              }
            );






          }
        },
          error => {
            console.error(error);
          }
        );
      }
      else if (this.estado === 'Upd') {

        if (this.publicaciones.MostrarValorProductoServicio === false)
          this.publicaciones.ValorProductoServicio = 0;

        //this.publicaciones.UsuarioID = Number(localStorage.getItem('userid'));
        this.publicaciones.FechaCreacion = this.obtenerFechaFormato();


        await this.publicacionesservice.PutPublicacionesById(this.publicaciones).subscribe(async result => {

          if (result.success == false) {
            notify("Error al actualizar la publicación", "error", 4000);
          }
          else {
            notify("Se ha actualizado la publicación correctamente ", "success", 4000);
            this.InicializarFormulario();
            if (this.perfil === 1) //Administrador (puede ver todas las publicaciones)
              await this.GetListaPublicaciones();
            else
              await this.GetListaPublicacionesUsuario(this.usuario);

            this.displayPublicaciones = true;
          }
        },
          error => {
            console.error(error);
          }
        );
      }
    }
  }

  public ValidarRenovacion() {
    //Si escogio la opción de Cambiar de Plan (validar el check marcado de esa opción, validar que haya selecciondo el tipo, y periodo)

    //Se debe validar que haya marcado un check en las opciones de aplicar cambios

    if (!this.tiporenovacion) {
      notify("Debe indicar que opción desea realizar, por favor marque la casilla que requiere", "error", 4000);
      return false;
    }
    else if (this.tiporenovacion === 'opcion1' && !this.formaaplicarcambioplan) //Si escogio la opción de Renvoar Plan Actual (validar el check marcado de esa opción)
    {
      notify("Debe indicar como desea aplicar el cambio de plan", "error", 4000);
      return false;
    }
    else if (this.tiporenovacion === 'opcion2' && !this.publicacionesRenovacion.TipoPublicacionID) //Si escogio la opción de Renvoar Plan Actual (validar el check marcado de esa opción)
    {
      notify("Debe seleccionar un tipo de plan", "error", 4000);
      return false;
    }
    else if (this.tiporenovacion === 'opcion2' && !this.publicacionesRenovacion.AlcanceID) //Si escogio la opción de Renvoar Plan Actual (validar el check marcado de esa opción)
    {
      notify("Debe seleccionar un alcance", "error", 4000);
      return false;
    }
    else if (this.tiporenovacion === 'opcion2' && !this.publicacionesRenovacion.PeriodoID) //Si escogio la opción de Renvoar Plan Actual (validar el check marcado de esa opción)
    {
      notify("Debe seleccionar un periodo", "error", 4000);
      return false;
    }
    else if (this.tiporenovacion === 'opcion2' && !this.formaaplicarcambioplan) //Si escogio la opción de Renvoar Plan Actual (validar el check marcado de esa opción)
    {
      notify("Debe indicar como desea aplicar el cambio de plan", "error", 4000);
      return false;
    }

    else
      return true;
  }

  public async RenovarPublicaciones() {

    this.motivoPago = 'Renovacion';

    //Se debe validar :
    if (this.ValidarRenovacion()) {


      if (this.tiporenovacion === 'opcion1')
        this.publicacionRenovacionDetalle.EsRenovacion = true;

      if (this.tiporenovacion === 'opcion2')
        this.publicacionRenovacionDetalle.EsCambioPlan = true;

      if (this.formaaplicarcambioplan === 'opcion1')
        this.publicacionRenovacionDetalle.AplicoInmediato = true;

      if (this.formaaplicarcambioplan === 'opcion2')
        this.publicacionRenovacionDetalle.AplicoFinVigencia = true;


      //Si se ha diligenciado todo, entonces se procede a enviar a Pagar Renovacion y enviar variables para que el backend aplique los cambios , sea de renovación del mismo 
      //plan o el cambio de plan y su nueva vigencia


      this.PagarRenovacionPublicacion();

    }
    else {
      notify("Debe ingresar los valores requeridos", "error", 4000);
    }






  }


  GetListaPublicacionesResult(): any {
    return this.publicacionesservice.GetListaPublicaciones().toPromise();
  }

  async GetListaPublicaciones() {
    try {

      this.ValorServicio = await this.GetListaPublicacionesResult();

      this.selectListaPublicacion = this.ValorServicio.Lista;

      this.selectListaPublicacion.forEach(publicacion => {

        const fechaActual = new Date();
        const fechaVencimiento = new Date(publicacion.FechaVencimiento);

        // Normaliza ambas fechas a medianoche para evitar diferencias en las horas
        fechaActual.setHours(0, 0, 0, 0);
        fechaVencimiento.setHours(0, 0, 0, 0);

        // Calcula la diferencia en milisegundos
        const diferenciaMilisegundos = fechaVencimiento.getTime() - fechaActual.getTime();

        // Convierte la diferencia de milisegundos a días
        const diasRestantes = Math.ceil(diferenciaMilisegundos / (1000 * 60 * 60 * 24));

        // Asigna el valor calculado a la propiedad DiasHastaVencimiento
        publicacion.DiasHastaVencimiento = diasRestantes;




        // Verifica si faltan 10 días o menos
        if (diasRestantes <= 10) {
          publicacion.ProximoAVencer = true;

        } else {
          publicacion.ProximoAVencer = false;
        }
      });

      ///AHora eliminar duplicados
      const idsUnicos = new Set();

      this.selectListaPublicacion = this.ValorServicio.Lista.filter(publicacion => {
        const esDuplicado = idsUnicos.has(publicacion.PublicacionID);
        idsUnicos.add(publicacion.PublicacionID);
        return !esDuplicado;
      });




    } catch (error) {
      console.error('[error en GetListaPublicaciones] : ' + error);
    }
  }




  GetListaPublicacionesUsuarioResult(usuario: number): any {
    return this.publicacionesservice.GetListaPublicacionesByUser(usuario).toPromise();
  }

  async GetListaPublicacionesUsuario(usuario: number) {
    try {
      this.ValorServicio = await this.GetListaPublicacionesUsuarioResult(usuario);



      this.selectListaPublicacion = this.ValorServicio.Lista;
      this.selectListaPublicacion.forEach(publicacion => {

        const fechaActual = new Date();
        const fechaVencimiento = new Date(publicacion.FechaVencimiento);

        // Normaliza ambas fechas a medianoche para evitar diferencias en las horas
        fechaActual.setHours(0, 0, 0, 0);
        fechaVencimiento.setHours(0, 0, 0, 0);

        // Calcula la diferencia en milisegundos
        const diferenciaMilisegundos = fechaVencimiento.getTime() - fechaActual.getTime();

        // Convierte la diferencia de milisegundos a días
        const diasRestantes = Math.ceil(diferenciaMilisegundos / (1000 * 60 * 60 * 24));

        // Asigna el valor calculado a la propiedad DiasHastaVencimiento
        publicacion.DiasHastaVencimiento = diasRestantes;




        // Verifica si faltan 10 días o menos
        if (diasRestantes <= 10) {
          publicacion.ProximoAVencer = true;

        } else {
          publicacion.ProximoAVencer = false;
        }
      });




      ///AHora eliminar duplicados
      const idsUnicos = new Set();

      this.selectListaPublicacion = this.ValorServicio.Lista.filter(publicacion => {
        const esDuplicado = idsUnicos.has(publicacion.PublicacionID);
        idsUnicos.add(publicacion.PublicacionID);
        return !esDuplicado;
      });




    }
    catch (error) {
      console.error('[error en GetListaPublicaciones] : ' + error);
    }
  }

  actualizarConteos(): void {
    this.mostrarDashBoardPublicaciones = true;
    this.dashboardCounts.total = this.selectListaPublicacion.length;
    this.dashboardCounts.aprobadas = this.selectListaPublicacion.filter(p => p.EstadoAprobacionID === 2).length;
    this.dashboardCounts.vencidas = this.selectListaPublicacion.filter(p => p.EstadoAprobacionID === 4).length;
    this.dashboardCounts.borrador = this.selectListaPublicacion.filter(p => p.EstadoAprobacionID === 1).length;
    this.dashboardCounts.rechazadas = this.selectListaPublicacion.filter(p => p.EstadoAprobacionID === 3).length;
  }



  GetListaPublicacionesUsuarioBienvenidaResult(usuario: number): any {
    return this.publicacionesservice.GetListaPublicacionesByUser(usuario).toPromise();
  }

  async GetListaPublicacionesUsuarioBienvenida(usuario: number) {
    try {

      this.ValorServicio = await this.GetListaPublicacionesUsuarioBienvenidaResult(usuario);
      this.selectListaPublicacionBienvenida = this.ValorServicio.Lista;

      this.selectListaPublicacion.forEach(publicacion => {

        const fechaActual = new Date();
        const fechaVencimiento = new Date(publicacion.FechaVencimiento);

        // Normaliza ambas fechas a medianoche para evitar diferencias en las horas
        fechaActual.setHours(0, 0, 0, 0);
        fechaVencimiento.setHours(0, 0, 0, 0);

        // Calcula la diferencia en milisegundos
        const diferenciaMilisegundos = fechaVencimiento.getTime() - fechaActual.getTime();

        // Convierte la diferencia de milisegundos a días
        const diasRestantes = Math.ceil(diferenciaMilisegundos / (1000 * 60 * 60 * 24));

        // Asigna el valor calculado a la propiedad DiasHastaVencimiento
        publicacion.DiasHastaVencimiento = diasRestantes;




        // Verifica si faltan 10 días o menos
        if (diasRestantes <= 10) {
          publicacion.ProximoAVencer = true;

        } else {
          publicacion.ProximoAVencer = false;
        }
      });


      // Ahora eliminar duplicados
      const idsUnicos = new Set();

      this.selectListaPublicacionBienvenida = this.ValorServicio.Lista.filter(publicacion => {
        const esDuplicado = idsUnicos.has(publicacion.PublicacionID);
        idsUnicos.add(publicacion.PublicacionID);
        return !esDuplicado;
      });


      // Filtrar registros donde TipoPublicacionID = 1
      //this.selectListaPublicacionBienvenida = this.selectListaPublicacionBienvenida.filter(publicacion => publicacion.TipoPublicacionID === 1);

    } catch (error) {
      console.error('[error en GetListaPublicaciones] : ' + error);
    }
  }



  async GetPublicacionesByIdResult(id: number) {
    return this.publicacionesservice.GetPublicacionesById(id).toPromise();
  }


  async DelImagenPublicacionByIdResult(id: number) {
    return this.imagenesPublicacionservice.DelImagenPublicacionById(id).toPromise();
  }

  async DelImagenPublicacionById(id: number) {
    await this.DelImagenPublicacionByIdResult(id);
  }

  DelVideoPublicacionByIdResult(id: number) {
    return this.videosPublicacionservice.DelVideoPublicacionById(id).toPromise();
  }

  async DelVideoPublicacionById(id: number) {
    await this.DelVideoPublicacionByIdResult(id);
  }


  DelCategoriaPublicacionByIdResult(id: number) {
    return this.categoriaPublicacionservice.DelCategoriaPublicacionById(id).toPromise();
  }

  async DelCategoriaPublicacionById(id: number) {
    await this.DelCategoriaPublicacionByIdResult(id);
  }


  DelConjuntoPublicacionByIdResult(id: number) {
    return this.conjuntoPublicacionservice.DelConjuntoPublicacionById(id).toPromise();
  }

  async DelConjuntoPublicacionById(id: number) {
    await this.DelConjuntoPublicacionByIdResult(id);
  }


  GetListaImagenesPublicacionResult(id: number): any {
    return this.imagenesPublicacionservice.GetListaImagenesPublicacion(id).toPromise();
  }

  async GetListaImagenesPublicacion(idPublicacion: number) {
    try {
      this.ValorServicio = await this.GetListaImagenesPublicacionResult(idPublicacion);



      this.selectListaImagenesPublicacion = this.ValorServicio.Lista;
      this.displayGuardarImagen = false;
    }
    catch (error) {
      console.error('[error en GetListaPublicaciones] : ' + error);
    }
  }


  GetListaVideosPublicacionResult(id: number): any {
    return this.videosPublicacionservice.GetListaVideosPublicacionById(id).toPromise();
  }

  async GetListaVideosPublicacion(idPublicacion: number) {
    try {
      this.ValorServicio = await this.GetListaVideosPublicacionResult(idPublicacion);



      this.selectListaVideosPublicacion = this.ValorServicio.Lista;
      this.displayGuardarVideo = false;
    }
    catch (error) {
      console.error('[error en GetListaVideos] : ' + error);
    }
  }


  GetListaCategoriasPublicacionResult(id: number): any {
    return this.categoriaPublicacionservice.GetListaCategoriaPublicacionesById(id).toPromise();
  }

  async GetListaCategoriasPublicacion(idPublicacion: number) {
    try {

      this.ValorServicio = await this.GetListaCategoriasPublicacionResult(idPublicacion);

      this.selectListaCategoriasPublicacion = this.ValorServicio.Lista;
      this.displayGuardarCategoria = false;
    }
    catch (error) {
      console.error('[error en GetListaCategorias] : ' + error);
    }
  }

  GetListaConjuntosPublicacionResult(id: number): any {
    return this.conjuntoPublicacionservice.GetListaConjuntoPublicacionesById(id).toPromise();
  }

  async GetListaConjuntosPublicacion(idPublicacion: number) {
    try {

      this.ValorServicio = await this.GetListaConjuntosPublicacionResult(idPublicacion);



      this.selectListaConjuntoPublicacion = this.ValorServicio.Lista;
      this.displayGuardarConjunto = false;
    }
    catch (error) {
      console.error('[error en GetListaConjunto] : ' + error);
    }
  }


  async GetPublicacionesById(id: number) {
    try {
      this.ValorServicio = await this.GetPublicacionesByIdResult(id);
      return this.ValorServicio;
    }
    catch (error) {
      console.error('[error en GetListaPublicaciones] : ' + error);
    }
  }

  DelPublicacionByIdResult(id: number) {
    return this.publicacionesservice.DelPublicacionById(id).toPromise();
  }

  async DelPublicacionById(id: number) {
    await this.DelPublicacionByIdResult(id);
  }


  DuplicacionPublicacionByIdResult(id: number) {
    let duplicado: DuplicacionPublicacion = new DuplicacionPublicacion();
    duplicado.PublicacionID = id;
    return this.publicacionesservice.AddDuplicacionPublicacion(duplicado).toPromise();
  }

  async DuplicacionPublicacionById(id: number) {
    let resultadoDuplicacion = await this.DuplicacionPublicacionByIdResult(id);
    await this.EditarPublicacion(resultadoDuplicacion.Result.NuevoPublicacionID);

  }



  async GetPuntuacionPublicacionesByIdResult(id: number, usuarioIDComento: number) {
    return this.puntuacionPublicacionservice.GetPuntuacionPublicacionesById(id, usuarioIDComento).toPromise();
  }


  async GetPuntuacionClasificadoById(id: number, usuarioIDComento: number) {
    try {
      this.ValorServicio = await this.GetPuntuacionPublicacionesByIdResult(id, usuarioIDComento);
      return this.ValorServicio;
    }
    catch (error) {
      console.error('[error en GetPuntuacionClasificadoById] : ' + error);
    }
  }




  async EditarPublicacion(id: number) {


    // this.mostrarBotonActualizarSecundario++;
    this.popupVisible = false;
    let publicaciones: Publicaciones;

    publicaciones = await this.GetPublicacionesById(id);


    this.publicaciones = publicaciones;


    const fecha = new Date(this.publicaciones.FechaCreacion.substring(0, 10));
    const year = fecha.getFullYear();
    const month = String(fecha.getMonth() + 1).padStart(2, '0'); // Aseguramos que el mes sea de 2 dígitos
    const day = String(fecha.getDate()).padStart(2, '0'); // Aseguramos que el día sea de 2 dígitos


    this.consecutivoPublicacion = year + month + day + this.publicaciones.PublicacionID.toString();



    if (this.publicaciones.RangoPrecios)
      this.usarRangoPrecios = true;



    this.estado = 'Upd';
    this.textoBtn = "Actualizar Publicación"

    this.displayCrearPublicacion = false;
    this.displayDatosBasicosPublicacion = true;
    this.displayPublicaciones = false;

    //Se muestran las imagenes Asociadas
    await this.VisualizarImagenesPublicacion();


    //Se muestran los Videos Asociados
    await this.VisualizarVideosPublicacion();

    //Se muestran las Categorias
    await this.VisualizarCategoriasPublicacion();


    //Se muestran los Conjuntos
    await this.VisualizarConjuntosPublicacion();





    if (this.publicaciones.PagoRealizado === true || this.publicaciones.EstadoAprobacionID === 2) {
      this.esEditable = false;
      this.displayPagarPublicacion = false;

    }
    else {
      this.esEditable = true;
      this.displayPagarPublicacion = true;

    }


    this.cdr.detectChanges();

    await this.CalcularCostoPublicacion();
    //Se muestra el total de Costo

  }

  open(content: any) {
    this.modalService.open(content, { backdrop: 'static', keyboard: false, size: 'lg', windowClass: 'modal-dialog-centered' });
  }



  ConfirmacionEliminarVideo() {
    this.open(this.deleteVideo);
    this.idRecordDelete = this.selectListaVideosPublicacion[0].VideoID;
  }


  ConfirmacionEliminarCategoria(id: number) {
    this.open(this.deleteCategoria);
    this.idRecordDelete = id;
  }

  ConfirmacionEliminarConjunto(id: number) {
    this.open(this.deleteConjunto);
    this.idRecordDelete = id;
  }


  ConfirmacionEliminarPublicacion(id: number) {
    this.popupVisible = false;
    this.open(this.deletePublicacion);
    this.idRecordDelete = id;
  }

  ConfirmacionDuplicacionPublicacion(id: number) {
    this.popupVisible = false;
    this.open(this.duplicatePublicacion);
    this.idRecordDuplicate = id;
  }


  async ConfirmacionRenovacionPublicacion(id: number) {

    this.popupVisible = false;

    this.filteredRadioOptions = [];
    this.filteredListaTipoPublicacionRenovacion = [];

    this.tiporenovacion = "";
    this.formaaplicarcambioplan = "";
    this.displayCalculoPublicacionRenovacion = false;




    //Se obtiene la información de la Publicación y se precarga la información de plan, alcance y periodo
    this.idRecordRenovate = id;

    let publicaciones: Publicaciones;

    publicaciones = await this.GetPublicacionesById(this.idRecordRenovate);

    this.publicaciones = publicaciones;
    this.publicacionesRenovacion = publicaciones;


    this.filteredRadioOptions = this.radioOptionsTipoRenovacion.filter(option => {
      // Si publicaciones.tipoPublicacionID es 1, excluir "opcion1"
      return !(this.publicaciones.TipoPublicacionID === 1 && option.value === 'opcion1');
    });

    await this.GetListaTiposPublicacionRenovacion();
    this.filteredListaTipoPublicacionRenovacion = this.selectListaTipoPublicacionRenovacion.filter(plan => {
      return plan.TipoPublicacionID > this.publicaciones.TipoPublicacionID;
    });

    /* if (this.publicaciones.TipoPublicacionID === 1)
       this.publicaciones.TipoPublicacionID = 0;*/

    const fecha = new Date(publicaciones.FechaCreacion.substring(0, 10));
    const year = fecha.getFullYear();
    const month = String(fecha.getMonth() + 1).padStart(2, '0'); // Aseguramos que el mes sea de 2 dígitos
    const day = String(fecha.getDate()).padStart(2, '0'); // Aseguramos que el día sea de 2 dígitos

    this.consecutivoPublicacion = year + month + day + this.publicaciones.PublicacionID.toString();

    await this.MostrarInformacionPlanActual();
    this.displayInformacionRenovacion = false;

    this.displayCalculoPublicacionRenovacion = false;

    this.open(this.renovarPublicacion);



  }


  async EliminarVideo() {
    this.videoPublicacion.RutaVideo = "";
    this.esEditableVideo = true;
    await this.DelVideoPublicacionById(this.idRecordDelete);
    await this.GetListaVideosPublicacion(this.publicaciones.PublicacionID);
  }

  async EliminarCategoria() {
    await this.DelCategoriaPublicacionById(this.idRecordDelete);
    await this.GetListaCategoriasPublicacion(this.publicaciones.PublicacionID);
  }

  async EliminarConjunto() {
    await this.DelConjuntoPublicacionById(this.idRecordDelete);
    await this.GetListaConjuntosPublicacion(this.publicaciones.PublicacionID);
  }


  async EliminarPublicacion() {

    //AQUI
    await this.DelPublicacionById(this.idRecordDelete);

    //await this.GetListaPublicaciones();
    if (this.perfil === 1) //Administrador (puede ver todas las publicaciones)
      await this.CargarPublicaciones();
    else //Usuario Publicador
      await this.CargarPublicacionesUser(this.usuario);

    this.displayDatosBasicosPublicacion = false;
    this.displayImagenesPublicacion = false;

  }

  async DuplicarPublicacion() {
    await this.ejecutarNgOnInit();
    await this.DuplicacionPublicacionById(this.idRecordDuplicate);
    await this.GetListaPublicaciones();

    //aqui se llama al codigo de crear publicacion
    this.estado = 'Upd';
    this.textoBtn = "Actualizar Publicación"
    this.displayDatosBasicosPublicacion = true;
    this.displayPublicaciones = false;
    this.displayCrearPublicacion = false;
    this.displayPagarPublicacion = true;


  }

  async EstablecerImagenPrincipal() {

    //Se establece la imagen principal de la Miniatura


    this.imagenPublicacion.ImagenID = this.idRecordImagenPrincipal;
    this.imagenPublicacion.PublicacionID = this.publicaciones.PublicacionID;
    this.imagenPublicacion.Orden = 1;
    this.imagenPublicacion.ImagenPrincipal = true;


    await this.imagenesPublicacionservice.PutUpdImagenesPublicacion(this.imagenPublicacion).subscribe(async result => {
      if (result.success == false) {
        notify("Error al actualizar imágenes de publicación", "error", 4000);

      } else {
        notify("Se ha asignado la imagen de la publicación correctamente", "success", 4000);
      }
    },
      error => {
        console.error(error);
      });

  }

  async CalcularCostoPublicacion() {



    //Se obtiene la información del tipo de Publicación elegido
    let tipoPublicacion = this.publicaciones.TipoPublicacionID;

    let valorDescuentoBono = 0;



    this.valorPublicacion = 0;

    //   await this.CargarAlcances();
    //   await this.CargarPeriodosPublicacion();


    if (tipoPublicacion != 0) {
      // Buscar el objeto en la lista selectListaTipoPublicacion
      this.tipoPublicacionEncontrado = this.selectListaTipoPublicacion.find(tipo => tipo.TipoPublicacionID === tipoPublicacion);



      let tipoAlcance = this.publicaciones.AlcanceID;
      let tipoAlcanceEncontrado = this.selectListaAlcances.find(tipo => tipo.AlcanceID === tipoAlcance);

      if (tipoAlcance != 0) {

        let tipoPeriodo = this.publicaciones.PeriodoID;
        let tipoPeriodoEncontrado = this.selectListaPeriodosPublicacion.find(tipo => tipo.PeriodoID === tipoPeriodo);


        if (tipoPeriodo != 0 && tipoPeriodoEncontrado != undefined) {

          let valorPeriodoEncontrado = this.selectListaValorPublicacion.find(tipo => tipo.TipoPublicacionID === tipoPublicacion);

          if (valorPeriodoEncontrado !== undefined) {

            this.valorBase = valorPeriodoEncontrado.ValorBasePublicacion;
            this.valorNeto = this.valorBase;

            //Fin validacion de codigo de descuento

            this.valorNetoPrevio = ((this.valorBase / 30) * tipoPeriodoEncontrado?.Dias!);
            this.valorNeto = ((this.valorNeto / 30) * tipoPeriodoEncontrado?.Dias!);

            if (tipoPeriodoEncontrado?.PorcentajeValorAlcance !== undefined && tipoPeriodoEncontrado?.PorcentajeValorAlcance != 0) {
              this.valorDescuento = (this.valorNeto * tipoPeriodoEncontrado.PorcentajeValorAlcance) / 100;
              this.valorNeto = this.valorNeto - this.valorDescuento;
            }

            //Codigo para validar si ha aplicado codigo de descuento
            if (this.descuentos.DescuentoCodeID !== '') {
              valorDescuentoBono = Math.round(((this.valorNeto * this.descuentos.PorcentajeDescuento) / 100));
              this.valorNeto = this.valorNeto - valorDescuentoBono;
            }

            this.valorIVA = Math.round(this.valorNeto * 0.19);

            this.valorPublicacion = Math.round(this.valorNeto + this.valorIVA);

            let FechaVencimiento = this.sumarDiasYFormatear(tipoPeriodoEncontrado!.Dias);
            this.publicaciones.FechaVencimiento = FechaVencimiento;


            //Se valida si la publicacion es de Bienvenida
            if (this.periodoPublicacionBienvenida === true) {
              this.valorPublicacion = 0;
            }

            this.infoPublicacion = "Has seleccionado el Tipo de Publicación <b>" + this.tipoPublicacionEncontrado?.NombreTipoPublicacion + "</b>, que incluye: <i>" + this.tipoPublicacionEncontrado?.DescripcionTipoPublicacion + "</i>.<br> Su Alcance estará definido para <b>" + tipoAlcanceEncontrado?.NombreAlcance + "</b>.<br>";
            this.infoPublicacion += "Su vigencia será de <b><i>" + tipoPeriodoEncontrado?.NombrePeriodo + " ( " + tipoPeriodoEncontrado?.Dias + " días ) </i></b>.<br>";
            this.infoPublicacion += "La publicación expirará el : <b>" + FechaVencimiento + "<b><br>";
            this.infoPublicacion += "<div style='text-align: right;'>"; // Inicia el div para alinear a la derecha
            this.infoPublicacion += "Valor Neto : <b>" + this.valorNetoPrevio.toLocaleString('es-CO', { style: 'currency', currency: 'COP', minimumFractionDigits: 0, maximumFractionDigits: 0 }) + "<b><br>";

            if (tipoPeriodoEncontrado!.PorcentajeValorAlcance > 0)
              this.infoPublicacion += "Descuento del : <b>" + tipoPeriodoEncontrado!.PorcentajeValorAlcance + "% por valor de : " + this.valorDescuento.toLocaleString('es-CO', { style: 'currency', currency: 'COP', minimumFractionDigits: 0, maximumFractionDigits: 0 }) + "<b><br>";

            if (this.descuentos.DescuentoCodeID !== '' && this.descuentos.PorcentajeDescuento > 0)
              this.infoPublicacion += "Se aplicó Código de Descuento : <b>" + this.descuentos.DescuentoCodeID + " de " + this.descuentos.PorcentajeDescuento + "% por valor de : " + valorDescuentoBono.toLocaleString('es-CO', { style: 'currency', currency: 'COP', minimumFractionDigits: 0, maximumFractionDigits: 0 }) + "<b><br>";

            this.infoPublicacion += "Subtotal : <b>" + this.valorNeto.toLocaleString('es-CO', { style: 'currency', currency: 'COP', minimumFractionDigits: 0, maximumFractionDigits: 0 }) + "<b><br>";
            this.infoPublicacion += "IVA 19 % : " + this.valorIVA.toLocaleString('es-CO', { style: 'currency', currency: 'COP', minimumFractionDigits: 0, maximumFractionDigits: 0 }) + "<b><br>";
            this.infoPublicacion += "Total a pagar Publicación: <b><font color='green'> " + this.valorPublicacion.toLocaleString('es-CO', { style: 'currency', currency: 'COP', minimumFractionDigits: 0, maximumFractionDigits: 0 }) + " </font></b>";
            this.infoPublicacion += "</div>"; // Cierra el div

            /*  if (tipoPublicacion !== 1)
                this.displayPagarPublicacion = true;*/

            if (this.periodoPublicacionBienvenida === true) {
              this.infoPublicacion += "<br> <b><font color= 'green'> PUBLICACIÓN GRATUITA </font></b>";
            }
            this.displayCalculoPublicacion = true;
          }

          //Se asigna el valor
          localStorage.setItem('valorPublicacion', this.valorPublicacion.toString());
          localStorage.setItem('valorIVAPublicacion', this.valorIVA.toString());
          localStorage.setItem('valorNetPublicacion', this.valorNeto.toString());
        }


      }


    }





  }

  async CalcularCostoPublicacionRenovacion() {


    //Se obtiene la información del tipo de Publicación elegido
    let tipoPublicacion = this.publicaciones.TipoPublicacionID;
    let valorDescuentoBono = 0;



    this.valorPublicacion = 0;

    await this.CargarAlcances();
    await this.CargarPeriodosPublicacion();

    if (tipoPublicacion != 0) {
      // Buscar el objeto en la lista selectListaTipoPublicacion
      this.tipoPublicacionEncontrado = this.selectListaTipoPublicacion.find(tipo => tipo.TipoPublicacionID === tipoPublicacion);

      let tipoAlcance = this.publicaciones.AlcanceID;
      let tipoAlcanceEncontrado = this.selectListaAlcances.find(tipo => tipo.AlcanceID === tipoAlcance);

      if (tipoAlcance != 0) {

        let tipoPeriodo = this.publicaciones.PeriodoID;
        let tipoPeriodoEncontrado = this.selectListaPeriodosPublicacion.find(tipo => tipo.PeriodoID === tipoPeriodo);
        if (tipoPeriodo != 0) {

          let valorPeriodoEncontrado = this.selectListaValorPublicacion.find(tipo => tipo.TipoPublicacionID === tipoPublicacion);

          if (valorPeriodoEncontrado !== undefined) {

            this.valorBase = valorPeriodoEncontrado.ValorBasePublicacion;
            this.valorNeto = this.valorBase;

            //Fin validacion de codigo de descuento

            this.valorNetoPrevio = ((this.valorBase / 30) * tipoPeriodoEncontrado?.Dias!);
            this.valorNeto = ((this.valorNeto / 30) * tipoPeriodoEncontrado?.Dias!);

            if (tipoPeriodoEncontrado?.PorcentajeValorAlcance !== undefined && tipoPeriodoEncontrado?.PorcentajeValorAlcance != 0) {
              this.valorDescuento = (this.valorNeto * tipoPeriodoEncontrado.PorcentajeValorAlcance) / 100;
              this.valorNeto = this.valorNeto - this.valorDescuento;
            }


            //Codigo para validar si ha aplicado codigo de descuento
            if (this.descuentos.DescuentoCodeID !== '') {
              valorDescuentoBono = Math.round(((this.valorNeto * this.descuentos.PorcentajeDescuento) / 100));
              this.valorNeto = this.valorNeto - valorDescuentoBono;
            }


            this.valorIVA = Math.round(this.valorNeto * 0.19);

            this.valorPublicacion = Math.round(this.valorNeto + this.valorIVA);

            let FechaVencimiento = this.sumarDiasYFormatear(tipoPeriodoEncontrado!.Dias);
            this.publicaciones.FechaVencimiento = FechaVencimiento;


            //Se valida si la publicacion es de Bienvenida
            if (this.periodoPublicacionBienvenida === true) {
              this.valorPublicacion = 0;
            }

            this.infoPublicacionRenovacion = "El Tipo de Plan seleccionado es <b>" + this.tipoPublicacionEncontrado?.NombreTipoPublicacion + "</b>, que incluye: <i>" + this.tipoPublicacionEncontrado?.DescripcionTipoPublicacion + "</i>.<br> Su Alcance estará definido para <b>" + tipoAlcanceEncontrado?.NombreAlcance + "</b>.<br>";
            this.infoPublicacionRenovacion += "Su vigencia será de <b><i>" + tipoPeriodoEncontrado?.NombrePeriodo + " ( " + tipoPeriodoEncontrado?.Dias + " días ) </i></b>.<br>";

            this.infoPublicacionRenovacion += "Valor Neto : <b>" + this.valorNetoPrevio.toLocaleString('es-CO', { style: 'currency', currency: 'COP', minimumFractionDigits: 0, maximumFractionDigits: 0 }) + "<b><br>";

            if (tipoPeriodoEncontrado!.PorcentajeValorAlcance > 0)
              this.infoPublicacionRenovacion += "Descuento del : <b>" + tipoPeriodoEncontrado!.PorcentajeValorAlcance + "% por valor de : " + this.valorDescuento.toLocaleString('es-CO', { style: 'currency', currency: 'COP', minimumFractionDigits: 0, maximumFractionDigits: 0 }) + "<b><br>";


            if (this.descuentos.DescuentoCodeID !== '' && this.descuentos.PorcentajeDescuento > 0)
              this.infoPublicacionRenovacion += "Se aplicó Código de Descuento : <b>" + this.descuentos.DescuentoCodeID + " de " + this.descuentos.PorcentajeDescuento + "% por valor de : " + valorDescuentoBono.toLocaleString('es-CO', { style: 'currency', currency: 'COP', minimumFractionDigits: 0, maximumFractionDigits: 0 }) + "<b><br>";


            this.infoPublicacionRenovacion += "Subtotal : <b>" + this.valorNeto.toLocaleString('es-CO', { style: 'currency', currency: 'COP', minimumFractionDigits: 0, maximumFractionDigits: 0 }) + "<b><br>";
            this.infoPublicacionRenovacion += "IVA 19 % : " + this.valorIVA.toLocaleString('es-CO', { style: 'currency', currency: 'COP', minimumFractionDigits: 0, maximumFractionDigits: 0 }) + "<b><br>";
            this.infoPublicacionRenovacion += "Total a pagar Publicación: <b><font color='green'> " + this.valorPublicacion.toLocaleString('es-CO', { style: 'currency', currency: 'COP', minimumFractionDigits: 0, maximumFractionDigits: 0 }) + " </font></b>";

            if (tipoPublicacion !== 1)
              this.displayPagarPublicacionRenovacion = true;

            this.displayCalculoPublicacionRenovacion = true;
          }


        }

      }
    }


    //Se asigna el valor
    localStorage.setItem('valorPublicacion', this.valorPublicacion.toString());
    localStorage.setItem('valorIVAPublicacion', this.valorIVA.toString());
    localStorage.setItem('valorNetPublicacion', this.valorNeto.toString());


  }

  async MostrarInformacionPlanActual() {


    //Se obtiene la información del tipo de Publicación elegido
    let tipoPublicacion = this.publicaciones.TipoPublicacionID;

    if (tipoPublicacion != 0) {
      // Buscar el objeto en la lista selectListaTipoPublicacion
      this.tipoPublicacionEncontrado = this.selectListaTipoPublicacion.find(tipo => tipo.TipoPublicacionID === tipoPublicacion);

      let tipoAlcance = this.publicaciones.AlcanceID;

      if (tipoAlcance != 0) {


        const fechaActual = new Date();
        const fechaVencimiento = new Date(this.publicaciones.FechaVencimiento);

        // Normaliza ambas fechas a medianoche para evitar diferencias en las horas
        fechaActual.setHours(0, 0, 0, 0);
        fechaVencimiento.setHours(0, 0, 0, 0);

        // Calcula la diferencia en milisegundos
        const diferenciaMilisegundos = fechaVencimiento.getTime() - fechaActual.getTime();

        // Convierte la diferencia de milisegundos a días
        const diasRestantes = Math.ceil(diferenciaMilisegundos / (1000 * 60 * 60 * 24));

        // Asigna el valor calculado a la propiedad DiasHastaVencimiento
        this.publicaciones.DiasHastaVencimiento = diasRestantes;
      }
    }


    //Se asigna el valor
    localStorage.setItem('valorPublicacion', this.valorPublicacion.toString());
    localStorage.setItem('valorIVAPublicacion', this.valorIVA.toString());
    localStorage.setItem('valorNetPublicacion', this.valorNeto.toString());


  }


  async VisualizarImagenesPublicacion() {

    //Se obtiene la información del tipo de Publicación elegido
    let tipoPublicacion = this.publicaciones.TipoPublicacionID;

    // Buscar el objeto en la lista selectListaTipoPublicacion
    let tipoPublicacionEncontrado = this.selectListaTipoPublicacion.find(tipo => tipo.TipoPublicacionID === tipoPublicacion);


    if (tipoPublicacionEncontrado === undefined) {
      this.numeroImagenesPublicacion = 1;
      this.displayImagenesPublicacion = true;
      await this.GetListaImagenesPublicacion(this.publicaciones.PublicacionID);
    }
    else if (tipoPublicacionEncontrado!.Imagenes > 0) {
      this.numeroImagenesPublicacion = tipoPublicacionEncontrado!.Imagenes;
      this.displayImagenesPublicacion = true;
      await this.GetListaImagenesPublicacion(this.publicaciones.PublicacionID);
    }
    else
      this.displayImagenesPublicacion = false;
  }


  async VisualizarVideosPublicacion() {

    //Se obtiene la información del tipo de Publicación elegido
    let tipoPublicacion = this.publicaciones.TipoPublicacionID;

    // Buscar el objeto en la lista selectListaTipoPublicacion
    let tipoPublicacionEncontrado = this.selectListaTipoPublicacion.find(tipo => tipo.TipoPublicacionID === tipoPublicacion);

    if(tipoPublicacionEncontrado === undefined)
    {
      this.displayVideosPublicacion = true;
      await this.GetListaVideosPublicacion(this.publicaciones.PublicacionID);
    }
   else if (tipoPublicacionEncontrado!.Video > 0) {
      this.displayVideosPublicacion = true;
      await this.GetListaVideosPublicacion(this.publicaciones.PublicacionID);
    }

    else
      this.displayVideosPublicacion = false;

  }

  async VisualizarCategoriasPublicacion() {
    this.displayCategoriasPublicacion = true;
    await this.GetListaCategoriasPublicacion(this.publicaciones.PublicacionID);
  }

  async VisualizarConjuntosPublicacion() {
    //Se debe determinar si por el tipo de Publicación y su Alcance, se permite la visualización de esta zona, y el tipo de selección Departamento y/o Municipio


    this.tipoCoberturaPublicacion = "Nacional";
    this.displayConjuntosPublicacion = true;
    this.displayCoberturaNacional = true;
    await this.GetListaConjuntosPublicacion(this.publicaciones.PublicacionID);


    /*
        
        //Se obtiene la información del tipo de Publicación elegido
        let tipoPublicacion = this.publicaciones.TipoPublicacionID;
        this.valorPublicacion = 0;
    
        await this.CargarAlcances();
    
    
        if (tipoPublicacion != 0) {
          // Buscar el objeto en la lista selectListaTipoPublicacion
          let tipoPublicacionEncontrado = this.selectListaTipoPublicacion.find(tipo => tipo.TipoPublicacionID === tipoPublicacion);
    
          let tipoAlcance = this.publicaciones.AlcanceID;
          alert("tipoalcance "+tipoAlcance);
    
          let tipoAlcanceEncontrado = this.selectListaAlcances.find(tipo => tipo.AlcanceID === tipoAlcance);
    
          this.alcanceTipo = tipoAlcanceEncontrado!.TipoAlcance;
          this.cantidadAlcance = tipoAlcanceEncontrado!.CantidadAlcance!;
    
    
    
          if (this.alcanceTipo === "Nacional") // Tiene cobertura Nacional, entonces no se muestra
          {
            this.tipoCoberturaPublicacion = "Nacional";
            this.displayConjuntosPublicacion = true;
            this.displayCoberturaNacional = true;
          }
    
          else if (this.alcanceTipo === "Departamento") //Solo muestra la lista desplegable de Departamento
          {
            if (tipoAlcanceEncontrado?.CantidadAlcance! === 1)
              this.tipoCoberturaPublicacion = " 1 Departamento";
            else
              this.tipoCoberturaPublicacion = tipoAlcanceEncontrado?.CantidadAlcance!.toString() + " Departamentos";
    
            this.displayConjuntosPublicacion = true;
            this.displayDepartamentoLocalizacion = true;
            this.displayCiudadLocalizacion = false;
            this.displayCoberturaNacional = false;
          }
          else if (this.alcanceTipo === "Ciudad") // Muestra la lista desplegable de Departamento y Ciudad
          {
    
            if (tipoAlcanceEncontrado?.CantidadAlcance! === 1)
              this.tipoCoberturaPublicacion = " 1 Ciudad";
            else
              this.tipoCoberturaPublicacion = tipoAlcanceEncontrado?.CantidadAlcance!.toString() + " Ciudades";
    
    
            this.displayConjuntosPublicacion = true;
            this.displayDepartamentoLocalizacion = true;
            this.displayCiudadLocalizacion = true;
            this.displayCoberturaNacional = false;
          }
    
          await this.GetListaConjuntosPublicacion(this.publicaciones.PublicacionID);
        }
    */



  }

  sumarDiasYFormatear(dias: number): string {
    const hoy = new Date();
    // Creamos una fecha en UTC a partir del año, mes y día local (esto "ignora" la hora local)
    const fechaUtc = new Date(Date.UTC(hoy.getFullYear(), hoy.getMonth(), hoy.getDate()));

    // Sumamos los días en UTC
    fechaUtc.setUTCDate(fechaUtc.getUTCDate() + dias);

    // Obtenemos los valores en UTC
    const year = fechaUtc.getUTCFullYear();
    const month = ('0' + (fechaUtc.getUTCMonth() + 1)).slice(-2);
    const day = ('0' + fechaUtc.getUTCDate()).slice(-2);

    return `${year}-${month}-${day}`;
  }


  obtenerFechaFormato(): string {
    const fecha = new Date(); // Obtiene la fecha actual
    fecha.setDate(fecha.getDate()); // Suma los días
    const fechaFormateada = fecha.toISOString().slice(0, 10); // Formatea la fecha en AAAA-MM-DD
    return fechaFormateada;
  }


  GetTiposPublicacionByIdResult(id: number) {
    return this.tipospublicacionservice.GetTiposPublicacionById(id).toPromise();
  }
  async GetTiposPublicacionById(id: number) {
    try {

      this.ValorServicio = await this.GetTiposPublicacionByIdResult(id);
      return this.ValorServicio;


    }
    catch (error) {
      console.error('[error en GetListaTiposPublicacion] : ' + error);
    }
  }

  //Cargue de Imagenes
  public async CargarImagenPublicacion() {

    let cantImagenesPermitidas = 0;

    let tipopublicacion = await this.GetTiposPublicacionById(this.publicaciones.TipoPublicacionID);

    if (tipopublicacion !== null) {
      cantImagenesPermitidas = tipopublicacion.Imagenes;
    }

    let cantImagenesCargadas = this.selectListaImagenesPublicacion.length;

    if (cantImagenesCargadas < cantImagenesPermitidas) {
      const files = this.value;
      const remainingSlots = cantImagenesPermitidas - cantImagenesCargadas;

      const filesToUpload = files.slice(0, remainingSlots);

      for (let i = 0; i < filesToUpload.length; i++) {
        const file = filesToUpload[i];
        const resizedImage = await this.resizeImage(file, 600, 276); // Redimensiona la imagen
        const base64Content = await this.convertFileToBase64(resizedImage);

        this.imagenPublicacion.Imagen64 = base64Content;
        this.imagenPublicacion.PublicacionID = this.publicaciones.PublicacionID;

        if (i === 0) {
          // Para la primera imagen
          this.imagenPublicacion.Orden = 1;
          this.imagenPublicacion.ImagenPrincipal = true;
        } else {
          // Para las siguientes imágenes
          this.imagenPublicacion.Orden = 2;
          this.imagenPublicacion.ImagenPrincipal = false;
        }

        await this.imagenesPublicacionservice.PostAddImagenesPublicacion(this.imagenPublicacion).subscribe(
          async result => {
            if (!result) {


              notify("Error al crear imágenes de publicación", "error", 4000);
              this.displayGuardarImagen = false;
            } else {
              notify("Se ha asignado la imagen de la publicación correctamente", "success", 4000);
              await this.GetListaImagenesPublicacion(this.publicaciones.PublicacionID);
            }
          },
          error => {
            console.error(error);
            this.displayGuardarImagen = false;
          }
        );
      }


      this.displayGuardarImagen = false;
      this.value = [];
    } else {
      notify("Se excede el número de Imágenes permitidas para esta publicación. Si desea agregar más imágenes, puede actualizar a Otro Plan", "error", 4000);
      this.displayGuardarImagen = false;
    }
  }

  public async resizeImage(file: File, width: number, height: number): Promise<File> {
    return new Promise<File>((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        const img = new Image();
        img.src = reader.result as string;
        img.onload = () => {
          const canvas = document.createElement('canvas');
          const ctx = canvas.getContext('2d');
          canvas.width = width;
          canvas.height = height;
          ctx!.drawImage(img, 0, 0, width, height);
          canvas.toBlob((blob) => {
            if (blob) {
              resolve(new File([blob], file.name, { type: file.type }));
            } else {
              reject(new Error('Image resizing failed.'));
            }
          }, file.type);
        };
      };
      reader.onerror = (error) => reject(error);
    });
  }

  public async convertFileToBase64(file: File): Promise<string> {
    return new Promise<string>((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        const result = reader.result as string;
        const base64Content = result.split(',')[1];
        resolve(base64Content);
      };
      reader.onerror = (error) => reject(error);
    });
  }



  subidaCompletaImagen(e: {
    value: null; request: { response: string; status: number; };
  }) {

    try {
      if (e.value == null || e.value == undefined) {
        this.displayGuardarImagen = false;
        notify("Error cargando el archivo", "error", 4000);
      }
      else {
        this.displayGuardarImagen = true;
      }
    }
    catch (error) {
      console.error('[error en Cargue de Imagenes] : ' + error);
    }
  }


  //Cargue de Videos 
  validarUrlYoutube(url: string): boolean {
    const youtubeRegex = /^(https?:\/\/)?(www\.)?(youtube\.com\/watch\?v=|youtu\.be\/)[\w\-]{11}(&.*)?$/;
    return youtubeRegex.test(url);
  }


  public async CargarVideoPublicacion() {

    if (!this.validarUrlYoutube(this.videoPublicacion.RutaVideo)) {
      notify("Debe ingresar una URL de Video válida en Youtube para poder grabar y/o actualizar.", "error", 4000);
      this.videoPublicacion.RutaVideo = "";
      return;
    }
    else if (this.videoPublicacion.RutaVideo == "") {
      notify("Debe ingresar la URL del Video para poder grabar y/o actualizar.", "error", 4000);
      return;
    }
    else {
      //Antes de grabar la imagen, se valida el número de imagenes existentes y el limite permitido según el tipo de publicación


      let cantVideosPermitidos = 0;

      if (this.publicaciones.TipoPublicacionID === 1) cantVideosPermitidos = 0;
      else cantVideosPermitidos = 1;

      let cantVideosCargados = 0;
      cantVideosCargados = this.selectListaVideosPublicacion.length;


      //   if (cantVideosCargados <= cantVideosPermitidos) {


      this.videoPublicacion.PublicacionID = this.publicaciones.PublicacionID;

      if (cantVideosCargados == 0) //No existe, entonces se agrega
      {
        await this.videosPublicacionservice.PostAddVideoPublicacion(this.videoPublicacion).subscribe(async result => {

          if (result.success == false) {
            notify("Error al cargar video publicación", "error", 4000);
            this.displayGuardarVideo = false;
          }
          else {
            this.displayGuardarVideo = false;
            this.esEditableVideo = false;

            notify("Se ha cargado el video de la publicación correctamente ", "success", 4000);

            await this.GetListaVideosPublicacion(this.publicaciones.PublicacionID);

          }
        },
          error => {
            console.error(error);
            this.displayGuardarVideo = false;

          }
        );

      }
      else {
        //Como ya existe, se actualiza
        this.videoPublicacion.VideoID = this.selectListaVideosPublicacion[0].VideoID;
        await this.videosPublicacionservice.PutUpdVideoPublicacion(this.videoPublicacion).subscribe(async result => {

          if (result.success == false) {
            notify("Error al cargar video publicación", "error", 4000);
            this.displayGuardarVideo = false;
          }
          else {
            this.displayGuardarVideo = false;

            notify("Se ha cargado el video de la publicación correctamente ", "success", 4000);

            await this.GetListaVideosPublicacion(this.publicaciones.PublicacionID);

          }
        },
          error => {
            console.error(error);
            this.displayGuardarVideo = false;

          }
        );

      }
    }




  }

  //Cargue de Categorias
  public async CargarCategoriasPublicacion() {
    let cantCategoriasPermitidas = 3;

    if (this.categoriaPublicacion.CategoriaID == 0) {
      notify("Debe seleccionar la Categoria", "error", 4000);
      return;
    }
    else if (this.categoriaPublicacion.SubcategoriaID == 0) {
      notify("Debe seleccionar la SubCategoria", "error", 4000);
      return;
    }
    else {

      // Verificar duplicados
      const duplicado = this.selectListaCategoriasPublicacion.some(subcategoria => {

        // Si se valida por Categoria y Subcategoria
        return subcategoria.CategoriaID === this.categoriaPublicacion.CategoriaID &&
          subcategoria.SubcategoriaID === this.categoriaPublicacion.SubcategoriaID;

      });

      if (duplicado) {
        notify("Ya existe esta combinación de Categoria y SubCategoria asignada", "error", 4000);
        return;
      }


      let cantCategoriasCargadas = this.selectListaCategoriasPublicacion.length;

      if (cantCategoriasCargadas < cantCategoriasPermitidas) {
        this.categoriaPublicacion.PublicacionID = this.publicaciones.PublicacionID;

        await this.categoriaPublicacionservice.PostAddCategoriaPublicacion(this.categoriaPublicacion).subscribe(async result => {

          if (result.success == false) {
            notify("Error al cargar categoria publicación", "error", 4000);
            this.displayGuardarCategoria = false;
          }
          else {
            this.displayGuardarCategoria = false;

            notify("Se ha cargado la categoria de la publicación correctamente ", "success", 4000);

            this.categoriaPublicacion.CategoriaID = 0;
            this.categoriaPublicacion.SubcategoriaID = 0;

            await this.GetListaCategoriasPublicacion(this.publicaciones.PublicacionID);

          }
        },
          error => {
            console.error(error);
            this.displayGuardarVideo = false;

          }
        );
      }
      else {
        notify("Se excede el número de Categorias permitidas para esta publicación.", "error", 4000);
        this.displayGuardarImagen = false;
      }



    }

  }


  //Cargue de Categorias
  public async CargarLocalizacionesPublicacion() {

    // De acuerdo al tipo de alcance y numero se determina si se permite grabar
    if (this.conjuntoPublicacion.ZonasLocalidadID == 0 && this.displayDepartamentoLocalizacion) {
      notify("Debe seleccionar el Departamento", "error", 4000);
      return;
    }
    if (this.conjuntoPublicacion.ConjuntoID == 0 && this.displayCiudadLocalizacion) {
      notify("Debe seleccionar la Ciudad / Municipio", "error", 4000);
      return;
    }

    // Verificar duplicados
    const duplicado = this.selectListaConjuntoPublicacion.some(conjunto => {
      if (this.displayCiudadLocalizacion && this.displayDepartamentoLocalizacion) {
        // Si se valida por Ciudad y Departamento
        return conjunto.ZonasLocalidadID === this.conjuntoPublicacion.ZonasLocalidadID &&
          conjunto.ConjuntoID === this.conjuntoPublicacion.ConjuntoID;
      } else if (this.displayDepartamentoLocalizacion) {
        // Si solo se valida por Departamento
        return conjunto.ZonasLocalidadID === this.conjuntoPublicacion.ZonasLocalidadID;
      } else if (this.displayCiudadLocalizacion) {
        // Si solo se valida por Ciudad
        return conjunto.ConjuntoID === this.conjuntoPublicacion.ConjuntoID;
      }
      return false;
    });

    if (duplicado) {
      notify("Ya existe esta combinación de Departamento y/o Ciudad asignada", "error", 4000);
      return;
    }

    // Se determina el número de asignaciones realizadas hasta el momento
    let localizacionesAsignadas = this.selectListaConjuntoPublicacion.length;

    if (this.alcanceTipo === "Departamento" && localizacionesAsignadas >= this.cantidadAlcance) {
      notify("Se supera el número de Departamentos permitidos para registrar", "error", 4000);
      this.conjuntoPublicacion.ZonasLocalidadID = 0;
      return;
    } else if (this.alcanceTipo === "Ciudad" && localizacionesAsignadas >= this.cantidadAlcance) {
      notify("Se supera el número de Ciudades permitidas para registrar", "error", 4000);
      this.conjuntoPublicacion.ConjuntoID = 0;
      return;
    } else {
      // Está dentro del rango de Departamentos o Ciudades permitidas

      this.conjuntoPublicacion.PublicacionID = this.publicaciones.PublicacionID;

      await this.conjuntoPublicacionservice.PostAddConjuntoPublicacion(this.conjuntoPublicacion).subscribe(async result => {
        if (result.OperacionExitosa == false) {
          notify("Error al guardar la Localización de la Publicación", "error", 4000);
          this.displayGuardarConjunto = false;
        } else {
          this.displayGuardarConjunto = false;

          notify("Se ha cargado la Localización de la Publicación correctamente ", "success", 4000);

          this.conjuntoPublicacion.ConjuntoID = 0;
          this.conjuntoPublicacion.ZonasLocalidadID = 0;
          this.conjuntoPublicacion.PublicacionID = 0;
          this.conjuntoPublicacion.PublicacionConjuntoID = 0;

          await this.GetListaConjuntosPublicacion(this.publicaciones.PublicacionID);
        }
      },
        error => {
          console.error(error);
          this.displayGuardarVideo = false;
        });
    }
  }


  public async ConsultarCodigoDescuentoBienvenida() {

    //Se valida si el codigo de Descuento de Bienvenida aún no ha sido aplicado 



    let codigoDescuento: Descuentos = new Descuentos();

 
    codigoDescuento = await this.GetDescuentoBienvenidaByUser(this.usuario);  //aqui voy


    if (codigoDescuento != null) //Existe el Codigo de Descuento
    {

   
  

      if (codigoDescuento.NumeroPublicaciones === 10 || codigoDescuento.NumeroPublicaciones === 20) {
        this.mensajebienvenidapublicacion = 'Encontramos que haz registrado un código de descuento válido. Por lo tanto tienes derecho a <b>' + codigoDescuento.NumeroPublicaciones + '<b> publicaciones <b><i>Totalmente Gratis por 3 meses</i></b>.';
        //invocar el modal de bienvenida
        this.open(this.codigoBienvenida);
        this.descuentos.DescuentoCodeID = codigoDescuento.DescuentoCodeID;
        this.descuentos.UserID = this.usuario;

      }
      else if (codigoDescuento.NumeroPublicaciones > 1) {
        this.mensajebienvenidapublicacion = 'Encontramos que haz registrado un código de descuento válido. Aún tienes derecho a <b>' + codigoDescuento.NumeroPublicaciones + '<b> publicaciones <b><i>Totalmente Gratis por 3 meses</i></b>.';
        //invocar el modal de bienvenida
        this.open(this.codigoBienvenida);
        this.descuentos.DescuentoCodeID = codigoDescuento.DescuentoCodeID;
        this.descuentos.UserID = this.usuario;

      }
      else if (codigoDescuento.NumeroPublicaciones === 1) {
        this.mensajebienvenidapublicacion = 'Encontramos que haz registrado un código de descuento válido. Aún tienes derecho a <b>' + codigoDescuento.NumeroPublicaciones + '<b> publicación <b><i>Totalmente Gratis por 3 meses</i></b>.';
        //invocar el modal de bienvenida
        this.open(this.codigoBienvenida);
        this.descuentos.DescuentoCodeID = codigoDescuento.DescuentoCodeID;
        this.descuentos.UserID = this.usuario;

      }
      else {
        this.periodoPublicacionBienvenida = false;

      }
    }
    else {
      this.periodoPublicacionBienvenida = false;

    }


  }


  async GetDescuentoBienvenidaById(id: string, userid: number) {
    try {
      this.ValorServicio = await this.GetDescuentoBienvenidaByIdResult(id, userid);
      return this.ValorServicio;
    }
    catch (error) {
      console.error('[error en GetDescuento] : ' + error);
    }
  }


  async GetDescuentoBienvenidaByIdResult(id: string, userid: number) {
    return this.pagosservice.GetDescuentoBienvenidaById(id, userid).toPromise();
  }


  async GetDescuentoBienvenidaByUser(userid: number) {
    try {
      this.ValorServicio = await this.GetDescuentoBienvenidaByUserResult(userid);
     
      return this.ValorServicio;
    }
    catch (error) {
      console.error('[error en GetDescuento] : ' + error);
    }
  }


  async GetDescuentoBienvenidaByUserResult(userid: number) {
    return this.pagosservice.GetDescuentoBienvenidaByUser(userid).toPromise();
  }




  public async AplicarCodigoDescuento() {

    //Se valida si el codigo de Descuento Existe y si está Vigente o No este siendo utilizado

    let codigopublicion: any;
    this.descuentos = new Descuentos();



    if (this.pagos.DescuentoCodeID === "") {
      notify("Debe ingresar el Código de Descuento", "error", 4000);
    }
    else {

      codigopublicion = await this.GetDescuentoById(this.pagos.DescuentoCodeID, this.usuario);

      if (codigopublicion != null) //Existe el Codigo de Descuento
      {
        this.descuentos.DescuentoCodeID = codigopublicion.DescuentoCodeID;
        this.descuentos.FechaGeneracion = codigopublicion.FechaGeneracion;
        this.descuentos.FechaVencimiento = codigopublicion.FechaVencimiento;
        this.descuentos.Utilizado = codigopublicion.Utilizado;
        this.descuentos.PorcentajeDescuento = codigopublicion.PorcentajeDescuento;
        this.descuentos.UserID = this.usuario;
        this.descuentos.NumeroPublicaciones = 1;
        this.descuentos.TipoUtilizacion = codigopublicion.TipoUtilizacion;


        this.pagos.PublicacionID = this.publicaciones.PublicacionID;
        const today = new Date();
        const yyyy = today.getFullYear();
        const mm = String(today.getMonth() + 1).padStart(2, '0'); // Los meses comienzan desde 0, por eso se suma 1
        const dd = String(today.getDate()).padStart(2, '0');

        const formattedDate = `${yyyy}-${mm}-${dd}`;
        this.pagos.FechaPago = new Date(formattedDate).toString();
        this.pagos.DescuentoCodeID = codigopublicion.DescuentoCodeID;
        this.pagos.MontoaPagar = 0;
        this.pagos.Descuentos = 0;
        this.pagos.MontoPagado = 0;
        this.pagos.TipoPago = "3"; //Descuento
        this.pagos.Estado = 2;






        // Obtener la fecha actual
        const fechaActual = new Date();
        // Convertir FechaVencimiento a tipo Date
        const fechaVencimiento = new Date(this.descuentos.FechaVencimiento);


        //Si el codigo existe, validar si no ha sido utilizado o haya expirado

        if (this.descuentos.Utilizado === true) {
          notify("El Código de Descuento ya fue utilizado. Ingrese un nuevo Código", "error", 4000);
        }
        else if (fechaActual > fechaVencimiento) {
          notify("El Código de Descuento ya ha expirado. Ingrese un nuevo Código", "error", 4000);
        }
        else {
          //Se aplica el Codigo de Descuento y se registra el Pago correspondiente y se emite mensaje de Aceptación del Codigo


          await this.pagosservice.PutDescuentoById(this.descuentos).subscribe(async result => {

            if (result.success == false) {
              notify("Error al actualizar descuento", "error", 4000);
            }
            else {
              //Se registra el pago correspondiente


              if (this.descuentos.TipoUtilizacion === 'Bienvenida' || this.descuentos.PorcentajeDescuento === 100) {
                //IMPORTANTE : Si el Código de Descuento es por el valor del 100 % no se requiere pago en Payú, y la publicación quedará aprobada inmediatamente (No aplica para renovación o cambio de plan)
                await this.pagosservice.PostPagos(this.pagos).subscribe(async result => {


                  if (result.success == false) {
                    notify("Error al asignar pagos", "error", 4000);
                  }
                  else {


                    //se debe actualizar el estado de la publicación
                    this.publicaciones.EstadoAprobacionID = 2; //Aprobado
                    this.publicaciones.PagoRealizado = true;

                    await this.publicacionesservice.PutPublicacionEstado(this.publicaciones).subscribe(async result => {

                      if (result.success == false) {
                        notify("Error al actualizar descuento", "error", 4000);
                      }
                      else {

                        notify("Se ha asignado el descuento correctamente ", "success", 4000);
                        this.cerrarModales();
                        this.Reiniciar();
                      }
                    },
                      error => {
                        console.error(error);
                      }
                    );

                    notify("Se ha asignado el descuento correctamente ", "success", 4000);
                    this.cerrarModales();
                    this.Reiniciar();


                  }
                },
                  error => {
                    console.error(error);
                  }
                );

              }
              else if (this.descuentos.TipoUtilizacion === 'Promocion') {


                if (this.motivoPago === 'PagoPublicacion') {
                  this.CalcularCostoPublicacion();
                  this.displayCalculoPublicacion = true;
                }
                else if (this.motivoPago === 'Renovacion') {
                  this.CalcularCostoPublicacionRenovacion();
                  this.displayCalculoPublicacionRenovacion = true;
                }
                //Se debe recalcular el valor total aplicando el descuento

                //1) El descuento se realiza al valor base (sea de primera vez o renovación)

                //2) Se le debe mostrar al usuario el nuevo valor con el Descuento y el código del descuento aplicado

                this.codigoDescuentoAplicado = true;
                notify("Se ha asignado el descuento correctamente ", "success", 4000);

              }

            }

          },
            error => {
              console.error(error);
            }
          );
        }

      }
      else
        notify("El Código de Descuento No Existe", "error", 4000);
    }
  }

  async GetDescuentoById(id: string, userid: number) {
    try {
      this.ValorServicio = await this.GetDescuentoByIdResult(id, userid);
      return this.ValorServicio;
    }
    catch (error) {
      console.error('[error en GetDescuento] : ' + error);
    }
  }


  async GetDescuentoByIdResult(id: string, userid: number) {
    return this.pagosservice.GetDescuentoById(id, userid).toPromise();
  }

  public async PagarPublicacion() {

    this.limpiarFormularioPagos();
    await this.open(this.formularioPago);

  }

  public async PagarRenovacionPublicacion() {
    this.limpiarFormularioPagos();
    await this.open(this.formularioPagoRenovacion);

  }

  limpiarFormularioPagos() {
    this.pagos.Email = "";
    this.pagos.TelefonoCelular = "";
    this.pagos.RazonSocial = "";
    this.pagos.TipoDocumentoID = 0;
    this.pagos.TipoDocumentoIDs = '';
    this.pagos.NumeroDocumento = "";
    this.pagos.TipoPago = null;
    this.pagos.PagoID = 0;
    this.pagos.PublicacionID = 0;
    this.pagos.FechaPago = '';
    this.pagos.MontoaPagar = 0;
    this.pagos.MontoPagado = 0;
    this.pagos.Descuentos = 0;
    this.pagos.DescuentoCodeID = '';
    this.pagos.NumeroReferencia = '';

  }


  async GenerarPagoPublicacion() {
    if (this.ValidacionesPreviaPago()) {


      const fecha = new Date();
      const year = fecha.getFullYear();
      const month = String(fecha.getMonth() + 1).padStart(2, '0'); // Aseguramos que el mes sea de 2 dígitos
      const day = String(fecha.getDate()).padStart(2, '0'); // Aseguramos que el día sea de 2 dígitos

      if (this.pagos.TipoPago === "1") {

        localStorage.setItem('consecutivoPublicacion', this.publicaciones.PublicacionID.toString());
        localStorage.setItem('consecutivoPublicacionFecha', this.consecutivoPublicacion.toString());

        localStorage.setItem('descripcionPublicacion', "Publicación: " + this.publicaciones.Titulo);
        localStorage.setItem('emailCliente', this.pagos.Email.toString());
        localStorage.setItem('telefonoCliente', this.pagos.TelefonoCelular.toString());

        localStorage.setItem('nombreComprador', this.pagos.RazonSocial);

        localStorage.setItem('tipoDocumentoComprador', this.pagos.TipoDocumentoIDs.toString());
        localStorage.setItem('numeroDocumentoComprador', this.pagos.NumeroDocumento.toString());

        localStorage.setItem('RazonPago', 'P');

        this.displayPasarelaPagos = true;
        this.router.navigate(['/pagar-payu']);


      } else if (this.pagos.TipoPago === "2") {
        await this.open(this.mensajePago);
      }
      else if (this.pagos.TipoPago === "3") {
        await this.open(this.DescuentoCodigo);
      }

    } else {
      notify("Por favor, revise los valores ingresados para proceder con el pago", "error", 4000);
    }
  }

  async GenerarPagoPublicacionRenovacion() {
    if (this.ValidacionesPreviaPago()) {


      const fecha = new Date();
      const year = fecha.getFullYear();
      const month = String(fecha.getMonth() + 1).padStart(2, '0'); // Aseguramos que el mes sea de 2 dígitos
      const day = String(fecha.getDate()).padStart(2, '0'); // Aseguramos que el día sea de 2 dígitos



      if (this.pagos.TipoPago === "1") {

        localStorage.setItem('consecutivoPublicacion', this.publicacionesRenovacion.PublicacionID.toString());
        localStorage.setItem('consecutivoPublicacionFecha', this.consecutivoPublicacion.toString());

        localStorage.setItem('descripcionPublicacion', "Publicación: " + this.publicacionesRenovacion.Titulo);
        localStorage.setItem('emailCliente', this.pagos.Email.toString());
        localStorage.setItem('telefonoCliente', this.pagos.TelefonoCelular.toString());

        localStorage.setItem('nombreComprador', this.pagos.RazonSocial);
        localStorage.setItem('tipoDocumentoComprador', this.pagos.TipoDocumentoIDs.toString());
        localStorage.setItem('numeroDocumentoComprador', this.pagos.NumeroDocumento.toString());

        //aqui se deben asignar los valores al objeto publicacionRenovacionDetalle


        localStorage.setItem('renovacionPublicacion', JSON.stringify(this.publicacionRenovacionDetalle));
        localStorage.setItem('publicacionesRenovacion', JSON.stringify(this.publicacionesRenovacion));

        localStorage.setItem('RazonPago', 'R');




        this.displayPasarelaPagos = true;

        this.router.navigate(['/pagar-payu']);


      } else if (this.pagos.TipoPago === "2") {
        await this.open(this.mensajePago);
      }


    } else {
      notify("Por favor, revise los valores ingresados para proceder con el pago", "error", 4000);
    }
  }

  async MostrarModalDescuento() {
    await this.open(this.DescuentoCodigo);
  }


  ValidacionesPreviaPago() {

    if (this.pagos.TipoDocumentoIDs === '') {
      notify("Debe seleccionar un Tipo de Documento", "error", 4000);
      return false;
    }
    else if (this.pagos.NumeroDocumento === '') {
      notify("Debe ingresar un Número de Documento", "error", 4000);
      return false;
    }
    else if (this.pagos.RazonSocial === '') {
      notify("Debe ingresar el Nombre Completo / Razón Social", "error", 4000);
      return false;
    }
    else if (this.pagos.TelefonoCelular === '') {
      notify("Debe ingresar el Número de Teléfono Celular", "error", 4000);
      return false;
    }
    else if (this.pagos.Email === '') {
      notify("Debe ingresar el Correo Electrónico", "error", 4000);
      return false;
    }
    else if (this.ValidarEmail(this.pagos.Email) == false) {
      notify("Debe ingresar un correo en un formato valido", "error", 4000);
      return false;
    }
    else if (this.pagos.TipoPago === '' || this.pagos.TipoPago === null) {
      notify("Debe seleccionar una Forma de Pago", "error", 4000);
      return false;
    }
    else
      return true;

  }


  ValidarEmail(email: string) {

    var EMAIL_REGEX = "^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$";

    if (email.match(EMAIL_REGEX))
      return true;
    else
      return false;

  }


  cerrarModales() {
    this.modalService.dismissAll();

  }

  generateSHA256(cadena: string): string {
    // Crear el hash SHA-256 y convertirlo a una cadena hexadecimal
    return CryptoJS.SHA256(cadena).toString(CryptoJS.enc.Hex);
  }

  generateMD5(cadena: string): string {
    // Crear el hash MD5 y convertirlo a una cadena hexadecimal
    return CryptoJS.MD5(cadena).toString(CryptoJS.enc.Hex);
  }

  Reiniciar() {
    const currentUrl = this.router.url;
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate([currentUrl]);
    });
  }



  /*******************Visualización del Detalle de la Publicación */

  async showHouse(house: PublicacionesBuscador) {
    this.numeroVistas = 0;
    this.puntuacionPublicacion = 0;
    this.puntuacionVendedor = 0;

    this.selectedHouse = house;
    this.currentHouse = house;
    // this.popupVisible = true;
    await this.GetListaImagenesDetallePublicacion(house.PublicacionID);
  }



  groupImages(images: any[], count: number): any[][] {
    const result: any[][] = [];
    for (let i = 0; i < images.length; i += count) {
      result.push(images.slice(i, i + count));
    }
    return result;
  }



  ConfirmacionEliminarImagen(id: number) {
    this.open(this.deleteImagen);
    this.idRecordDelete = id;
  }

  onRightClickImage(event: MouseEvent, imagenID: number): void {
    event.preventDefault();
    this.idRecordImagenPrincipal = imagenID;
    this.open(this.establecerImagenPrincipal);
  }

  onRightClick(event: MouseEvent, index: number, imagenId: number) {
    event.preventDefault(); // Evita que se muestre el menú contextual predeterminado
    this.open(this.deleteImagen);
    this.idRecordDelete = imagenId;
    this.indexImagen = index;
  }


  // Para el carrusel
  onTouchStartImage(imagenID: number): void {
    this.longPressTimeout = setTimeout(() => {
      this.idRecordImagenPrincipal = imagenID;
      this.open(this.establecerImagenPrincipal);
    }, this.longPressDuration);
  }

  onTouchEnd(): void {
    clearTimeout(this.longPressTimeout); // Cancelar la acción si se levanta el dedo antes de que pase el tiempo
  }

  // Para la galería
  onTouchStart(imagenId: number, index: number): void {
    this.longPressTimeout = setTimeout(() => {
      this.idRecordDelete = imagenId;
      this.indexImagen = index;
      this.open(this.deleteImagen);
    }, this.longPressDuration);
  }


  deleteImage(index: number, imagenId: number) {

    this.selectListaImagenesPublicacion.splice(index, 1); // Elimina la imagen de la lista
  }

  async EliminarImagen() {
    await this.DelImagenPublicacionById(this.idRecordDelete);
    await this.GetListaImagenesPublicacion(this.publicaciones.PublicacionID);
    this.selectListaImagenesPublicacion.splice(this.indexImagen, 1); // Elimina la imagen de la lista

  }




  // Método que se llama al hacer clic en una imagen de la galería
  selectImage(index: number) {
    this.activeSlideIndex = index; // Cambia la imagen activa en el carrusel
    this.pauseCarouselTemporarily();
  }

  // Método para pausar el carrusel temporalmente
  pauseCarouselTemporarily() {
    this.carouselInterval = 0; // Pausa el carrusel

    setTimeout(() => {
      this.carouselInterval = 3000; // Reanuda el carrusel después de 5 segundos
    }, 5000); // 5000 milisegundos = 5 segundos
  }



  onRightClickVideo(event: MouseEvent, video: any): void {
    event.preventDefault(); // Prevenir el menú contextual por defecto
    if (confirm('¿Estás seguro de que quieres eliminar este video?')) {
      this.eliminarVideo(video);
    }
  }

  eliminarVideo(video: any): void {
    this.selectListaVideosPublicacion = this.selectListaVideosPublicacion.filter(v => v !== video);
  }



  /******************************** */
  async showHousePublicacion(PublicacionID: number) {

    this.loading = true;  // Inicia la carga

    try {
      await this.GetListaImagenesPublicacion(PublicacionID);
      this.selectedHousePublicacion = this.selectListaImagenesPublicacion[0];
      this.currentHousePublicacion = this.selectListaImagenesPublicacion[0];

      this.publicaciones = await this.GetPublicacionesById(PublicacionID);

      //await this.CalcularCostoPublicacion();

      await this.GetListaImagenesDetallePublicacion(PublicacionID);

      // Una vez que todo ha sido cargado, abre el modal
      this.open2(this.detallePublicacion);
    } catch (error) {
      console.error('Error al cargar los datos', error);
    } finally {
      this.loading = false;  // Finaliza la carga
    }
  }


  isMobile(): boolean {
    return window.innerWidth <= 768; // Puedes ajustar este tamaño según tus necesidades
  }


  async onMarcarDatosPagador(event: Event): Promise<void> {
    const isChecked = (event.target as HTMLInputElement).checked;
    if (isChecked) {

      let idPublicador = Number(localStorage.getItem('userid'));
      let usuario: any;

      //Se obtienen los datos del usuario publicador y se asignan al formulario

      usuario = await this.GetUsuariosById(idPublicador);

      this.pagos.PublicacionID = this.publicaciones.PublicacionID;
      this.pagos.Email = usuario.CorreoElectronico;
      this.pagos.TelefonoCelular = usuario.TelefonoCelular;
      this.pagos.RazonSocial = usuario.RazonSocial;
      this.pagos.TipoDocumentoID = usuario.TipoDocumentoID;
      this.pagos.TipoDocumentoIDs = usuario.TipoDocumentoID.toString();
      this.pagos.NumeroDocumento = usuario.NumeroDocumento;
      this.pagos.TipoPago = null;

    } else {
      // se limpian los datos del formulario para registrar un nuevo pagador
      this.limpiarFormularioPagos();
    }

  }

  GetUsuariosByIdResult(id: number) {
    return this.usuariosservice.GetUsuarioGeneralById(id).toPromise();
  }

  async GetUsuariosById(id: number) {
    try {

      this.ValorServicio = await this.GetUsuariosByIdResult(id);
      return this.ValorServicio;
    }
    catch (error) {
      console.error('[error en GetUsuarioData] : ' + error);
    }
  }


  onTituloChange(event: any) {
    this.publicaciones.Titulo = event.target.value;
  }

  onDescripcionChange(event: any) {
    this.publicaciones.Descripcion = event.target.value;
  }

  getColorClass(estado: string): string {
    switch (estado) {
      case 'Borrador':
        return 'text-blue';
      case 'Aprobado':
        return 'text-green';
      case 'Rechazado':
      case 'Vencido':
        return 'text-red';
      default:
        return '';
    }
  }


  // Método para truncar la descripción a 300 caracteres
  get truncatedDescription(): string {
    return this.publicaciones?.Descripcion?.length > 300
      ? this.publicaciones.Descripcion.substring(0, 300) + '...'
      : this.publicaciones?.Descripcion;
  }

  // Método para abrir el popup
  openPopup(): void {
    this.showPopup = true;
  }

  // Método para cerrar el popup
  closePopup(): void {
    this.showPopup = false;
  }


  // Método para obtener las imágenes que se deben mostrar en la página actual
  get paginatedImages() {
    const startIndex = this.currentPage * this.imagesPerPage;
    const endIndex = startIndex + this.imagesPerPage;
    return this.selectListaImagenesPublicacion.slice(startIndex, endIndex);
  }

  // Método para avanzar a la siguiente página
  nextPage() {
    if (this.currentPage < Math.floor(this.selectListaImagenesPublicacion.length / this.imagesPerPage)) {
      this.currentPage++;
    }
  }

  // Método para retroceder a la página anterior
  prevPage() {
    if (this.currentPage > 0) {
      this.currentPage--;
    }
  }

  GetListaImagenesDetallePublicacionResult(id: number): any {
    return this.imagenesPublicacionservice.GetListaImagenesPublicacion(id).toPromise();
  }

  async GetListaImagenesDetallePublicacion(idPublicacion: number) {
    try {
      this.ValorServicio = await this.GetListaImagenesPublicacionResult(idPublicacion);
      this.selectListaImagenesDetallePublicacion = this.ValorServicio.Lista;

    }
    catch (error) {
      console.error('[error en GetListaPublicaciones] : ' + error);
    }
  }



  onImageError(event: Event) {
    const element = event.target as HTMLImageElement;
    element.src = this.urlBlob + 'SinImagen.png';
  }



  formatCurrency(value: number): string {
    if (!value) return '0';

    // Formatea el valor como COP con separadores correctos
    let formattedValue = new Intl.NumberFormat('es-CO', {
      style: 'currency',
      currency: 'COP',
      minimumFractionDigits: 0,
      maximumFractionDigits: 0
    }).format(value);

    // Reemplaza las comas por puntos si es necesario
    return formattedValue.replace(/,/g, '.');
  }


  formatTitle(title: string): string {
    const minLength = 100; // Longitud mínima deseada
    const filler = " (Rellenado) "; // Texto para rellenar
    if (title.length < minLength) {
      const neededLength = minLength - title.length;
      const repeatCount = Math.ceil(neededLength / filler.length);
      return title + filler.repeat(repeatCount).slice(0, neededLength); // Rellenar con la cadena
    }
    return title.length > minLength ? title.slice(0, minLength) + '...' : title;
  }

  getFiller(currentLength: number): string {
    const minLength = 100; // Longitud mínima deseada
    return ' '.repeat(minLength - currentLength); // Rellenar con espacios en blanco
  }

  capitalizeFirstLetter(text: string): string {
    return text
      .split(' ') // Divide el texto en palabras
      .map(word => word.charAt(0).toUpperCase() + word.slice(1).toLowerCase()) // Capitaliza la primera letra y convierte el resto a minúsculas
      .join(' '); // Une las palabras de nuevo
  }



  /*************************Vistas y Puntuaciones */

  //Consulta de Vistas de la Publicacion
  GetListaVistasPublicacionResult(id: number): any {
    return this.vistaspublicacionservice.GetListaVistaPuntuacionPublicacion(id).toPromise();
  }

  async GetListaVistasPublicacion(idPublicacion: number) {

    try {
      this.ValorServicio = await this.GetListaVistasPublicacionResult(idPublicacion);
      this.selectListaVistasPublicacion = this.ValorServicio.Lista;

      let contadorVistas = 0;

      // Usamos un foreach para contar cuántos elementos tienen 'VistaID'
      this.selectListaVistasPublicacion.forEach((item: any) => {
        if (item.VistaID) {
          contadorVistas++;
        }
      });

      this.numeroVistas = contadorVistas;
    }
    catch (error) {
      console.error('[error en GetListaVistaPublicacion] : ' + error);
    }


  }

  //Determinar si el usuario ya tiene una vista asociada
  GetVistaPublicacionesByIdResult(UsuarioIDVio: number, PublicacionId: number): any {
    return this.vistaspublicacionservice.GetVistaPublicacionesById(UsuarioIDVio, PublicacionId).toPromise();
  }

  async ConsultarVistaUsuario(UsuarioIDVio: number, PublicacionId: number) {
    try {

      this.ValorServicio = await this.GetVistaPublicacionesByIdResult(UsuarioIDVio, PublicacionId);


      if (this.ValorServicio.VistaID == 0) //No existe la vista de ese usuario, se adiciona
      {

        this.vistaspublicacion.PublicacionID = PublicacionId;
        this.vistaspublicacion.UsuarioIDVio = UsuarioIDVio;

        await this.vistaspublicacionservice.PostAddVistaPublicacion(this.vistaspublicacion).subscribe(async result => {

          if (result.success == false) {
            notify("Error al actualizar vista de la publicación", "error", 4000);
            await this.GetListaVistasPublicacion(PublicacionId);
          }
          else {
            await this.GetListaVistasPublicacion(PublicacionId);

          }
        },
          error => {
            console.error(error);
          }
        );

      }
      else {
        await this.GetListaVistasPublicacion(PublicacionId);
      }

    }
    catch (error) {
      console.error('[error en GetListaConjuntos] : ' + error);
    }
  }




  //Consulta de Vistas de la Publicacion
  GetListaPuntuacionPublicacionResult(id: number): any {
    return this.puntuacionPublicacionservice.GetListaPuntuacionPublicacion(id).toPromise();
  }

  async GetListaPuntuacionPublicacion(idPublicacion: number) {

    try {
      this.ValorServicio = await this.GetListaPuntuacionPublicacionResult(idPublicacion);
      this.selectListaPuntuacionPublicacion = this.ValorServicio.Lista;

      let acumuladorPuntaje = 0;
      let contadorPuntaje = 0;

      // Usamos un foreach para contar cuántos elementos tienen 'VistaID'
      this.selectListaPuntuacionPublicacion.forEach((item: any) => {
        if (item.PuntuacionID) {
          acumuladorPuntaje = acumuladorPuntaje + item.Puntuacion;
          contadorPuntaje++;
        }
      });

      this.puntuacionPublicacion = Math.round(acumuladorPuntaje / contadorPuntaje);

    }
    catch (error) {
      console.error('[error en GetListaVistaPublicacion] : ' + error);
    }


  }


  //Consulta de Puntuacion Vendedor
  GetListaPuntuacionVendedorResult(id: number): any {
    return this.puntuacionPublicacionservice.GetListaPuntuacionVendedor(id).toPromise();
  }

  async GetListaPuntuacionVendedor(idVendedor: number) {

    try {
      this.ValorServicio = await this.GetListaPuntuacionVendedorResult(idVendedor);
      this.selectListaPuntuacionVendedor = this.ValorServicio.Lista;

      let acumuladorPuntaje = 0;
      let contadorPuntaje = 0;

      // Usamos un foreach para contar cuántos elementos tienen 'VistaID'
      this.selectListaPuntuacionVendedor.forEach((item: any) => {
        if (item.PuntuacionID) {
          acumuladorPuntaje = acumuladorPuntaje + item.Puntuacion;
          contadorPuntaje++;
        }
      });

      this.puntuacionVendedor = Math.round(acumuladorPuntaje / contadorPuntaje);

    }
    catch (error) {
      console.error('[error en GetListaVistaPublicacion] : ' + error);
    }


  }

  open2(content: any) {
    this.modalService.open(content, { backdrop: 'static', size: 'xl', keyboard: false, windowClass: 'modal-dialog-centered' });

  }

  filtrarPorEstado(estadoId: number): void {
    if (estadoId === 0) {
      // Si estadoId es 0, se restablecen todos los registros
      this.selectListaPublicacion = [...this.selectListaPublicacionOriginal];
    } else {
      // Si no, se filtra por el estadoId especificado
      this.selectListaPublicacion = this.selectListaPublicacionOriginal.filter(
        (p) => p.EstadoAprobacionID === estadoId
      );
    }
  }

  verPlanes() {
    const url = 'https://www.landing.vecinoemprende.com/?planes=all';

    const windowOptions = 'width=' + screen.width + ',height=' + screen.height + ',left=0,top=0,fullscreen=yes,toolbar=no,menubar=no,scrollbars=no,resizable=no';
    window.open(url, '_blank', windowOptions);

  }

  onTipoDocumentoSeleccionado(event: any): void {
  }

  ModalPagarPublicacion() {

    this.motivoPago = 'PagoPublicacion';

    let perfilActualizado = localStorage.getItem('perfilActualizado');

    if (perfilActualizado === "false")
      this.open(this.mensajeRecordatorioPerfil);
    else
      this.PagarPublicacion();
  }

}