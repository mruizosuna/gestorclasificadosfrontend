import { Component, OnInit } from '@angular/core';
import { environment } from '../../../environments/environment';
import * as CryptoJS from 'crypto-js';
import { Publicaciones, PublicacionRenovacionDetalle } from '../../../models/publicaciones.model';

@Component({
  selector: 'app-pagopayu',
  templateUrl: './pagopayu.component.html',
  styleUrls: ['./pagopayu.component.scss']
})
export class PagopayuComponent implements OnInit {
  hash: string;

  async ngOnInit() {
    await this.GenerarPagoPublicacion();
  }

  async GenerarPagoPublicacion() {


    localStorage.setItem('pagoPayu', "1");






    let consecutivoPublicacion = Number(localStorage.getItem('consecutivoPublicacion'));
    let consecutivoPublicacionFecha = Number(localStorage.getItem('consecutivoPublicacionFecha'));



    let descripcionPublicacion = localStorage.getItem('descripcionPublicacion');
    let valorPublicacion = Number(localStorage.getItem('valorPublicacion'));
    let valorIVAPublicacion = Number(localStorage.getItem('valorIVAPublicacion'));
    let valorNetoPublicacion = Number(localStorage.getItem('valorNetPublicacion'));
    let emailCliente = localStorage.getItem('emailCliente');
    let telefonoCliente = localStorage.getItem('telefonoCliente');

    let nombreComprador = localStorage.getItem('nombreComprador');
    let tipoDocumentoComprador = localStorage.getItem('tipoDocumentoComprador');
    let numeroDocumentoComprador = localStorage.getItem('numeroDocumentoComprador');

    let razonPago = localStorage.getItem('RazonPago');
    let storedData = localStorage.getItem('renovacionPublicacion');
    let storedData2 = localStorage.getItem('publicacionesRenovacion');
    let datosEnvioRenovacion = '';
    let publicacionRenovacionDetalle = new PublicacionRenovacionDetalle();
    let publicacion = new Publicaciones();

  
    if (storedData) {
      // Parsear el JSON
      const parsedData = JSON.parse(storedData);

      
      // Reconstruir el objeto en la estructura de PublicacionRenovacionDetalle
      publicacionRenovacionDetalle = Object.assign(new PublicacionRenovacionDetalle(), parsedData);


    }

    if (storedData2) {
      const parsedData2 = JSON.parse(storedData2);

      
      // Reconstruir el objeto en la estructura de PublicacionRenovacionDetalle
      publicacion = Object.assign(new Publicaciones(), parsedData2);

    }



    datosEnvioRenovacion = 'EsRenovacion:' + publicacionRenovacionDetalle.EsRenovacion + ',EsCambioPlan:' + publicacionRenovacionDetalle.EsCambioPlan + ',AplicoInmediato:' + publicacionRenovacionDetalle.AplicoInmediato + ',AplicoFinVigencia:' + publicacionRenovacionDetalle.AplicoFinVigencia + ',TipoPublicacionID:' + publicacion.TipoPublicacionID + ',AlcanceID:' + publicacion.AlcanceID + ',PeriodoID:' + publicacion.PeriodoID;


    const fecha = new Date();
    const year = fecha.getFullYear();
    const month = String(fecha.getMonth() + 1).padStart(2, '0'); // Aseguramos que el mes sea de 2 dígitos
    const day = String(fecha.getDate()).padStart(2, '0'); // Aseguramos que el día sea de 2 dígitos
    const hours = String(fecha.getHours()).padStart(2, '0'); // Hora en dos dígitos
    const minutes = String(fecha.getMinutes()).padStart(2, '0'); // Minutos en dos dígitos


    this.hash = this.generateMD5(environment.apiKey + "~" + environment.merchantId + "~" + environment.referenceCode+"_" + year + month + day + hours + minutes+"_" + consecutivoPublicacionFecha.toString() +"_"+ razonPago + "~" + valorPublicacion.toString() + "~" + environment.currency);

    const formHtml = `
      <form method="post" action="${environment.ProduccionPasarela}" id="payuForm">
        <input name="merchantId" type="hidden" value="${environment.merchantId}">
        <input name="accountId" type="hidden" value="${environment.accountId}">
        <input name="description" type="hidden" value="`+ descripcionPublicacion + `">
        <input name="referenceCode" type="hidden" value="${environment.referenceCode}` +"_" +year + month + day + hours + minutes +"_"+ consecutivoPublicacionFecha +"_" +razonPago + `">
        <input name="amount" type="hidden" value="`+ valorPublicacion + `">
        <input name="tax" type="hidden" value="`+ valorIVAPublicacion + `">
        <input name="taxReturnBase" type="hidden" value="`+ parseFloat(valorNetoPublicacion.toFixed(2)) + `">
        <input name="currency" type="hidden" value="${environment.currency}">
        <input name="signature" type="hidden" value="${this.hash}">
        <input name="test" type="hidden" value="`+ environment.mode + `">
        <input name="buyerEmail" type="hidden" value="`+ emailCliente + `">
       
       
        <input name="extra1" type="hidden" value="`+ tipoDocumentoComprador + `">
        <input name="extra2" type="hidden" value="`+ datosEnvioRenovacion + `">
        <input name="extra3" type="hidden" value="`+ numeroDocumentoComprador + "/" + nombreComprador + "/" + telefonoCliente + `">
      
         

        <input name="responseUrl" type="hidden" value="${environment.responseUrl}">
        <input name="confirmationUrl" type="hidden" value="${environment.confirmationUrl}">
        <input name="payerFullName" type="hidden" value="APPROVED">
        <input name="payerDocument" type="hidden" value="9015564008">
        <input name="payerDocumentType" type="hidden" value="NIT">
        <input name="payerMobilePhone" type="hidden" value="3108644377">

      </form>
      <script type="text/javascript">
        document.getElementById('payuForm').submit();
      </script>`;


    // Crear un nuevo elemento div y agregar el formulario HTML
    const div = document.createElement('div');
    div.innerHTML = formHtml;
    document.body.appendChild(div);
    // Ejecutar el script para enviar el formulario automáticamente
    div.querySelector('script')?.remove();
    div.querySelector('form')?.submit();
  }

  generateMD5(cadena: string): string {
    return CryptoJS.MD5(cadena).toString(CryptoJS.enc.Hex);
  }
}
