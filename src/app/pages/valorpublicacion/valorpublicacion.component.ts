import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ValorPublicacion, ListaValorPublicacion } from '../../../models/valorpublicacion.model';
import { ValorPublicacionService } from '../../../services/valorpublicacion.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import notify from 'devextreme/ui/notify';
import { TiposPublicacionService } from '../../../services/tipospublicacion.service';
import { TiposPublicacion } from '../../../models/tipospublicacion.model';



@Component({
  selector: 'app-valorpublicacion',
  templateUrl: './valorpublicacion.component.html',
  styleUrls: ['./valorpublicacion.component.scss']
})
export class ValorPublicacionComponent implements OnInit {
  @ViewChild('delete') delete: any;
  selectValorPublicacion: Array<ListaValorPublicacion> = new Array<ListaValorPublicacion>;
  valorpublicacion: ValorPublicacion = new ValorPublicacion();
  estado: string = 'Add';
    
  selectTiposPublicacion: Array<TiposPublicacion> = [];
  idRecordDelete : number = 0; //Id del Registro a Borrar
  ValorServicio: any; //Valores devueltos por el servicio web

  constructor(

    private valorpublicacionservice: ValorPublicacionService,private modalService: NgbModal,
    private tipospublicacionservice: TiposPublicacionService 

  ) { }


  
  async ngOnInit(){
    
  await this.CargarTiposPublicacion();
  await this.CargarValorPublicacion();
  await this.InicializarFormulario();
  }
  
  public async CargarValorPublicacion() {

    await this.GetListaValorPublicacion();
    
  }
  public async CargarTiposPublicacion() {

    await this.GetListaTiposPublicacion();
    
  }

  InicializarFormulario()
  {
    this.valorpublicacion.ValorBasePublicacion  = 0;
    this.estado = 'Add';
  }

 /* GetListaCategoriaSecundariaResult(): any {
    return this.tipospublicacionservice.GetListaTiposPublicacion().toPromise();
  }*/

  async GetListaTiposPublicacion() {
    try {
      
      this.ValorServicio = await this.GetListaValorPublicacionResult();
      this.selectTiposPublicacion = this.ValorServicio.Lista;
     
      var objCategoriaPrincipal: TiposPublicacion = new TiposPublicacion();
      objCategoriaPrincipal.TipoPublicacionID = 0;
      objCategoriaPrincipal.NombreTipoPublicacion = "-- Seleccione el Tipo de Publicación --";
      this.selectTiposPublicacion.push(objCategoriaPrincipal);

    }
    catch (error) {
      console.error('[error en GetListaTiposPublicacion] : ' + error);
    }
  }
  
  ValidarValorPublicacion() {

    if (this.valorpublicacion.ValorBasePublicacion === 0) {
      notify("Debe ingresar un valor en la casilla nombre", "warning", 4000);
      return false;
    }
    else if(this.valorpublicacion.TipoPublicacionID === 0){
      notify("Debe seleccionar un valor de la casilla categoria principal", "warning", 4000);
      return false;
    }
    else
      return true;

  }

  

  public async AsignarValorPublicacion() {

    if (this.ValidarValorPublicacion()) {
      

      if (this.estado === 'Add') {

        await this.valorpublicacionservice.PostValorPublicacion(this.valorpublicacion).subscribe(async result => {

          
          if (result.success == false) { 
            notify("Error al asignar valor de epublicación", "error", 4000);
          }
          else 
          {
            notify("Se ha asignado la asignar valor de publicación correctamente ", "success", 4000);
            this.InicializarFormulario();
            await this.GetListaValorPublicacion();
          }
        },
          error => {
            console.error(error);
          }
        );
      }
      else if (this.estado === 'Upd') {

        await this.valorpublicacionservice.PutValorPublicacionById(this.valorpublicacion).subscribe(async result => {

          if (result.success == false) { 
            notify("Error al actualizar la valorpublicacion", "error", 4000);
          }
          else 
          {
            notify("Se ha actualizado la valorpublicacion correctamente ", "success", 4000);
            this.InicializarFormulario();
            await this.GetListaValorPublicacion();
          }
        },
          error => {
            console.error(error);
          }
        );
      }




    }
  }
  GetListaValorPublicacionResult(): any {
    return this.valorpublicacionservice.GetListaValorPublicacion().toPromise();
  }

  async GetListaValorPublicacion() {
    try {
      this.ValorServicio = await this.GetListaValorPublicacionResult();
      
  
      this.selectValorPublicacion = this.ValorServicio.Lista;
    }
    catch (error) {
      console.error('[error en GetListaValorPublicacion] : ' + error);
    }
  }
  GetValorPublicacionByIdResult(id: number) {
    return this.valorpublicacionservice.GetValorPublicacionById(id).toPromise();
  }
  DelValorPublicacionByIdResult(id: number) {
    return this.valorpublicacionservice.DelValorPublicacionById(id).toPromise();
  }
  async DelValorPublicacionById(id: number){
    await this.DelValorPublicacionByIdResult(id);
  }

  async GetValorPublicacionById(id: number) {
    try {
      
      this.ValorServicio = await this.GetValorPublicacionByIdResult(id);
      return this.ValorServicio;


    }
    catch (error) {
      console.error('[error en GetListaValorPublicacion] : ' + error);
    }
  }

  async Editar(id: number) {

    let valorpublicacion: any;
    
    valorpublicacion = await this.GetValorPublicacionById(id);

    
    this.valorpublicacion.TipoPublicacionID = valorpublicacion.TipoPublicacionID;
    this.valorpublicacion.ValorBasePublicacion = valorpublicacion.ValorBasePublicacion;
    this.valorpublicacion.ValorPublicacionID = valorpublicacion.ValorPublicacionID;
    this.estado = 'Upd';

    //await this.AsignarDispositivoUsuario(); //Se elimina el llamado a esta función
    await this.GetListaValorPublicacion();

  }

  open(content: any) {
    this.modalService.open(content, { backdrop: 'static', keyboard: false, size: 'lg', windowClass: 'modal-dialog-centered'  });
  }

  ConfirmacionEliminar(id: number)
  {
    this.open(this.delete);
    this.idRecordDelete = id;
  }


    async Eliminar() {

    await this.DelValorPublicacionById(this.idRecordDelete);
    
    await this.GetListaValorPublicacion();

  }
}