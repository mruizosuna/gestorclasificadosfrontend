import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Zonas } from '../../../models/zonas.model';
import { ZonasService } from '../../../services/zonas.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import notify from 'devextreme/ui/notify';



@Component({
  selector: 'app-zonas',
  templateUrl: './zonas.component.html',
  styleUrls: ['./zonas.component.scss']
})
export class ZonasComponent implements OnInit {
  @ViewChild('delete') delete: any;
  selectZonas: Array<Zonas> = new Array<Zonas>;
  zona: Zonas = new Zonas();
  estado: string = 'Add';
  idRecordDelete : number = 0; //Id del Registro a Borrar
  ValorServicio: any; //Valores devueltos por el servicio web

  constructor(

    private zonasservice: ZonasService,private modalService: NgbModal


  ) { }


  
  ngOnInit(){
    this.CargarZonas();
    this.InicializarFormulario();
  }
  public async CargarZonas() {


    



    await this.GetListaZonas();
    
  }
  InicializarFormulario()
  {
    this.zona.NombreZonaLocalidad  = '';
    this.estado = 'Add';
  }


  ValidarZona() {

    if (this.zona.NombreZonaLocalidad === '') {
      notify("Debe ingresar un valor en la casilla nombre", "warning", 4000);
      return false;
    }
    else
      return true;

  }

  public async AsignarZona() {

    if (this.ValidarZona()) {
      

      if (this.estado === 'Add') {

        await this.zonasservice.PostZona(this.zona).subscribe(async result => {

          
          if (result.success == false) { 
            notify("Error al asignar zona", "error", 4000);
          }
          else 
          {
            notify("Se ha asignado la zona correctamente ", "success", 4000);
            this.InicializarFormulario();
            await this.GetListaZonas();
          }
        },
          error => {
            console.error(error);
          }
        );
      }
      else if (this.estado === 'Upd') {

        await this.zonasservice.PutZonaById(this.zona).subscribe(async result => {

          if (result.success == false) { 
            notify("Error al actualizar la zona", "error", 4000);
          }
          else 
          {
            notify("Se ha actualizado la zona correctamente ", "success", 4000);
            this.InicializarFormulario();
            await this.GetListaZonas();
          }
        },
          error => {
            console.error(error);
          }
        );
      }




    }
  }
  GetListaZonasResult(): any {
    return this.zonasservice.GetListaZonas().toPromise();
  }

  async GetListaZonas() {
    try {
      this.ValorServicio = await this.GetListaZonasResult();
      
   
      this.selectZonas = this.ValorServicio.Lista;
    }
    catch (error) {
      console.error('[error en GetListaLocalidad] : ' + error);
    }
  }
  GetZonaByIdResult(id: number) {
    return this.zonasservice.GetZonaById(id).toPromise();
  }
  DelZonaByIdResult(id: number) {
    return this.zonasservice.DelZonaById(id).toPromise();
  }
  async DelZonaById(id: number){
    await this.DelZonaByIdResult(id);
  }

  async GetZonaById(id: number) {
    try {
      
      this.ValorServicio = await this.GetZonaByIdResult(id);
      return this.ValorServicio;


    }
    catch (error) {
      console.error('[error en GetListaZona] : ' + error);
    }
  }

  async Editar(id: number) {

    let zona: any;
    
    zona = await this.GetZonaById(id);

    
    this.zona.ZonasLocalidadID = zona.ZonasLocalidadID;
    this.zona.NombreZonaLocalidad = zona.NombreZonaLocalidad;

    this.estado = 'Upd';

    //await this.AsignarDispositivoUsuario(); //Se elimina el llamado a esta función
    await this.GetListaZonas();

  }

  open(content: any) {
    this.modalService.open(content, { backdrop: 'static', keyboard: false, size: 'lg', windowClass: 'modal-dialog-centered'  });
  }

  ConfirmacionEliminar(id: number)
  {
    this.open(this.delete);
    this.idRecordDelete = id;
  }


    async Eliminar() {

    await this.DelZonaById(this.idRecordDelete);
    
    await this.GetListaZonas();

  }
}