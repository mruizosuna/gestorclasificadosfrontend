import { ElementSchemaRegistry } from '@angular/compiler';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Generic, Localidad } from 'src/models/parametricas.model';
import { DisponibilidadUsuario, LocalidadUsuario, Usuario } from 'src/models/usuario.model';
import { ParametricasService } from 'src/services/parametricas.service';
import { UsuarioService } from 'src/services/usuarios.service';
import notify from "devextreme/ui/notify";
import { formatDate } from '@angular/common';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-calendario',
  templateUrl: './calendario.component.html',
  styleUrls: ['./calendario.component.scss']
})
export class CalendarioComponent implements OnInit {

  //Variables Generales

  @ViewChild('delete') delete: any;
  idRecordDelete: number = 0; //Id del Registro a Borrar

  ValorServicio: any; //Valores devueltos por el servicio web
  selectUsuario: Array<Usuario> = []; // Lista de Usuarios
  selectLocalidad: Array<Localidad> = []; // Lista de Localidades
  disponibilidadusuario: DisponibilidadUsuario = new DisponibilidadUsuario();
  usuariolocalidadLista: Array<Usuario> = [];

  lstusuario: Array<Generic> = new Array<Generic>();
  lstusuarioasignado: Array<Generic> = new Array<Generic>();

  selectDisponibilidadAsignada: Array<DisponibilidadUsuario> = [];
  localidadBuscar : number = 0;

  FechaHora: Date | undefined;
  FechaHoraMin: string = "";
  FechaHoraMax: string = "";

  estado: string = 'Add';

  //Variables para configuración de selector de lista de Promotores
  selectAllModeVlaue = 'page';
  selectionModeValue = 'all';

  currentDate: Date = new Date();


  //Variables para el manejo de selector de fechas
  now: Date = new Date();
  currentValue: Date = new Date();
  minDateValue: Date | null = null;
  maxDateValue: Date | null = null;
  disabledDates: Function | null = null;

  zoomLevels: string[] = [
    'month', 'year', 'decade', 'century',
  ];

  weekDays: { id: number; text: string }[] = [
    { id: 0, text: 'Sunday' },
    { id: 1, text: 'Monday' },
    { id: 2, text: 'Tuesday' },
    { id: 3, text: 'Wednesday' },
    { id: 4, text: 'Thursday' },
    { id: 5, text: 'Friday' },
    { id: 6, text: 'Saturday' },
  ];

  weekNumberRules: string[] = [
    'auto', 'firstDay', 'firstFourDays', 'fullWeek',
  ];

  cellTemplate = 'cell';
  holidays: any = [[1, 0], [4, 6], [25, 11]];




  constructor(
    private usuarioservice: UsuarioService,
    private parametricaservice: ParametricasService,
    private modalService: NgbModal
  ) { }

  ngOnInit(): void {
    this.InicializarFechas();
    this.InicializarListas();

  }

  InicializarFechas() {
    this.FechaHora = new Date();
    this.FechaHora.setDate(this.FechaHora.getDate());
    this.FechaHoraMin = formatDate(this.FechaHora, "yyyy-MM-ddT00:00:00", "en-US");
    this.FechaHora.setDate(this.FechaHora.getDate());
    this.FechaHoraMax = formatDate(this.FechaHora, "yyyy-MM-ddT00:00:00", "en-US");

    this.disponibilidadusuario.startDate = this.FechaHora;
    this.disponibilidadusuario.endDate = this.FechaHora;

  }

  //Metodos para el selector de Lista de Promotores
  onSelectionChanged(e: any) {
    const addedItems = e.addedItems;
    const removedItems = e.removedItems;
    var objUser: Generic = new Generic();

    if (addedItems.length > 0) {
      objUser.id = addedItems[0].id;
      objUser.text = addedItems[0].text;
      this.lstusuarioasignado.push(objUser);
    }
    else if (removedItems.length > 0) {
      this.lstusuarioasignado = this.lstusuarioasignado.filter(item => item.id !== removedItems[0].id);
    }
  }

  onSelectAllValueChanged(e: any) {
    const newCheckBoxValue = e.value;
  }


  //Metodos para el Selector de Fechas
  isWeekend(date : any) {
    const day = date.getDay();
    return day === 0 || day === 6;
  }

  setMinDate({ value }: { value: boolean }) {
    this.minDateValue = value
      ? new Date(this.now.getTime() - 1000 * 60 * 60 * 24 * 3)
      : null;
  }

  setMaxDate({ value }: { value: boolean }) {
    this.maxDateValue = value
      ? new Date(this.now.getTime() + 1000 * 60 * 60 * 24 * 3)
      : null;
  }

  disableWeekend({ value }: { value: boolean }) {
    this.disabledDates = value
      ? (data: { view: string; date: any; }) => data.view === 'month' && this.isWeekend(data.date)
      : null;
  }

  useCellTemplate({ value }: { value: boolean }) {
    this.cellTemplate = value ? 'custom' : 'cell';
  }

  async onAppointmentDeleted(e: any) {

    await this.GetListaDisponibilidadAsignada(this.localidadBuscar);
    let id = e.appointmentData.id;
    this.open(this.delete);
    this.idRecordDelete = id;

  }


  //Metodos Generales
  public async InicializarListas() {
    await this.GetListaLocalidad();
    await this.GetListaUsuarioPerfil(3);

  }
  async onValueChanged(e: any) {
    ;
    await this.GetListaDisponibilidadAsignada(Number(e.value));
  
  }

  GetListaLocalidadResult(): any {
    return this.parametricaservice.GetListaLocalidad().toPromise();
  }

  async GetListaLocalidad() {
    try {
      this.ValorServicio = await this.GetListaLocalidadResult();
      this.selectLocalidad = this.ValorServicio.data;

      var objLocalidad: Localidad = new Localidad();
      objLocalidad.id = 0;
      objLocalidad.localidad = "-- Seleccione la Localidad --";
      this.selectLocalidad.push(objLocalidad);

    }
    catch (error) {
      console.error('[error en GetListaLocalidad] : ' + error);
    }
  }

  GetListaUsuarioPerfilResult(perfil: number): any {
    return this.usuarioservice.GetListaUsuarioByPerfil(perfil).toPromise();
  }

  async GetListaUsuarioPerfil(perfil: number) {
    try {

      this.ValorServicio = await this.GetListaUsuarioPerfilResult(perfil);

      this.selectUsuario = this.ValorServicio.data;

      for (let user of this.selectUsuario) {
        var objUser: Generic = new Generic();
        objUser.id = user.id;
        objUser.text = user.nombresApellidos;
        this.lstusuario.push(objUser);
      };
    }
    catch (error) {
      console.error('[error en GetListaUsuario] : ' + error);
    }
  }




  GetUsuarioByIdResult(id: number) {
    return this.usuarioservice.GetUsuarioById(id).toPromise();
  }

  async GetUsuarioById(id: number) {
    try {

      this.ValorServicio = await this.GetUsuarioByIdResult(id);
      return this.ValorServicio.data;


    }
    catch (error) {
      console.error('[error en GetListaUsuario] : ' + error);
    }
  }


  GetDisponibilidadAsignadaResult(localidad: number): any {
    return this.usuarioservice.GetDisponibilidadAsignada(localidad).toPromise();
  }

  async GetListaDisponibilidadAsignada(localidad: number) {
    try {
;
      this.ValorServicio = await this.GetDisponibilidadAsignadaResult(localidad);
      this.selectDisponibilidadAsignada = this.ValorServicio.data;
    }
    catch (error) {
      console.error('[error en GetListaDisponibilidadAsignada] : ' + error);
    }
  }


  ValidarAsignacionDisponibilidad() {

    if (this.disponibilidadusuario.localidadId === 0) {
      notify("Debe seleccionar un valor en la casilla Localidad", "warning", 4000);
      return false;
    }
    else
      if (this.lstusuarioasignado.length === 0) {
        notify("Debe seleccionar un mínimo un usuario para asignar disponibilidad", "warning", 4000);
        return false;
      }
      else
        return true;

  }

  public async AsignarDisponibilidadUsuario() {

    this.localidadBuscar = this.disponibilidadusuario.localidadId;

    if (this.estado === 'Add') {
      if (this.ValidarAsignacionDisponibilidad()) {
        this.disponibilidadusuario.horaInicio = this.disponibilidadusuario.startDate?.toLocaleTimeString('en-US') ?? '';
        this.disponibilidadusuario.horaFin = this.disponibilidadusuario.endDate?.toLocaleTimeString('en-US') ?? '';


        for (let user of this.lstusuarioasignado) {
          this.disponibilidadusuario.usuarioId = user.id;
          await this.usuarioservice.PostDisponibilidad(this.disponibilidadusuario).subscribe(async result => {

            if (result.success === true) {
              notify("Se ha asignado la disponibilidad al usuario correctamente ", "success", 4000);
              await this.GetListaDisponibilidadAsignada(this.localidadBuscar);
            }
            else {
              notify("Error al asignar disponibilidad al usuario", "error", 4000);
            }
          },
            error => {
              console.error(error);
            }
          );
        };
      }
    }
    else if (this.estado === 'Upd') {
      this.disponibilidadusuario.usuarioModifica = localStorage.getItem("usuarioSesionado") ?? '';

      await this.usuarioservice.PutDisponibilidadById(this.disponibilidadusuario, this.disponibilidadusuario.id).subscribe(async result => {

        if (result.success == false) {
          notify("Error al actualizar el disponibilidad del usuario", "error", 4000);
        }
        else {
          notify("Se ha actualizado la disponibilidad del usuario correctamente ", "success", 4000);

          await this.GetListaDisponibilidadAsignada(this.localidadBuscar);
        }
      },
        error => {
          console.error(error);
        }
      );
    }
  }

  GetDisponibilidadByIdResult(id: number) {
    return this.usuarioservice.GetDisponibilidadById(id).toPromise();
  }

  async GetDisponibilidadById(id: number) {
    try {
      this.ValorServicio = await this.GetDisponibilidadByIdResult(id);
      return this.ValorServicio.data;


    }
    catch (error) {
      console.error('[error en GetListaDisponibilidad] : ' + error);
    }
  }

  async Editar() {

    await this.GetListaDisponibilidadAsignada(this.localidadBuscar);
    let disponibilidad: any;
    disponibilidad = await this.GetDisponibilidadById(this.idRecordDelete);

    this.disponibilidadusuario.id = disponibilidad.id;
    this.disponibilidadusuario.estado = false;
    this.disponibilidadusuario.usuarioModifica = localStorage.getItem("usuarioSesionado") ?? '';

    this.estado = 'Upd';

    await this.AsignarDisponibilidadUsuario();

  }


  open(content: any) {
    this.modalService.open(content, { backdrop: 'static', keyboard: false, size: 'lg', windowClass: 'modal-dialog-centered' });
  }



}
