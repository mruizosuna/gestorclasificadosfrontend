import { Component, Input, SimpleChanges } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

@Component({
  selector: 'app-video-item',
  templateUrl: './video-item.component.html',
  styleUrls: ['./video-item.component.scss']
})
export class VideoItemComponent {
  @Input() video: any;
  safeUrl: SafeResourceUrl;

  videos = [];

  constructor(private sanitizer: DomSanitizer) { }

  ngOnChanges(changes: SimpleChanges): void {

    if (changes['video']) {

      if (JSON.stringify(this.video).includes("https://www.youtube.com/")) {

        let videoresultante = JSON.stringify(this.video).replace("https://www.youtube.com/watch?v=", "");

        let jsonString = videoresultante;
        let videoObject = JSON.parse(jsonString);
        let videoId = videoObject.RutaVideo !== undefined ? videoObject.RutaVideo : videoObject;
        this.safeUrl = this.sanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/' + videoId);

      }
      else {
        this.safeUrl = this.sanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/' + this.video.RutaVideo);
      }

    }

  }
}