import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { TiposPublicacion } from '../../../models/tipospublicacion.model';

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import notify from 'devextreme/ui/notify';
import { TiposPublicacionService } from '../../../services/tipospublicacion.service';



@Component({
  selector: 'app-tipospublicacion',
  templateUrl: './tipospublicacion.component.html',
  styleUrls: ['./tipospublicacion.component.scss']
})
export class TiposPublicacionComponent implements OnInit {
  @ViewChild('delete') delete: any;
  selectTiposPublicacion: Array<TiposPublicacion> = new Array<TiposPublicacion>;
  tipospublicacion: TiposPublicacion = new TiposPublicacion();
  estado: string = 'Add';
  idRecordDelete : number = 0; //Id del Registro a Borrar
  ValorServicio: any; //Valores devueltos por el servicio web

  constructor(

    private tipospublicacionservice: TiposPublicacionService,private modalService: NgbModal


  ) { }


  
  ngOnInit(){
    this.CargarTiposPublicacion();
    this.InicializarFormulario();
  }
  public async CargarTiposPublicacion() {


    



    await this.GetListaTiposPublicacion();
    
  }
  InicializarFormulario()
  {
    
    this.tipospublicacion.NombreTipoPublicacion = '';
    this.tipospublicacion.DescripcionTipoPublicacion  = '';
    this.estado = 'Add';
  }


  ValidarTiposPublicacion() {

    if (this.tipospublicacion.NombreTipoPublicacion === '') {
      notify("Debe ingresar un valor en la casilla nombre", "warning", 4000);
      return false;
    }
    else if(this.tipospublicacion.DescripcionTipoPublicacion === ''){
      notify("Debe ingresar un valor en la casilla de descripción", "warning", 4000);
      return false;
    }
    else
      return true;

  }

  public async AsignarTiposPublicacion() {

    if (this.ValidarTiposPublicacion()) {
      

      if (this.estado === 'Add') {

        await this.tipospublicacionservice.PostTiposPublicacion(this.tipospublicacion).subscribe(async result => {

          
          if (result.success == false) { 
            notify("Error al asignar tipospublicacion", "error", 4000);
          }
          else 
          {
            notify("Se ha asignado la tipospublicacion correctamente ", "success", 4000);
            this.InicializarFormulario();
            await this.GetListaTiposPublicacion();
          }
        },
          error => {
            console.error(error);
          }
        );
      }
      else if (this.estado === 'Upd') {

        await this.tipospublicacionservice.PutTiposPublicacionById(this.tipospublicacion).subscribe(async result => {

          if (result.success == false) { 
            notify("Error al actualizar la tipospublicacion", "error", 4000);
          }
          else 
          {
            notify("Se ha actualizado tipospublicacion correctamente ", "success", 4000);
            this.InicializarFormulario();
            await this.GetListaTiposPublicacion();
          }
        },
          error => {
            console.error(error);
          }
        );
      }




    }
  }
  GetListaTiposPublicacionResult(): any {
    return this.tipospublicacionservice.GetListaTiposPublicacion().toPromise();
  }

  async GetListaTiposPublicacion() {
    try {
      this.ValorServicio = await this.GetListaTiposPublicacionResult();
      
    
      this.selectTiposPublicacion = this.ValorServicio.Lista;
    }
    catch (error) {
      console.error('[error en GetListaTiposPublicacion] : ' + error);
    }
  }

  GetTiposPublicacionByIdResult(id: number) {
    return this.tipospublicacionservice.GetTiposPublicacionById(id).toPromise();
  }
  async GetTiposPublicacionById(id: number) {
    try {
      
      this.ValorServicio = await this.GetTiposPublicacionByIdResult(id);
      return this.ValorServicio;


    }
    catch (error) {
      console.error('[error en GetListaTiposPublicacion] : ' + error);
    }
  }

  
  DelTiposPublicacionByIdResult(id: number) {
    return this.tipospublicacionservice.DelTiposPublicacionById(id).toPromise();
  }
  async DelTiposPublicacionById(id: number){
    await this.DelTiposPublicacionByIdResult(id);
  }



  async Editar(id: number) {

    let tipospublicacion: any;
    
    tipospublicacion = await this.GetTiposPublicacionById(id);

    
    this.tipospublicacion.TipoPublicacionID = tipospublicacion.TiposPublicacionID;
    this.tipospublicacion.NombreTipoPublicacion = tipospublicacion.NombreTipoPublicacion;
    this.tipospublicacion.DescripcionTipoPublicacion = tipospublicacion.DescripcionTipoPublicacion;


    this.estado = 'Upd';

    //await this.AsignarDispositivoUsuario(); //Se elimina el llamado a esta función
    await this.GetListaTiposPublicacion();

  }

  open(content: any) {
    this.modalService.open(content, { backdrop: 'static', keyboard: false, size: 'lg', windowClass: 'modal-dialog-centered'  });
  }

  ConfirmacionEliminar(id: number)
  {
    this.open(this.delete);
    this.idRecordDelete = id;
  }


    async Eliminar() {

    await this.DelTiposPublicacionById(this.idRecordDelete);
    
    await this.GetListaTiposPublicacion();

  }
}