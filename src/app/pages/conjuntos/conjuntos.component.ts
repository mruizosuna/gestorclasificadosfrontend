import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Conjuntos, ListaConjuntos } from '../../../models/conjuntos.model';
import { ConjuntosService } from '../../../services/conjuntos.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import notify from 'devextreme/ui/notify';
import { ZonasService } from '../../../services/zonas.service';
import { Zonas } from '../../../models/zonas.model';



@Component({
  selector: 'app-conjuntos',
  templateUrl: './conjuntos.component.html',
  styleUrls: ['./conjuntos.component.scss']
})
export class ConjuntosComponent implements OnInit {
  @ViewChild('delete') delete: any;
  selectConjuntos: Array<ListaConjuntos> = new Array<ListaConjuntos>;
  conjuntos: Conjuntos = new Conjuntos();
  estado: string = 'Add';
    
  selectZonasLocalidad: Array<Zonas> = [];
  idRecordDelete : number = 0; //Id del Registro a Borrar
  ValorServicio: any; //Valores devueltos por el servicio web

  constructor(

    private conjuntosservice: ConjuntosService,private modalService: NgbModal,
    private zonaslocalidadservice: ZonasService 

  ) { }


  
  async ngOnInit(){
    
  await this.CargarZonasLocalidad();
  await this.CargarConjuntos();
  await this.InicializarFormulario();
  }
  
  public async CargarConjuntos() {

    await this.GetListaConjuntos();
    
  }
  public async CargarZonasLocalidad() {

    await this.GetListaZonasLocalidad();
    
  }

  InicializarFormulario()
  {
    this.conjuntos.NombreConjunto  = '';
    this.estado = 'Add';
  }

  GetListaZonasLocalidadResult(): any {
    return this.zonaslocalidadservice.GetListaZonas().toPromise();
  }

  async GetListaZonasLocalidad() {
    try {
      
      this.ValorServicio = await this.GetListaZonasLocalidadResult();
      this.selectZonasLocalidad = this.ValorServicio.Lista;
     
      var objZonaLocalidad: Zonas = new Zonas();
      objZonaLocalidad.ZonasLocalidadID = 0;
      objZonaLocalidad.NombreZonaLocalidad = "-- Seleccione la localidad --";
      this.selectZonasLocalidad.push(objZonaLocalidad);

    }
    catch (error) {
      console.error('[error en GetListaConjuntos] : ' + error);
    }
  }
  
  ValidarConjuntos() {

    if (this.conjuntos.NombreConjunto === '') {
      notify("Debe ingresar el nombre de un conjunto", "warning", 4000);
      return false;
    }
    else if(this.conjuntos.ZonaLocalidadID === 0){
      notify("Debe seleccionar un valor de la casilla localidad", "warning", 4000);
      return false;
    }
    else
      return true;

  }

  

  public async AsignarConjuntos() {

    if (this.ValidarConjuntos()) {
      

      if (this.estado === 'Add') {

        await this.conjuntosservice.PostConjuntos(this.conjuntos).subscribe(async result => {

          
          if (result.success == false) { 
            notify("Error al asignar conjunto", "error", 4000);
          }
          else 
          {
            notify("Se ha asignado conjunto correctamente ", "success", 4000);
            this.InicializarFormulario();
            await this.GetListaConjuntos();
          }
        },
          error => {
            console.error(error);
          }
        );
      }
      else if (this.estado === 'Upd') {

        await this.conjuntosservice.PutConjuntosById(this.conjuntos).subscribe(async result => {

          if (result.success == false) { 
            notify("Error al actualizar la conjuntos", "error", 4000);
          }
          else 
          {
            notify("Se ha actualizado la conjuntos correctamente ", "success", 4000);
            this.InicializarFormulario();
            await this.GetListaConjuntos();
          }
        },
          error => {
            console.error(error);
          }
        );
      }




    }
  }
  GetListaConjuntosResult(): any {
    return this.conjuntosservice.GetListaConjuntos().toPromise();
  }

  async GetListaConjuntos() {
    try {
      this.ValorServicio = await this.GetListaConjuntosResult();
      
    
      this.selectConjuntos = this.ValorServicio.Lista;
    }
    catch (error) {
      console.error('[error en GetListaConjuntos] : ' + error);
    }
  }
  GetConjuntosByIdResult(id: number) {
    return this.conjuntosservice.GetConjuntosById(id).toPromise();
  }
  DelConjuntosByIdResult(id: number) {
    return this.conjuntosservice.DelConjuntosById(id).toPromise();
  }
  async DelConjuntosById(id: number){
    await this.DelConjuntosByIdResult(id);
  }

  async GetConjuntosById(id: number) {
    try {
      
      this.ValorServicio = await this.GetConjuntosByIdResult(id);
      return this.ValorServicio;


    }
    catch (error) {
      console.error('[error en GetListaConjuntos] : ' + error);
    }
  }

  async Editar(id: number) {

    let conjuntos: any;
    
    conjuntos = await this.GetConjuntosById(id);

    
    this.conjuntos.ZonaLocalidadID = conjuntos.ZonaLocalidadID;
    this.conjuntos.NombreConjunto = conjuntos.NombreConjunto;
    this.conjuntos.ConjuntoID = conjuntos.ConjuntoID;

    this.estado = 'Upd';

    //await this.AsignarDispositivoUsuario(); //Se elimina el llamado a esta función
    await this.GetListaConjuntos();

  }

  open(content: any) {
    this.modalService.open(content, { backdrop: 'static', keyboard: false, size: 'lg', windowClass: 'modal-dialog-centered'  });
  }

  ConfirmacionEliminar(id: number)
  {
    this.open(this.delete);
    this.idRecordDelete = id;
  }


    async Eliminar() {

    await this.DelConjuntosById(this.idRecordDelete);
    
    await this.GetListaConjuntos();

  }
}