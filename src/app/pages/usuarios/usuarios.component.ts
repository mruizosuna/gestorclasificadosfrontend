import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import notify from 'devextreme/ui/notify';
import { UsuarioData } from '../../../models/usuario.model';
import { Perfiles } from '../../../models/perfiles.model';
import { UsuarioService } from '../../../services/usuarios.service';
import { PerfilesService } from '../../../services/perfiles.service';
import { tags } from '@angular-devkit/core';
import { Parametros } from '../../../models/parametros.model';
import { ParametricasService } from '../../../services/parametricas.service';
import { environment } from '../../../environments/environment';



@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.scss']
})
export class UsuariosComponent implements OnInit {
  @ViewChild('delete') delete: any;
  selectUsuarios: Array<UsuarioData> = new Array<UsuarioData>;
  usuarios: UsuarioData = new UsuarioData();
  estado: string = 'Add';

  selectPerfil: Array<Perfiles> = [];
  idRecordDelete: number = 0; //Id del Registro a Borrar
  ValorServicio: any; //Valores devueltos por el servicio web

  selectListaTipoDocumento: Array<Parametros> = new Array<Parametros>;

  displayGuardarImagen: boolean = false;

  value: any[] = [];

  urlBlob: string;

  constructor(

    private usuariosservice: UsuarioService, private modalService: NgbModal,
    private perfilesservice: PerfilesService, private parametricasservice: ParametricasService,

  ) {
    this.urlBlob = environment.urlBlob;
  }

  async ngOnInit() {

    await this.CargarPerfiles();
    await this.CargarUsuarios();
    await this.CargarTiposDocumento();
    await this.InicializarFormulario();
  }

  public async CargarUsuarios() {
    await this.GetUsuarioData();
  }

  public async CargarPerfiles() {
    await this.GetListaPerfiles();
  }

  InicializarFormulario() {
    this.limpiarCampos();
    this.estado = 'Add';
  }

  GetListaPerfilesResult(): any {
    return this.perfilesservice.GetListaPerfiles().toPromise();
  }

  async GetListaPerfiles() {
    try {

      this.ValorServicio = await this.GetListaPerfilesResult();
      this.selectPerfil = this.ValorServicio.Lista;
    }
    catch (error) {
      console.error('[error en GetListaPerfiles] : ' + error);
    }
  }

  ValidarUsuarios() {

    if (this.usuarios.PerfilUsuarioID === 0) {
      notify("Debe seleccionar un valor de la casilla de perfil", "warning", 4000);
      return false;
    }
    else if (this.usuarios.Nombre === '') {
      notify("Debe ingresar un valor en la casilla nombre", "warning", 4000);
      return false;
    }
    else if (this.usuarios.CorreoElectronico === '') {
      notify("Debe ingresar un valor de la casilla de correo electrónico", "warning", 4000);
      return false;
    }
    else if (this.usuarios.Contrasena === '') {
      notify("Debe ingresar un valor de la casilla de contraseña", "warning", 4000);
      return false;
    }

    else
      return true;

  }



  public async AsignarUsuarios() {

    if (this.ValidarUsuarios()) {


      if (this.estado === 'Add') {

        this.usuarios.TipoDocumentoID = Number(this.usuarios.TipoDocumentoIDS);

        if (this.usuarios.ImagenUsuario == 'SinImagen.png')
          this.usuarios.ImagenUsuario = null;
        else {
          //Guardado de la fotografia
          const files = this.value;

          for (let file of files) {
            const resizedImage = await this.resizeImage(file, 600, 400); // Redimensiona la imagen a 800x600
            const base64Content = await this.convertFileToBase64(resizedImage);
            this.usuarios.Imagen64 = base64Content;
          }

          this.displayGuardarImagen = false;
          this.value = [];
        }


        await this.usuariosservice.PostUsuarioGeneral(this.usuarios).subscribe(async result => {

          if (result.success == false) {
            notify("Error al asignar usuario", "error", 4000);
          }
          else {
            notify("Se ha asignado el usuario correctamente ", "success", 4000);
            this.InicializarFormulario();
            await this.GetUsuarioData();
          }
        },
          error => {
            console.error(error);
          }
        );
      }
      else if (this.estado === 'Upd') {

        this.usuarios.TipoDocumentoID = Number(this.usuarios.TipoDocumentoIDS);

        if (this.usuarios.ImagenUsuario == 'SinImagen.png')
          this.usuarios.ImagenUsuario = null;
        else {
          //Guardado de la fotografia
          const files = this.value;

          for (let file of files) {
            const resizedImage = await this.resizeImage(file, 600, 400); // Redimensiona la imagen a 800x600
            const base64Content = await this.convertFileToBase64(resizedImage);
            this.usuarios.Imagen64 = base64Content;
          }

          this.displayGuardarImagen = false;
          this.value = [];
        }


        await this.usuariosservice.PutUsuarioGeneralById(this.usuarios).subscribe(async result => {

          if (result.success == false) {
            notify("Error al actualizar el usuario", "error", 4000);
          }
          else {
            notify("Se ha actualizado el usuario correctamente ", "success", 4000);
            this.InicializarFormulario();
            await this.GetUsuarioData();
          }
        },
          error => {
            console.error(error);
          }
        );
      }
    }
  }

  GetUsuarioDataResult(): any {
    return this.usuariosservice.GetListaUsuarioGeneral().toPromise();
  }

  async GetUsuarioData() {
    try {
      this.ValorServicio = await this.GetUsuarioDataResult();

 
      this.selectUsuarios = this.ValorServicio.Lista;
    }
    catch (error) {
      console.error('[error en GetUsuarioData] : ' + error);
    }
  }

  GetUsuariosByIdResult(id: number) {
    return this.usuariosservice.GetUsuarioGeneralById(id).toPromise();
  }
  DelUsuariosByIdResult(id: number) {
    return this.usuariosservice.DelUsuarioGeneralById(id).toPromise();
  }
  async DelUsuariosById(id: number) {
    await this.DelUsuariosByIdResult(id);
  }

  async GetUsuariosById(id: number) {
    try {

      this.ValorServicio = await this.GetUsuariosByIdResult(id);
      return this.ValorServicio;


    }
    catch (error) {
      console.error('[error en GetUsuarioData] : ' + error);
    }
  }

  async Editar(id: number) {

    let usuarios: any;

    usuarios = await this.GetUsuariosById(id);

    this.usuarios.PerfilUsuarioID = usuarios.PerfilUsuarioID;
    this.usuarios.UserID = usuarios.UserID;
    this.usuarios.Nombre = usuarios.Nombre;
    this.usuarios.CorreoElectronico = usuarios.CorreoElectronico;
    this.usuarios.Contrasena = usuarios.Contrasena;
    this.usuarios.TipoDocumentoID = usuarios.TipoDocumentoID;
    this.usuarios.RazonSocial = usuarios.RazonSocial;
    this.usuarios.NombreSocial = usuarios.NombreSocial;
    this.usuarios.TipoDocumentoIDS = usuarios.TipoDocumentoID.toString();
    this.usuarios.NumeroDocumento = usuarios.NumeroDocumento;
    this.usuarios.TelefonoCelular = usuarios.TelefonoCelular;

    if (usuarios.ImagenUsuario == null)
      this.usuarios.ImagenUsuario = 'SinImagen.png';
    else
      this.usuarios.ImagenUsuario = usuarios.ImagenUsuario;

    this.usuarios.TipoDocumentoID = 1;

    this.estado = 'Upd';

  }

  open(content: any) {
    this.modalService.open(content, { backdrop: 'static', keyboard: false, size: 'lg', windowClass: 'modal-dialog-centered' });
  }

  ConfirmacionEliminar(id: number) {
    this.open(this.delete);
    this.idRecordDelete = id;
  }


  async Eliminar() {
    await this.DelUsuariosById(this.idRecordDelete);
    await this.GetUsuarioData();
    this.limpiarCampos();
  }


  limpiarCampos() {
    this.usuarios.PerfilUsuarioID = 0;
    this.usuarios.Nombre = "";
    this.usuarios.CorreoElectronico = "";
    this.usuarios.Contrasena = "";
    this.usuarios.TipoDocumentoID = 0;
    this.usuarios.TipoDocumentoIDS = '';
    this.usuarios.NumeroDocumento = '';
    this.usuarios.RazonSocial = '';
    this.usuarios.NombreSocial = '';
    this.usuarios.TelefonoCelular = '';
    this.usuarios.ImagenUsuario = 'SinImagen.png';
    this.usuarios.Imagen64 = '';
  }


  GetListaParametrosResult(tipo: string): any {
    return this.parametricasservice.GetListaParametrosById(tipo).toPromise();
  }

  async GetListaTipoDocumento() {
    try {

      this.ValorServicio = await this.GetListaParametrosResult("TIPODOCUMENTO");
      this.selectListaTipoDocumento = this.ValorServicio.Lista;
    }
    catch (error) {
      console.error('[error en GetListaAlcances] : ' + error);
    }
  }

  public async CargarTiposDocumento() {
    await this.GetListaTipoDocumento();
  }


  public async resizeImage(file: File, width: number, height: number): Promise<File> {
    return new Promise<File>((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        const img = new Image();
        img.src = reader.result as string;
        img.onload = () => {
          const canvas = document.createElement('canvas');
          const ctx = canvas.getContext('2d');
          canvas.width = width;
          canvas.height = height;
          ctx!.drawImage(img, 0, 0, width, height);
          canvas.toBlob((blob) => {
            if (blob) {
              resolve(new File([blob], file.name, { type: file.type }));
            } else {
              reject(new Error('Image resizing failed.'));
            }
          }, file.type);
        };
      };
      reader.onerror = (error) => reject(error);
    });
  }

  public async convertFileToBase64(file: File): Promise<string> {
    return new Promise<string>((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        const result = reader.result as string;
        const base64Content = result.split(',')[1];
        resolve(base64Content);
      };
      reader.onerror = (error) => reject(error);
    });
  }



  subidaCompletaImagen(e: {
    value: null; request: { response: string; status: number; };
  }) {

    try {
      if (e.value == null || e.value == undefined) {
        this.displayGuardarImagen = false;
        notify("Error cargando el archivo", "error", 4000);
      }
      else {
        this.displayGuardarImagen = true;
      }
    }
    catch (error) {
      console.error('[error en Cargue de Imagenes] : ' + error);
    }
  }

}