import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { PeriodosPublicacion } from '../../../models/periodospublicacion.model';
import { PeriodosPublicacionService } from '../../../services/periodospublicacion.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import notify from 'devextreme/ui/notify';



@Component({
  selector: 'app-periodospublicacion',
  templateUrl: './periodospublicacion.component.html',
  styleUrls: ['./periodospublicacion.component.scss']
})
export class PeriodosPublicacionComponent implements OnInit {
  @ViewChild('delete') delete: any;
  selectPeriodosPublicacion: Array<PeriodosPublicacion> = new Array<PeriodosPublicacion>;
  periodospublicacion: PeriodosPublicacion = new PeriodosPublicacion();
  estado: string = 'Add';
  idRecordDelete : number = 0; //Id del Registro a Borrar
  ValorServicio: any; //Valores devueltos por el servicio web

  constructor(
    private periodospublicacionservice: PeriodosPublicacionService,private modalService: NgbModal
  ) { }

  ngOnInit(){
    this.CargarPeriodosPublicacion();
    this.InicializarFormulario();
  }
  public async CargarPeriodosPublicacion() {
    await this.GetListaPeriodosPublicacion();
  }

  InicializarFormulario()
  {
    this.periodospublicacion.NombrePeriodo  = '';
    this.periodospublicacion.Dias = 0;
    this.periodospublicacion.PorcentajeValorPeriodo = 0;
    this.periodospublicacion.PorcentajeValorAlcance = 0;
    
    this.estado = 'Add';
  }


  ValidarPeriodosPublicacion() {

    if (this.periodospublicacion.NombrePeriodo === '') {
      notify("Debe ingresar un valor en la casilla nombre", "warning", 4000);
      return false;
    }
    else if (this.periodospublicacion.Dias === 0)
      {
        notify("Debe ingresar un valor en la casilla cantidad de dias", "warning", 4000);
        return false;
      }
    else if (this.periodospublicacion.PorcentajeValorPeriodo === 0)
    {
      notify("Debe ingresar un valor en la casilla porcentaje", "warning", 4000);
      return false;
    }
    else if(this.periodospublicacion.PorcentajeValorAlcance === 0){
      notify("Debe ingresar un valor en la casilla porcentaje Alcance", "warning", 4000);
      return false;
    }
    else
      return true;

  }

  public async AsignarPeriodosPublicacion() {

    if (this.ValidarPeriodosPublicacion()) {
      

      if (this.estado === 'Add') {

        await this.periodospublicacionservice.PostPeriodosPublicacion(this.periodospublicacion).subscribe(async result => {

          
          if (result.success == false) { 
            notify("Error al asignar periodospublicacion", "error", 4000);
          }
          else 
          {
            notify("Se ha asignado el periodospublicacion correctamente ", "success", 4000);
            this.InicializarFormulario();
            await this.GetListaPeriodosPublicacion();
          }
        },
          error => {
            console.error(error);
          }
        );
      }
      else if (this.estado === 'Upd') {

        await this.periodospublicacionservice.PutPeriodosPublicacionById(this.periodospublicacion).subscribe(async result => {

          if (result.success == false) { 
            notify("Error al actualizar la periodospublicacion", "error", 4000);
          }
          else 
          {
            notify("Se ha actualizado la periodospublicacion correctamente ", "success", 4000);
            this.InicializarFormulario();
            await this.GetListaPeriodosPublicacion();
          }
        },
          error => {
            console.error(error);
          }
        );
      }




    }
  }
  GetListaPeriodosPublicacionResult(): any {
    return this.periodospublicacionservice.GetListaPeriodosPublicacion().toPromise();
  }

  async GetListaPeriodosPublicacion() {
    try {
      this.ValorServicio = await this.GetListaPeriodosPublicacionResult();
      
   
      this.selectPeriodosPublicacion = this.ValorServicio.Lista;
    }
    catch (error) {
      console.error('[error en GetListaPeriodosPublicacion] : ' + error);
    }
  }
  GetPeriodosPublicacionByIdResult(id: number) {
    return this.periodospublicacionservice.GetPeriodosPublicacionById(id).toPromise();
  }
  DelPeriodosPublicacionByIdResult(id: number) {
    return this.periodospublicacionservice.DelPeriodosPublicacionById(id).toPromise();
  }
  async DelPeriodosPublicacionById(id: number){
    await this.DelPeriodosPublicacionByIdResult(id);
  }

  async GetPeriodosPublicacionById(id: number) {
    try {
      
      this.ValorServicio = await this.GetPeriodosPublicacionByIdResult(id);
      return this.ValorServicio;


    }
    catch (error) {
      console.error('[error en GetListaPeriodosPublicacion] : ' + error);
    }
  }

  async Editar(id: number) {

    let periodospublicacion: any;
    
    periodospublicacion = await this.GetPeriodosPublicacionById(id);

    
    this.periodospublicacion.PeriodoID = periodospublicacion.PeriodoID;
    this.periodospublicacion.NombrePeriodo = periodospublicacion.NombrePeriodo;
    this.periodospublicacion.Dias = periodospublicacion.Dias;
    this.periodospublicacion.PorcentajeValorPeriodo = periodospublicacion.PorcentajeValorPeriodo;
    this.periodospublicacion.PorcentajeValorAlcance = periodospublicacion.PorcentajeValorAlcance;
    
    this.estado = 'Upd';

    //await this.AsignarDispositivoUsuario(); //Se elimina el llamado a esta función
    await this.GetListaPeriodosPublicacion();

  }

  open(content: any) {
    this.modalService.open(content, { backdrop: 'static', keyboard: false, size: 'lg', windowClass: 'modal-dialog-centered'  });
  }

  ConfirmacionEliminar(id: number)
  {
    this.open(this.delete);
    this.idRecordDelete = id;
  }


    async Eliminar() {

    await this.DelPeriodosPublicacionById(this.idRecordDelete);
    
    await this.GetListaPeriodosPublicacion();

  }
}