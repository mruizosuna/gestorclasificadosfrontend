import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { CategoriasSecundarias, ListaCategoriasSecundarias } from '../../../models/categoriassecundarias.model';
import { CategoriasSecundariasService } from '../../../services/categoriassecundarias.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import notify from 'devextreme/ui/notify';
import { CategoriasPrincipalesService } from '../../../services/categoriasprincipales.service';
import { CategoriasPrincipales } from '../../../models/categoriasprincipales.model';



@Component({
  selector: 'app-categoriassecundarias',
  templateUrl: './categoriassecundarias.component.html',
  styleUrls: ['./categoriassecundarias.component.scss']
})
export class CategoriasSecundariasComponent implements OnInit {
  @ViewChild('delete') delete: any;
  selectCategoriasSecundarias: Array<ListaCategoriasSecundarias> = new Array<ListaCategoriasSecundarias>;
  categoriassecundarias: CategoriasSecundarias = new CategoriasSecundarias();
  estado: string = 'Add';
    
  selectCategoriasPrincipales: Array<CategoriasPrincipales> = [];
  idRecordDelete : number = 0; //Id del Registro a Borrar
  ValorServicio: any; //Valores devueltos por el servicio web

  constructor(

    private categoriassecundariasservice: CategoriasSecundariasService,private modalService: NgbModal,
    private categoriasprincipalesservice: CategoriasPrincipalesService 

  ) { }


  
  async ngOnInit(){
    
  await this.CargarCategoriasPrincipales();
  await this.CargarCategoriasSecundarias();
  await this.InicializarFormulario();
  }
  
  public async CargarCategoriasSecundarias() {

    await this.GetListaCategoriasSecundarias();
    
  }
  public async CargarCategoriasPrincipales() {

    await this.GetListaCategoriasPrincipales();
    
  }

  InicializarFormulario()
  {
    this.categoriassecundarias.NombreSubcategoria  = '';
    this.estado = 'Add';
  }

  GetListaCategoriaSecundariaResult(): any {
    return this.categoriasprincipalesservice.GetListaCategoriasPrincipales().toPromise();
  }

  async GetListaCategoriasPrincipales() {
    try {
      
      this.ValorServicio = await this.GetListaCategoriaSecundariaResult();
      this.selectCategoriasPrincipales = this.ValorServicio.Lista;
    
      var objCategoriaPrincipal: CategoriasPrincipales = new CategoriasPrincipales();
      objCategoriaPrincipal.CategoriaID = 0;
      objCategoriaPrincipal.NombreCategoria = "-- Seleccione la Categoria Principal --";
      this.selectCategoriasPrincipales.push(objCategoriaPrincipal);

    }
    catch (error) {
      console.error('[error en GetListaCategoriasPrincipal] : ' + error);
    }
  }
  
  ValidarCategoriasSecundarias() {

    if (this.categoriassecundarias.NombreSubcategoria === '') {
      notify("Debe ingresar un valor en la casilla nombre", "warning", 4000);
      return false;
    }
    else if(this.categoriassecundarias.CategoriaID === 0){
      notify("Debe seleccionar un valor de la casilla categoria principal", "warning", 4000);
      return false;
    }
    else
      return true;

  }

  

  public async AsignarCategoriasSecundarias() {

    if (this.ValidarCategoriasSecundarias()) {
      

      if (this.estado === 'Add') {

        await this.categoriassecundariasservice.PostCategoriasSecundarias(this.categoriassecundarias).subscribe(async result => {

          
          if (result.success == false) { 
            notify("Error al asignar categoria secundaria", "error", 4000);
          }
          else 
          {
            notify("Se ha asignado la categoria secundaria correctamente ", "success", 4000);
            this.InicializarFormulario();
            await this.GetListaCategoriasSecundarias();
          }
        },
          error => {
            console.error(error);
          }
        );
      }
      else if (this.estado === 'Upd') {
        
        await this.categoriassecundariasservice.PutCategoriasSecundariasById(this.categoriassecundarias).subscribe(async result => {

          if (result.success == false) { 
            notify("Error al actualizar la categoriassecundarias", "error", 4000);
          }
          else 
          {
            notify("Se ha actualizado la categoriassecundarias correctamente ", "success", 4000);
            this.InicializarFormulario();
            await this.GetListaCategoriasSecundarias();
          }
        },
          error => {
            console.error(error);
          }
        );
      }




    }
  }
  GetListaCategoriasSecundariasResult(): any {
    return this.categoriassecundariasservice.GetListaCategoriasSecundarias().toPromise();
  }

  async GetListaCategoriasSecundarias() {
    try {
      this.ValorServicio = await this.GetListaCategoriasSecundariasResult();
      

      this.selectCategoriasSecundarias = this.ValorServicio.Lista;
    }
    catch (error) {
      console.error('[error en GetListaCategoriasSecundarias] : ' + error);
    }
  }
  GetCategoriasSecundariasByIdResult(id: number) {
    return this.categoriassecundariasservice.GetCategoriasSecundariasById(id).toPromise();
  }
  DelCategoriasSecundariasByIdResult(id: number) {
    return this.categoriassecundariasservice.DelCategoriasSecundariasById(id).toPromise();
  }
  async DelCategoriasSecundariasById(id: number){
    await this.DelCategoriasSecundariasByIdResult(id);
  }

  async GetCategoriasSecundariasById(id: number) {
    try {
      
      this.ValorServicio = await this.GetCategoriasSecundariasByIdResult(id);
      return this.ValorServicio;


    }
    catch (error) {
      console.error('[error en GetListaCategoriasSecundarias] : ' + error);
    }
  }

  async Editar(id: number) {

    let categoriassecundarias: any;
    
    categoriassecundarias = await this.GetCategoriasSecundariasById(id);

    
    this.categoriassecundarias.CategoriaID = categoriassecundarias.CategoriaID;
    this.categoriassecundarias.NombreSubcategoria = categoriassecundarias.NombreSubcategoria;
    this.categoriassecundarias.SubcategoriaID = categoriassecundarias.SubcategoriaID;
    this.estado = 'Upd';

    //await this.AsignarDispositivoUsuario(); //Se elimina el llamado a esta función
    await this.GetListaCategoriasSecundarias();

  }

  open(content: any) {
    this.modalService.open(content, { backdrop: 'static', keyboard: false, size: 'lg', windowClass: 'modal-dialog-centered'  });
  }

  ConfirmacionEliminar(id: number)
  {
    this.open(this.delete);
    this.idRecordDelete = id;
  }


    async Eliminar() {

    await this.DelCategoriasSecundariasById(this.idRecordDelete);
    
    await this.GetListaCategoriasSecundarias();

  }
}