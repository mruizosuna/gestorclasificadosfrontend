import { Component } from '@angular/core';
import { Vencimientos } from '../../../models/publicaciones.model';
import { PublicacionesService } from '../../../services/publicaciones.service';
import { ActivatedRoute } from '@angular/router';
import { Service } from '../../shared/services/app.service';
import { LoggerService } from '../../../services/logger.service';

@Component({
  selector: 'app-ayuda',
  templateUrl: './ayuda.component.html',
  styleUrls: ['./ayuda.component.scss'],
  providers: [Service],
})
export class AyudaComponent {
  isMobileDevice: boolean = false;

  video: string = 'https://www.youtube.com/watch?v=fuXo_R3e_gA';

  vencimiento: Vencimientos = new Vencimientos();

  constructor(private route: ActivatedRoute, service: Service,
    private publicacionesservice: PublicacionesService,
    private logger: LoggerService,

  ) {
  }


  async ngOnInit(): Promise<void> {

    this.isMobileDevice = this.detectMobileDevice();

    await this.ActualizarVencimientosPublicaciones();


  }



  async ActualizarVencimientosPublicaciones() {


    const date = new Date();
    const year = date.getFullYear();
    const month = String(date.getMonth() + 1).padStart(2, '0'); // Los meses en JavaScript son 0-indexados, por lo que sumamos 1
    const day = String(date.getDate()).padStart(2, '0');

    const formattedDate = `${year}-${month}-${day}`;
    this.vencimiento.Fecha = formattedDate;

    await this.publicacionesservice.PutVencimientosPublicacion(this.vencimiento).subscribe(async result => {

      if (result.success == false) {
        this.logger.log("Error al actualizar vencimientos publicación");
      }

    },
      error => {
        this.logger.log(error);
      }
    );


    await this.publicacionesservice.PutRenovacionesFinVigenciaPublicacion(this.vencimiento).subscribe(async result => {

      if (result.success == false) {
        this.logger.log("Error al actualizar renovaciones fin vigencia publicación");
      }

    },
      error => {
        console.error(error);
      }
    );

  }


  detectMobileDevice(): boolean {
    const userAgent = navigator.userAgent || navigator.vendor;
    return /android|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(userAgent.toLowerCase());
  }

}
