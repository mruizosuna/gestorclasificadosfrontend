import { Component, HostListener, Input, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common'; // Asegúrate de importar esto
import { BuscadordePublicaciones, Publicaciones, PublicacionesBuscador, Vencimientos } from '../../../models/publicaciones.model';
import { PublicacionesService } from '../../../services/publicaciones.service';
import { environment } from '../../../environments/environment';
import { PuntuacionPublicacion } from '../../../models/puntuacionpublicacion.model';
import { PuntuacionVendedor } from '../../../models/puntuacionvendedor.model';
import { VistasPublicacion } from '../../../models/vistaspublicacion.model';
import { ImagenesPublicacion } from '../../../models/imagenespublicacion.model';
import { CategoriasSecundarias, ListaCategoriasSecundarias } from '../../../models/categoriassecundarias.model';
import { CategoriasPrincipales } from '../../../models/categoriasprincipales.model';
import { Zonas } from '../../../models/zonas.model';
import { Conjuntos } from '../../../models/conjuntos.model';
import { CategoriaPublicacion } from '../../../models/categoriapublicacion.model';
import { House, Service } from '../../shared/services/app.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TiposPublicacionService } from '../../../services/tipospublicacion.service';
import { AlcancesService } from '../../../services/alcances.service';
import { PeriodosPublicacionService } from '../../../services/periodospublicacion.service';
import { CategoriasPrincipalesService } from '../../../services/categoriasprincipales.service';
import { CategoriasSecundariasService } from '../../../services/categoriassecundarias.service';
import { VideoService } from '../../../services/video.service';
import { ImagenesPublicacionService } from '../../../services/imagenespublicacion.service';
import { VistasPublicacionService } from '../../../services/vistaspublicacion.service';
import { PuntuacionesPublicacionService } from '../../../services/puntuacionespublicacion.service';
import { ZonasService } from '../../../services/zonas.service';
import { ConjuntosService } from '../../../services/conjuntos.service';
import { Router } from '@angular/router';
import notify from 'devextreme/ui/notify';


@Component({
  selector: 'app-destacados',
  templateUrl: './destacados.component.html',
  styleUrls: ['./destacados.component.scss']
})
export class DestacadosComponent implements OnInit {




  @Input() displayInicio: boolean = true; // Declarar como Input
  @Input() tipoCategoria: number = 0; // Declarar como Input


  @ViewChild('detallePublicacion') detallePublicacion: any;

  publicaciones: Publicaciones = new Publicaciones();
  selectCategoriasSecundarias: Array<ListaCategoriasSecundarias> = new Array<ListaCategoriasSecundarias>;
  selectCategoriasPrincipales: Array<CategoriasPrincipales> = [];
  selectListaPublicacionBuscador: Array<PublicacionesBuscador> = new Array<PublicacionesBuscador>;
  selectListaPublicacionBuscadorFiltradoImagenes: Array<PublicacionesBuscador> = new Array<PublicacionesBuscador>;
  selectZonas: Array<Zonas> = new Array<Zonas>;
  selectConjuntos: Array<Conjuntos> = new Array<Conjuntos>;

  categoriassecundarias: CategoriasSecundarias = new CategoriasSecundarias();
  buscardorpublicaciones: BuscadordePublicaciones = new BuscadordePublicaciones();
  categoriaPublicacion: CategoriaPublicacion = new CategoriaPublicacion();
  selectUsuarios: Array<Publicaciones> = [];
  idRecordDelete: number = 0; //Id del Registro a Borrar
  ValorServicio: any; //Valores devueltos por el servicio web
  videos: Video[] = [];
  jsonvideo: Video;

  urlBlob: string;

  selectedHouse: PublicacionesBuscador;

  selectListaImagenesPublicacion: Array<ImagenesPublicacion> = new Array<ImagenesPublicacion>;
  imagenSeleccionada: ImagenesPublicacion | null = null;



  selectListaVistasPublicacion: Array<VistasPublicacion> = new Array<VistasPublicacion>;
  selectListaPuntuacionPublicacion: Array<PuntuacionPublicacion> = new Array<PuntuacionPublicacion>;
  selectListaPuntuacionVendedor: Array<PuntuacionVendedor> = new Array<PuntuacionVendedor>;

  displayVideosSugeridos: boolean = false;
  displayResultados: boolean = false;



  opciones: string[] = ['Bogotá', 'Medellín', 'Cali', 'Barranquilla', 'Cartagena', 'Bucaramanga', 'Cúcuta', 'Ibagué'];
  opcionesFiltradas: string[] = [];
  carouselInterval: number = 3000; // Intervalo de tiempo entre cada imagen (en milisegundos)
  activeSlideIndex: number = 0; // Índice de la imagen activa en el carrusel

  popupPosition: any;

  vencimiento: Vencimientos = new Vencimientos();

  showPopup: boolean = false;


  currentPage = 0;
  imagesPerPage = 5;

  numeroVistas: number = 0;
  puntuacionPublicacion: number = 0;
  puntuacionVendedor: number = 0;

  vistaspublicacion: VistasPublicacion = new VistasPublicacion();

  usuario: number;

  displaybotondorado1: boolean = true;
  displaybotonverde1: boolean = false;
  displaybotondorado2: boolean = true;
  displaybotonverde2: boolean = false;
  displaybotondorado3: boolean = true;
  displaybotonverde3: boolean = false;
  displaybotondorado4: boolean = true;
  displaybotonverde4: boolean = false;
  displaybotondorado5: boolean = true;
  displaybotonverde5: boolean = false;

  displaybotondoradovend1: boolean = true;
  displaybotonverdevend1: boolean = false;
  displaybotondoradovend2: boolean = true;
  displaybotonverdevend2: boolean = false;
  displaybotondoradovend3: boolean = true;
  displaybotonverdevend3: boolean = false;
  displaybotondoradovend4: boolean = true;
  displaybotonverdevend4: boolean = false;
  displaybotondoradovend5: boolean = true;
  displaybotonverdevend5: boolean = false;

  displayValorarPublicacion: boolean = false;
  displayValorarAnunciante: boolean = false;
  displayEnlaceValorarAnunciante: boolean = false;
  displayEnlaceValorarPublicacion: boolean = false;

  puntuacionPublicacionObj: PuntuacionPublicacion = new PuntuacionPublicacion();
  puntuacionVendedorObj: PuntuacionVendedor = new PuntuacionVendedor();



  comments = [
    { text: 'Buen servicio', stars: 5, date: new Date('2023-11-01'), usuario: 'Test' },
  ];


  visibleComments = [
    { text: 'Buen servicio', stars: 5, date: new Date('2023-11-01'), usuario: 'Test' },
  ];


  commentsAnunciante = [
    { text: 'Buen servicio', stars: 5, date: new Date('2023-11-01'), usuario: 'Test' },
  ];

  visibleCommentsAnunciante = [
    { text: 'Buen servicio', stars: 5, date: new Date('2023-11-01'), usuario: 'Test' },
  ];

  showAllComments: boolean = false;
  showAllCommentsAnunciante: boolean = false;

  currentIndex: number = 0;
  rotationInterval: any;

  currentIndexAnunciante: number = 0;
  rotationIntervalAnunciante: any;

  textoBusqueda: string = '';

  ////////////////////////////




  mensajedetalle: string = 'Detalle de la Publicación';

  houses: House[];

  currentHouse: PublicacionesBuscador;

  popupVisible = false;

  ADD_TO_FAVORITES = 'Add to Favorites';

  REMOVE_FROM_FAVORITES = 'Remove from Favorites';


  anuncios: Anuncio[] = [
    { id: 'PUB001', imagen: 'https://picsum.photos/300/200?random=1', categoria: 'Apartamentos', precio: '$250,000', titulo: 'Apartamento de Lujo en el Centro', estrellas: 4, usuario: { nombre: 'Juan Pérez', logo: 'https://picsum.photos/30/30?random=101' } },
    { id: 'PUB002', imagen: 'https://picsum.photos/300/200?random=2', categoria: 'Casas', precio: '$450,000', titulo: 'Casa de Campo Familiar', estrellas: 5, usuario: { nombre: 'María González', logo: 'https://picsum.photos/30/30?random=102' } },
    { id: 'PUB003', imagen: 'https://picsum.photos/300/200?random=3', categoria: 'Oficinas', precio: '$180,000', titulo: 'Oficina Moderna Distrito Financiero', estrellas: 3, usuario: { nombre: 'Carlos Ruiz', logo: 'https://picsum.photos/30/30?random=103' } },
    { id: 'PUB004', imagen: 'https://picsum.photos/300/200?random=4', categoria: 'Lofts', precio: '$320,000', titulo: 'Loft Industrial Renovado', estrellas: 5, usuario: { nombre: 'Ana Torres', logo: 'https://picsum.photos/30/30?random=104' } },
    { id: 'PUB005', imagen: 'https://picsum.photos/300/200?random=5', categoria: 'Penthouse', precio: '$150,000', titulo: 'Penthouse con Vista 1', estrellas: 4, usuario: { nombre: 'Jaime Méndez', logo: 'https://picsum.photos/30/30?random=105' } },
    { id: 'PUB006', imagen: 'https://picsum.photos/300/200?random=6', categoria: 'Penthouse', precio: '$250,000', titulo: 'Penthouse con Vista 2', estrellas: 2, usuario: { nombre: 'Roberto Pedraza', logo: 'https://picsum.photos/30/30?random=106' } },
    { id: 'PUB007', imagen: 'https://picsum.photos/300/200?random=7', categoria: 'Penthouse', precio: '$350,000', titulo: 'Penthouse con Vista 3', estrellas: 2, usuario: { nombre: 'Juan Perez', logo: 'https://picsum.photos/30/30?random=107' } },
    { id: 'PUB008', imagen: 'https://picsum.photos/300/200?random=8', categoria: 'Penthouse', precio: '$450,000', titulo: 'Penthouse con Vista 4', estrellas: 1, usuario: { nombre: 'Renata Gonzalez', logo: 'https://picsum.photos/30/30?random=108' } },
    { id: 'PUB009', imagen: 'https://picsum.photos/300/200?random=9', categoria: 'Penthouse', precio: '$550,000', titulo: 'Penthouse con Vista 5', estrellas: 4, usuario: { nombre: 'Samuel Castaño', logo: 'https://picsum.photos/30/30?random=109' } },
    { id: 'PUB0010', imagen: 'https://picsum.photos/300/200?random=10', categoria: 'Penthouse', precio: '$650,000', titulo: 'Penthouse con Vista 6', estrellas: 4, usuario: { nombre: 'Jorge Diaz', logo: 'https://picsum.photos/30/30?random=110' } }


  ];


  itemsPerSlide = 4;
  slides: Anuncio[][] = [];
  autoplayInterval: any;


  isMobileDevice: boolean = false;
  paginatedItems: any[] = []; // Elementos de la página actual
  itemsPerPage: number = 20; // Número de elementos por página
  totalPages: number = 0; // Número total de páginas

  isModalOpen = false; // Controla si el modal está abierto
  selectedImage: string = ''; // Almacena la ruta de la imagen seleccionada

  isModalVideoOpen = false; // Controla si el modal está abierto


  constructor(private publicacionesservice: PublicacionesService, private modalService: NgbModal,
    private tipoPublicacionservice: TiposPublicacionService, private alcancesservice: AlcancesService,
    private periodosPublicacionservice: PeriodosPublicacionService, private categoriasprincipalesservice: CategoriasPrincipalesService,
    private categoriassecundariasservice: CategoriasSecundariasService,
    private videoService: VideoService, private imagenesPublicacionservice: ImagenesPublicacionService,
    private vistaspublicacionservice: VistasPublicacionService,
    private puntuacionPublicacionservice: PuntuacionesPublicacionService,

    private zonaservice: ZonasService, private conjuntosservice: ConjuntosService,
    private router: Router
  ) {

    this.urlBlob = environment.urlBlob;
    //  this.houses = service.getHouses();
    this.usuario = Number(localStorage.getItem('userid'));
  }



  onScroll = (event: any): void => {
    try {
      let scrollPosition: number;
      let windowHeight: number;
      let bodyHeight: number;

      if (event.target.scrollingElement) {
        scrollPosition = event.target.scrollingElement.scrollTop;
        windowHeight = window.innerHeight;
        bodyHeight = event.target.scrollingElement.scrollHeight;
      } else if (event.target.documentElement) {
        scrollPosition = event.target.documentElement.scrollTop;
        windowHeight = window.innerHeight;
        bodyHeight = event.target.documentElement.scrollHeight;
      } else {
        return;
      }

      if (scrollPosition + windowHeight >= bodyHeight - 50) {
        //  this.loadMoreVideos();
      }
    } catch (error) {
      console.error('Error handling scroll event:', error);
    }
  }

  copyToClipboard(text: string): void {
    navigator.clipboard.writeText(text).then(() => {
      notify("¡URL copiada al portapapeles!", "success", 2000);
    }).catch(err => {
      console.error('Error al copiar al portapapeles:', err);
    });
  }





  getYouTubeVideoId(url: string): string | null {

    const regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
    const match = url.match(regExp);
    return (match && match[2].length === 11) ? match[2] : null;
  }

  ngOnDestroy(): void {
    window.removeEventListener('scroll', this.onScroll, true);
  }





  toggleComments() {
    this.showAllComments = !this.showAllComments;

    if (this.showAllComments) {
      // Mostrar todos los comentarios y detener la rotación
      this.visibleComments = this.comments;
      clearInterval(this.rotationInterval);
    } else {
      // Ocultar comentarios y reiniciar la rotación
      this.visibleComments = this.comments.slice(0, 3);
      // this.startCommentRotation();
    }
  }

  filterComments(type: 'positive' | 'negative') {
    if (type === 'positive') {
      // Filtrar comentarios positivos
      this.visibleComments = this.comments
        .filter(comment => comment.stars > 3)
        .sort((a, b) => this.sortByDateAndStars(a, b));
    } else if (type === 'negative') {
      // Filtrar comentarios negativos
      this.visibleComments = this.comments
        .filter(comment => comment.stars <= 3)
        .sort((a, b) => this.sortByDateAndStars(a, b));
    }
  }

  filterCommentsAnunciante(type: 'positive' | 'negative') {
    if (type === 'positive') {
      // Filtrar comentarios positivos
      this.visibleCommentsAnunciante = this.commentsAnunciante
        .filter(comment => comment.stars > 3)
        .sort((a, b) => this.sortByDateAndStars(a, b));
    } else if (type === 'negative') {
      // Filtrar comentarios negativos
      this.visibleCommentsAnunciante = this.commentsAnunciante
        .filter(comment => comment.stars <= 3)
        .sort((a, b) => this.sortByDateAndStars(a, b));
    }
  }

  // Función de ordenación por fecha y luego por puntuación
  private sortByDateAndStars(a: any, b: any): number {
    const dateA = new Date(a.date).getTime();
    const dateB = new Date(b.date).getTime();

    if (dateA !== dateB) {
      // Ordenar por fecha en orden descendente
      return dateB - dateA;
    } else {
      // Si la fecha es igual, ordenar por puntaje en orden descendente
      return b.stars - a.stars;
    }
  }



  toggleCommentsAnunciante() {
    this.showAllCommentsAnunciante = !this.showAllCommentsAnunciante;

    if (this.showAllCommentsAnunciante) {
      // Mostrar todos los comentarios y detener la rotación
      this.visibleCommentsAnunciante = this.commentsAnunciante;
      clearInterval(this.rotationInterval);
    } else {
      // Ocultar comentarios y reiniciar la rotación
      this.visibleCommentsAnunciante = this.commentsAnunciante.slice(0, 3);
      //this.startCommentRotationAnunciante();
    }
  }



  open(content: any) {
    this.modalService.open(content, { backdrop: 'static', keyboard: false, size: 'lg', windowClass: 'modal-dialog-centered' });
  }

  open2(content: any) {
    this.modalService.open(content, { backdrop: 'static', size: 'xl', keyboard: false, windowClass: 'modal-dialog-centered' });

  }



  /*******************Visualización del Detalle de la Publicación */
/*
  async showHouse(house: PublicacionesBuscador) {
    this.numeroVistas = 0;
    this.puntuacionPublicacion = 0;
    this.puntuacionVendedor = 0;

    await this.GetPuntuacionPublicacionesById(house.PublicacionID, this.usuario);

    await this.GetPuntuacionVendedorById(house.UsuarioID, this.usuario);

    await this.ConsultarVistaUsuario(this.usuario, house.PublicacionID);
    await this.GetListaImagenesPublicacion(house.PublicacionID);
    await this.GetListaPuntuacionPublicacion(house.PublicacionID);
    await this.GetListaPuntuacionVendedor(house.UsuarioID);


    this.publicaciones.PublicacionID = house.PublicacionID;
    this.publicaciones.UsuarioID = house.UsuarioID;


    this.selectedHouse = house;
    this.currentHouse = house;
    //  this.popupVisible = true;
    this.open2(this.detallePublicacion);
  }*/

    async showHouse(house: PublicacionesBuscador) {
      this.numeroVistas = 0;
      this.puntuacionPublicacion = 0;
      this.puntuacionVendedor = 0;
  
      try {
          // Esperar a que todas las operaciones asincrónicas se completen
          await Promise.all([
              this.GetPuntuacionPublicacionesById(house.PublicacionID, this.usuario),
              this.GetPuntuacionVendedorById(house.UsuarioID, this.usuario),
              this.ConsultarVistaUsuario(this.usuario, house.PublicacionID),
              this.GetListaImagenesPublicacion(house.PublicacionID),
              this.GetListaPuntuacionPublicacion(house.PublicacionID),
              this.GetListaPuntuacionVendedor(house.UsuarioID)
          ]);
  
          // Configuración de datos después de cargar todo
          this.publicaciones.PublicacionID = house.PublicacionID;
          this.publicaciones.UsuarioID = house.UsuarioID;
          this.selectedHouse = house;
          this.currentHouse = house;
  
          // Mostrar el modal
          this.open2(this.detallePublicacion);
  
      } catch (error) {
          console.error("Error al cargar los datos:", error);
          // Manejo de errores
      }
  }

  
  GetListaImagenesPublicacionResult(id: number): any {
    return this.imagenesPublicacionservice.GetListaImagenesPublicacion(id).toPromise();
  }

  async GetListaImagenesPublicacion(idPublicacion: number) {
    try {
      this.ValorServicio = await this.GetListaImagenesPublicacionResult(idPublicacion);
      this.selectListaImagenesPublicacion = this.ValorServicio.Lista;
      this.activeSlideIndex = 0;
      this.prevPage();
    }
    catch (error) {
      console.error('[error en GetListaPublicaciones] : ' + error);
    }
  }




  groupImages(array: ImagenesPublicacion[], size: number): ImagenesPublicacion[][] {

    const groupedArray: ImagenesPublicacion[][] = [];
    for (let i = 0; i < array.length; i += size) {
      groupedArray.push(array.slice(i, i + size));
    }
    return groupedArray;
  }

  seleccionarImagen(imagen: ImagenesPublicacion): void {
    this.imagenSeleccionada = imagen;


  }







  filtrarOpciones(event: any) {
    const query = event.event.target.value.toLowerCase();
    this.opcionesFiltradas = this.opciones.filter(opcion =>
      opcion.toLowerCase().includes(query)
    );
  }


  GetListaZonasResult(): any {
    return this.zonaservice.GetListaZonas().toPromise();
  }

  async GetListaZonas() {
    try {

      this.ValorServicio = await this.GetListaZonasResult();
      this.selectZonas = this.ValorServicio.Lista;

    }
    catch (error) {
      console.error('[error en GetListaZona] : ' + error);
    }
  }

  GetListaConjuntosResult(id: number): any {
    return this.conjuntosservice.GetListaConjuntosById(id).toPromise();
  }

  async GetListaConjuntos(id: number) {
    try {

      this.ValorServicio = await this.GetListaConjuntosResult(id);
      this.selectConjuntos = this.ValorServicio.Lista;

    }
    catch (error) {
      console.error('[error en GetListaConjuntos] : ' + error);
    }
  }



  public async CargarZonas() {
    await this.GetListaZonas();
  }


  public async CargarConjuntos(id: number) {
    await this.GetListaConjuntos(id);
  }




  // Método que se llama al hacer clic en una imagen de la galería
  selectImage(index: number) {
    this.activeSlideIndex = index; // Cambia la imagen activa en el carrusel
    this.pauseCarouselTemporarily();
  }

  // Método para pausar el carrusel temporalmente
  pauseCarouselTemporarily() {
    this.carouselInterval = 0; // Pausa el carrusel

    setTimeout(() => {
      this.carouselInterval = 3000; // Reanuda el carrusel después de 5 segundos
    }, 5000); // 5000 milisegundos = 5 segundos
  }


  LimpiarFiltros() {
    const currentUrl = this.router.url;
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate([currentUrl]);
    });
  }

  isMobile(): boolean {
    return window.innerWidth <= 768; // Puedes ajustar este tamaño según tus necesidades
  }







  // Método para truncar la descripción a 300 caracteres
  get truncatedDescription(): string {
    return this.selectedHouse?.Descripcion?.length > 300
      ? this.selectedHouse.Descripcion.substring(0, 300) + '...'
      : this.selectedHouse?.Descripcion;
  }

  // Método para abrir el popup
  openPopup(): void {
    this.showPopup = true;
  }

  // Método para cerrar el popup
  closePopup(): void {
    this.showPopup = false;
  }


  // Método para obtener las imágenes que se deben mostrar en la página actual
  get paginatedImages() {
    const startIndex = this.currentPage * this.imagesPerPage;
    const endIndex = startIndex + this.imagesPerPage;
    return this.selectListaImagenesPublicacion.slice(startIndex, endIndex);
  }

  // Método para avanzar a la siguiente página
  nextPage() {
    if (this.currentPage < Math.floor(this.selectListaImagenesPublicacion.length / this.imagesPerPage)) {
      this.currentPage++;
    }
  }

  // Método para retroceder a la página anterior
  prevPage() {
    if (this.currentPage > 0) {
      this.currentPage--;
    }
  }

  onImageError(event: Event) {
    const element = event.target as HTMLImageElement;
    element.src = this.urlBlob + 'SinImagen.png';
  }




  /*************************Vistas y Puntuaciones */

  //Consulta de Vistas de la Publicacion
  GetListaVistasPublicacionResult(id: number): any {
    return this.vistaspublicacionservice.GetListaVistaPuntuacionPublicacion(id).toPromise();
  }

  async GetListaVistasPublicacion(idPublicacion: number) {

    try {
      this.ValorServicio = await this.GetListaVistasPublicacionResult(idPublicacion);
      this.selectListaVistasPublicacion = this.ValorServicio.Lista;

      let contadorVistas = 0;

      // Usamos un foreach para contar cuántos elementos tienen 'VistaID'
      this.selectListaVistasPublicacion.forEach((item: any) => {
        if (item.VistaID) {
          contadorVistas++;
        }
      });

      this.numeroVistas = contadorVistas;
    }
    catch (error) {
      console.error('[error en GetListaVistaPublicacion] : ' + error);
    }


  }

  //Determinar si el usuario ya tiene una vista asociada
  GetVistaPublicacionesByIdResult(UsuarioIDVio: number, PublicacionId: number): any {
    return this.vistaspublicacionservice.GetVistaPublicacionesById(UsuarioIDVio, PublicacionId).toPromise();
  }

  async ConsultarVistaUsuario(UsuarioIDVio: number, PublicacionId: number) {
    try {

      this.ValorServicio = await this.GetVistaPublicacionesByIdResult(UsuarioIDVio, PublicacionId);


      if (this.ValorServicio.VistaID == 0) //No existe la vista de ese usuario, se adiciona
      {

        this.vistaspublicacion.PublicacionID = PublicacionId;
        this.vistaspublicacion.UsuarioIDVio = UsuarioIDVio;

        await this.vistaspublicacionservice.PostAddVistaPublicacion(this.vistaspublicacion).subscribe(async result => {

          if (result.success == false) {
            notify("Error al actualizar vista de la publicación", "error", 4000);
            await this.GetListaVistasPublicacion(PublicacionId);
          }
          else {
            await this.GetListaVistasPublicacion(PublicacionId);

          }
        },
          error => {
            console.error(error);
          }
        );

      }
      else {
        await this.GetListaVistasPublicacion(PublicacionId);
      }

    }
    catch (error) {
      console.error('[error en GetListaConjuntos] : ' + error);
    }
  }




  //Consulta de Vistas de la Publicacion
  GetListaPuntuacionPublicacionResult(id: number): any {
    return this.puntuacionPublicacionservice.GetListaPuntuacionPublicacion(id).toPromise();
  }

  async GetListaPuntuacionPublicacion(idPublicacion: number) {
    this.comments = [];
    this.visibleComments = [];

    try {

      this.ValorServicio = await this.GetListaPuntuacionPublicacionResult(idPublicacion);
      this.selectListaPuntuacionPublicacion = this.ValorServicio.Lista;

      let acumuladorPuntaje = 0;
      let contadorPuntaje = 0;

      // Usamos un foreach para contar cuántos elementos tienen 'VistaID'
      this.selectListaPuntuacionPublicacion.forEach((item: any) => {

        //this.comments.push(item.Comentario);

        this.comments.push({ text: item.Comentario, stars: item.Puntuacion, date: item.FechaPuntuacion, usuario: item.UsuarioComento });
        if (item.PuntuacionID) {
          acumuladorPuntaje = acumuladorPuntaje + item.Puntuacion;
          contadorPuntaje++;
        }
      });

      this.puntuacionPublicacion = Math.round(acumuladorPuntaje / contadorPuntaje);
      this.visibleComments = this.comments;

      this.filterComments("positive");

    }
    catch (error) {
      console.error('[error en GetListaVistaPublicacion] : ' + error);
    }


  }


  //Consulta de Puntuacion Vendedor
  GetListaPuntuacionVendedorResult(id: number): any {
    return this.puntuacionPublicacionservice.GetListaPuntuacionVendedor(id).toPromise();
  }

  async GetListaPuntuacionVendedor(idVendedor: number) {
    this.commentsAnunciante = [];
    this.visibleCommentsAnunciante = [];

    try {
      this.ValorServicio = await this.GetListaPuntuacionVendedorResult(idVendedor);
      this.selectListaPuntuacionVendedor = this.ValorServicio.Lista;

      let acumuladorPuntaje = 0;
      let contadorPuntaje = 0;

      // Usamos un foreach para contar cuántos elementos tienen 'VistaID'
      this.selectListaPuntuacionVendedor.forEach((item: any) => {
        this.commentsAnunciante.push({ text: item.Comentario, stars: item.Puntuacion, date: item.FechaPuntuacion, usuario: item.UsuarioComento });

        if (item.PuntuacionID) {
          acumuladorPuntaje = acumuladorPuntaje + item.Puntuacion;
          contadorPuntaje++;
        }
      });

      this.puntuacionVendedor = Math.round(acumuladorPuntaje / contadorPuntaje);

      this.visibleCommentsAnunciante = this.commentsAnunciante;

      this.filterCommentsAnunciante("positive");
    }
    catch (error) {
      console.error('[error en GetListaVistaPublicacion] : ' + error);
    }


  }

  selectStar(selectedIndex) {
    const container = document.getElementById('rating-stars');
    if (container) {
      const buttons = container.querySelectorAll('.star-button');

      // Itera sobre todos los botones de estrella
      buttons.forEach((button, index) => {
        const icon = button.querySelector('.fa-star'); // Accede al ícono dentro del botón

        if (icon) { // Verifica si el icono fue encontrado

          if (selectedIndex === 1) {
            this.displaybotonverde1 = true;
            this.displaybotondorado1 = false;
            this.displaybotonverde2 = false;
            this.displaybotondorado2 = true;
            this.displaybotonverde3 = false;
            this.displaybotondorado3 = true;
            this.displaybotonverde4 = false;
            this.displaybotondorado4 = true;
            this.displaybotonverde5 = false;
            this.displaybotondorado5 = true;

          }
          else if (selectedIndex === 2) {
            this.displaybotonverde1 = true;
            this.displaybotondorado1 = false;
            this.displaybotonverde2 = true;
            this.displaybotondorado2 = false;
            this.displaybotonverde3 = false;
            this.displaybotondorado3 = true;
            this.displaybotonverde4 = false;
            this.displaybotondorado4 = true;
            this.displaybotonverde5 = false;
            this.displaybotondorado5 = true;
          }
          else if (selectedIndex === 3) {
            this.displaybotonverde1 = true;
            this.displaybotondorado1 = false;
            this.displaybotonverde2 = true;
            this.displaybotondorado2 = false;
            this.displaybotonverde3 = true;
            this.displaybotondorado3 = false;
            this.displaybotonverde4 = false;
            this.displaybotondorado4 = true;
            this.displaybotonverde5 = false;
            this.displaybotondorado5 = true;
          }
          else if (selectedIndex === 4) {
            this.displaybotonverde1 = true;
            this.displaybotondorado1 = false;
            this.displaybotonverde2 = true;
            this.displaybotondorado2 = false;
            this.displaybotonverde3 = true;
            this.displaybotondorado3 = false;
            this.displaybotonverde4 = true;
            this.displaybotondorado4 = false;
            this.displaybotonverde5 = false;
            this.displaybotondorado5 = true;
          }
          else if (selectedIndex === 5) {
            this.displaybotonverde1 = true;
            this.displaybotondorado1 = false;
            this.displaybotonverde2 = true;
            this.displaybotondorado2 = false;
            this.displaybotonverde3 = true;
            this.displaybotondorado3 = false;
            this.displaybotonverde4 = true;
            this.displaybotondorado4 = false;
            this.displaybotonverde5 = true;
            this.displaybotondorado5 = false;
          }
          else if (selectedIndex === 6 || selectedIndex === 7 || selectedIndex === 8 || selectedIndex === 9 || selectedIndex === 10) {
            this.displaybotonverde1 = false;
            this.displaybotondorado1 = true;
            this.displaybotonverde2 = false;
            this.displaybotondorado2 = true;
            this.displaybotonverde3 = false;
            this.displaybotondorado3 = true;
            this.displaybotonverde4 = false;
            this.displaybotondorado4 = true;
            this.displaybotonverde5 = false;
            this.displaybotondorado5 = true;

          }





        }
      });
    }
  }

  selectStarvend(selectedIndex) {

    const container = document.getElementById('rating-stars-vend');
    if (container) {

      const buttons = container.querySelectorAll('.star-buttonvend');


      // Itera sobre todos los botones de estrella
      buttons.forEach((button, index) => {
        const icon = button.querySelector('.fa-star'); // Accede al ícono dentro del botón

        if (icon) { // Verifica si el icono fue encontrado

          if (selectedIndex === 1) {

            this.displaybotonverdevend1 = true;
            this.displaybotondoradovend1 = false;
            this.displaybotonverdevend2 = false;
            this.displaybotondoradovend2 = true;
            this.displaybotonverdevend3 = false;
            this.displaybotondoradovend3 = true;
            this.displaybotonverdevend4 = false;
            this.displaybotondoradovend4 = true;
            this.displaybotonverdevend5 = false;
            this.displaybotondoradovend5 = true;

          }
          else if (selectedIndex === 2) {
            this.displaybotonverdevend1 = true;
            this.displaybotondoradovend1 = false;
            this.displaybotonverdevend2 = true;
            this.displaybotondoradovend2 = false;
            this.displaybotonverdevend3 = false;
            this.displaybotondoradovend3 = true;
            this.displaybotonverdevend4 = false;
            this.displaybotondoradovend4 = true;
            this.displaybotonverdevend5 = false;
            this.displaybotondoradovend5 = true;
          }
          else if (selectedIndex === 3) {
            this.displaybotonverdevend1 = true;
            this.displaybotondoradovend1 = false;
            this.displaybotonverdevend2 = true;
            this.displaybotondoradovend2 = false;
            this.displaybotonverdevend3 = true;
            this.displaybotondoradovend3 = false;
            this.displaybotonverdevend4 = false;
            this.displaybotondoradovend4 = true;
            this.displaybotonverdevend5 = false;
            this.displaybotondoradovend5 = true;
          }
          else if (selectedIndex === 4) {
            this.displaybotonverdevend1 = true;
            this.displaybotondoradovend1 = false;
            this.displaybotonverdevend2 = true;
            this.displaybotondoradovend2 = false;
            this.displaybotonverdevend3 = true;
            this.displaybotondoradovend3 = false;
            this.displaybotonverdevend4 = true;
            this.displaybotondoradovend4 = false;
            this.displaybotonverdevend5 = false;
            this.displaybotondoradovend5 = true;
          }
          else if (selectedIndex === 5) {
            this.displaybotonverdevend1 = true;
            this.displaybotondoradovend1 = false;
            this.displaybotonverdevend2 = true;
            this.displaybotondoradovend2 = false;
            this.displaybotonverdevend3 = true;
            this.displaybotondoradovend3 = false;
            this.displaybotonverdevend4 = true;
            this.displaybotondoradovend4 = false;
            this.displaybotonverdevend5 = true;
            this.displaybotondoradovend5 = false;
          }
          else if (selectedIndex === 6 || selectedIndex === 7 || selectedIndex === 8 || selectedIndex === 9 || selectedIndex === 10) {
            this.displaybotonverdevend1 = false;
            this.displaybotondoradovend1 = true;
            this.displaybotonverdevend2 = false;
            this.displaybotondoradovend2 = true;
            this.displaybotonverdevend3 = false;
            this.displaybotondoradovend3 = true;
            this.displaybotonverdevend4 = false;
            this.displaybotondoradovend4 = true;
            this.displaybotonverdevend5 = false;
            this.displaybotondoradovend5 = true;

          }


        }
      });
    }
  }

  limpiarestrellas() {
    this.displaybotonverde1 = false;
    this.displaybotondorado1 = true;
    this.displaybotonverde2 = false;
    this.displaybotondorado2 = true;
    this.displaybotonverde3 = false;
    this.displaybotondorado3 = true;
    this.displaybotonverde4 = false;
    this.displaybotondorado4 = true;
    this.displaybotonverde5 = false;
    this.displaybotondorado5 = true;
  }

  limpiarestrellasVendedor() {
    this.displaybotonverdevend1 = false;
    this.displaybotondoradovend1 = true;
    this.displaybotonverdevend2 = false;
    this.displaybotondoradovend2 = true;
    this.displaybotonverdevend3 = false;
    this.displaybotondoradovend3 = true;
    this.displaybotonverdevend4 = false;
    this.displaybotondoradovend4 = true;
    this.displaybotonverdevend5 = false;
    this.displaybotondoradovend5 = true;
  }


  onDescripcionChange(event: any) {
    this.puntuacionPublicacionObj.Comentario = event.target.value;
  }

  onDescripcionChangeVendedor(event: any) {
    this.puntuacionVendedorObj.Comentario = event.target.value;
  }

  async guardarComentario() {


    if (this.displaybotondorado1 === true && this.displaybotondorado2 === true && this.displaybotondorado3 === true && this.displaybotondorado4 === true && this.displaybotondorado5 === true) {
      notify("Si desea registrar su valoración, debe marcar una de las estrellas", "warning", 4000);
    }
    else {

      let puntuacion = 0;

      if (this.displaybotonverde1) puntuacion++;
      if (this.displaybotonverde2) puntuacion++;
      if (this.displaybotonverde3) puntuacion++;
      if (this.displaybotonverde4) puntuacion++;
      if (this.displaybotonverde5) puntuacion++;

      this.puntuacionPublicacionObj.Puntuacion = puntuacion;

      this.puntuacionPublicacionObj.PublicacionID = this.publicaciones.PublicacionID;
      this.puntuacionPublicacionObj.UsuarioIDComento = this.usuario;





      await this.puntuacionPublicacionservice.PostAddPuntuacionPublicacion(this.puntuacionPublicacionObj).subscribe(async result => {
        if (result.success == false) {
          notify("Error al crear comentario de la publicación", "error", 4000);
          this.displayValorarPublicacion = false;
          this.displayEnlaceValorarPublicacion = false;
          this.puntuacionPublicacionObj.Comentario = '';

        } else {
          notify("Se ha registrado su votación de la publicación correctamente", "success", 4000);
          this.displayValorarPublicacion = false;
          this.displayEnlaceValorarPublicacion = false;
          this.puntuacionPublicacionObj.Comentario = '';
        }
        await this.GetListaPuntuacionPublicacion(this.puntuacionPublicacionObj.PublicacionID);
      },
        async error => {
          console.error(error);
          this.displayValorarPublicacion = false;
          this.displayEnlaceValorarPublicacion = false;
          this.puntuacionPublicacionObj.Comentario = '';
          await this.GetListaPuntuacionPublicacion(this.puntuacionPublicacionObj.PublicacionID);
        });
    }

  }


  async guardarComentarioVendedor() {


    if (this.displaybotondoradovend1 === true && this.displaybotondoradovend2 === true && this.displaybotondoradovend3 === true && this.displaybotondoradovend4 === true && this.displaybotondoradovend5 === true) {
      notify("Si desea registrar su valoración, debe marcar una de las estrellas", "warning", 4000);
    }
    else {


      let puntuacion = 0;

      if (this.displaybotonverdevend1) puntuacion++;
      if (this.displaybotonverdevend2) puntuacion++;
      if (this.displaybotonverdevend3) puntuacion++;
      if (this.displaybotonverdevend4) puntuacion++;
      if (this.displaybotonverdevend5) puntuacion++;

      this.puntuacionVendedorObj.Puntuacion = puntuacion

      this.puntuacionVendedorObj.VendedorID = this.publicaciones.UsuarioID;
      this.puntuacionVendedorObj.UsuarioIDComento = this.usuario;

      await this.puntuacionPublicacionservice.PostAddPuntuacionVendedor(this.puntuacionVendedorObj).subscribe(async result => {
        if (result.success == false) {
          notify("Error al crear comentario del anunciante", "error", 4000);
          this.displayValorarAnunciante = false;
          this.displayEnlaceValorarAnunciante = false;
          this.puntuacionVendedorObj.Comentario = '';

        } else {
          notify("Se ha registrado su votación del anunciante correctamente", "success", 4000);
          this.displayValorarAnunciante = false;
          this.displayEnlaceValorarAnunciante = false;
          this.puntuacionVendedorObj.Comentario = '';
        }
        await this.GetListaPuntuacionVendedor(this.puntuacionVendedorObj.VendedorID);
      },
        async error => {
          console.error(error);
          this.displayValorarAnunciante = false;
          this.displayEnlaceValorarAnunciante = false;
          this.puntuacionVendedorObj.Comentario = '';
          await this.GetListaPuntuacionVendedor(this.puntuacionVendedorObj.VendedorID);
        });
    }

  }

  //Determinar si el usuario ya tiene una puntuacion asociada
  GetPuntuacionPublicacionesByIdResult(PublicacionId: number, UsuarioIdComento: number): any {
    return this.puntuacionPublicacionservice.GetPuntuacionPublicacionesById(PublicacionId, UsuarioIdComento).toPromise();
  }

  async GetPuntuacionPublicacionesById(PublicacionId: number, UsuarioIdComento: number) {
    try {


      this.ValorServicio = await this.GetPuntuacionPublicacionesByIdResult(PublicacionId, UsuarioIdComento);


      if (!this.ValorServicio || this.ValorServicio.PuntuacionID === 0) {//No existe la vista de ese usuario, se adiciona

        /*  if (this.ValorServicio.PuntuacionID === '-1')
            this.displayEnlaceValorarPublicacion = false;
          else*/
        this.displayEnlaceValorarPublicacion = true;
      }
      else {
        this.displayEnlaceValorarPublicacion = false;

      }

    }
    catch (error) {
      console.error('[error en GetListaConjuntos] : ' + error);
    }
  }



  //Determinar si el usuario ya tiene una puntuacion asociada
  GetPuntuacionVendedorByIdResult(PublicacionId: number, UsuarioIdComento: number): any {
    return this.puntuacionPublicacionservice.GetPuntuacionVendedorById(PublicacionId, UsuarioIdComento).toPromise();
  }

  async GetPuntuacionVendedorById(PublicacionId: number, UsuarioIdComento: number) {
    try {

      this.ValorServicio = await this.GetPuntuacionVendedorByIdResult(PublicacionId, UsuarioIdComento);

      if (!this.ValorServicio || this.ValorServicio.PuntuacionID === 0) { //No existe la vista de ese usuario, se adiciona

        /* if (this.ValorServicio.PuntuacionID === -1)
           this.displayEnlaceValorarAnunciante = false;
         else*/
        this.displayEnlaceValorarAnunciante = true;
      }
      else {
        this.displayEnlaceValorarAnunciante = false;

      }

    }
    catch (error) {
      console.error('[error en GetListaConjuntos] : ' + error);
    }
  }


  toggleValorarAnunciante() {
    this.displayValorarAnunciante = !this.displayValorarAnunciante;

    if (this.displayEnlaceValorarAnunciante === true) {
      this.displayValorarPublicacion = false;
    }
  }

  toggleValorarPublicacion() {
    this.displayValorarPublicacion = !this.displayValorarPublicacion;

    if (this.displayEnlaceValorarAnunciante === true) {
      this.displayValorarAnunciante = false;
    }
  }


  async obtenerPuntuacion(id: number) {
    await this.GetListaPuntuacionPublicacion(id);
  }





  formatTitle(title: string): string {
    const minLength = 100; // Longitud mínima deseada
    const filler = " (Rellenado) "; // Texto para rellenar
    if (title.length < minLength) {
      const neededLength = minLength - title.length;
      const repeatCount = Math.ceil(neededLength / filler.length);
      return title + filler.repeat(repeatCount).slice(0, neededLength); // Rellenar con la cadena
    }
    return title.length > minLength ? title.slice(0, minLength) + '...' : title;
  }

  getFiller(currentLength: number): string {
    const minLength = 100; // Longitud mínima deseada
    return ' '.repeat(minLength - currentLength); // Rellenar con espacios en blanco
  }


  cerrarModales() {
    this.modalService.dismissAll();
  }



  


  async ngOnInit(): Promise<void> {

    this.isMobileDevice = this.detectMobileDevice();
    if (this.tipoCategoria !== 0)
      this.buscardorpublicaciones.CategoriaID = this.tipoCategoria;

    await this.BuscarPublicacionesDestacadas();
    //this.shuffleArray(this.anuncios); // Mezcla los anuncios si lo necesitas
    this.startAutoplay();
  }



getFormattedTitulo(titulo: string): string {
  const maxLength = 200;
  const description = titulo?.trim() || ''; // Elimina espacios al inicio y al final

  if (description.length >= maxLength) {
    // Si la descripción es más larga o igual a maxLength, la truncamos
    return description.substring(0, maxLength).trimLeft();
  }

  // Si la descripción es más corta, rellenamos con espacios
  return (description + ' '.repeat(maxLength - description.length)).trimLeft();
}





  getCurrentSlide(): PublicacionesBuscador[] {
    // Obtiene el bloque actual de anuncios basado en currentIndex y itemsPerSlide

    return this.selectListaPublicacionBuscador.slice(this.currentIndex, this.currentIndex + this.itemsPerSlide);
  }

  getCurrentSlideMovil(): PublicacionesBuscador[] {
    // Obtiene el bloque actual de anuncios basado en currentIndex y itemsPerSlide

    return this.selectListaPublicacionBuscador.slice(this.currentIndex, 1);
  }


  getCurrentSlideMobile(): PublicacionesBuscador[] {
    // Obtiene el bloque actual de anuncios basado en currentIndex y itemsPerSlide

    return this.selectListaPublicacionBuscador;
  }

  moveCarousel(direction: number): void {
    const totalItems = this.selectListaPublicacionBuscadorFiltradoImagenes.length;
    const maxIndex = Math.floor((totalItems - 1) / this.itemsPerSlide) * this.itemsPerSlide;

    this.currentIndex += direction * this.itemsPerSlide;

    // Asegura que currentIndex esté dentro de los límites
    if (this.currentIndex < 0) {
      this.currentIndex = maxIndex; // Ir al final
    } else if (this.currentIndex > maxIndex) {
      this.currentIndex = 0; // Regresar al inicio
    }
  }

  createStars(count: number): string {
    return '★'.repeat(count) + '☆'.repeat(5 - count);
  }

  shuffleArray(array: any[]): any[] {
    for (let i = array.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [array[i], array[j]] = [array[j], array[i]];
    }
    return array;
  }

  startAutoplay(): void {
    this.autoplayInterval = setInterval(() => this.moveCarousel(1), 7000);
  }


  async BuscarPublicacionesDestacadas() {
    await this.GetListaBuscadorPublicacionesDestacadas(this.buscardorpublicaciones.CategoriaID!, this.buscardorpublicaciones.SubCategoriaID!, this.buscardorpublicaciones.ZonasLocalidadID!, this.buscardorpublicaciones.ConjuntoID, this.buscardorpublicaciones.PalabrasClave!);
  }

  GetListaBuscadorPublicacionesDestacadasResult(categoriaid: number, subcategoriaid: number, zonalocalidadid: number, conjuntoid: number, palabrasclave: string): any {
    return this.publicacionesservice.GetListaBuscadorPublicacionesDestacadas(categoriaid, subcategoriaid, zonalocalidadid, conjuntoid, palabrasclave).toPromise();
  }

  async GetListaBuscadorPublicacionesDestacadas(categoriaid: number, subcategoriaid: number, zonalocalidadid: number, conjuntoid: number, palabrasclave: string) {
    try {

      this.ValorServicio = await this.GetListaBuscadorPublicacionesDestacadasResult(
        categoriaid,
        subcategoriaid,
        zonalocalidadid,
        conjuntoid,
        palabrasclave
      );

      this.selectListaPublicacionBuscador = this.ValorServicio.Lista;
      

      // Filtrar publicaciones duplicadas por Titulo
      const seenCategoriaIds = new Set<string>();
      this.selectListaPublicacionBuscadorFiltradoImagenes = this.selectListaPublicacionBuscador.filter(publicacion => {
        if (seenCategoriaIds.has(publicacion.Titulo)) {
          return false;
        }
        seenCategoriaIds.add(publicacion.Titulo);
        return true;
      });

      // Asigna el array filtrado a `selectListaPublicacionBuscador`
      this.selectListaPublicacionBuscador = this.selectListaPublicacionBuscadorFiltradoImagenes;

      // Mezcla el array filtrado
      this.selectListaPublicacionBuscador = this.shuffleArray(this.selectListaPublicacionBuscador);



    } catch (error) {
      console.error('[error en GetListaBuscadorPublicacion] : ' + error);
    }
  }

  capitalizeFirstLetter(text: string): string {
    return text
      .split(' ') // Divide el texto en palabras
      .map(word => word.charAt(0).toUpperCase() + word.slice(1).toLowerCase()) // Capitaliza la primera letra y convierte el resto a minúsculas
      .join(' '); // Une las palabras de nuevo
  }

  formatCurrency(value: number): string {
    if (!value) return '0';

    // Formatea el valor como COP con separadores correctos
    let formattedValue = new Intl.NumberFormat('es-CO', {
      style: 'currency',
      currency: 'COP',
      minimumFractionDigits: 0,
      maximumFractionDigits: 0
    }).format(value);

    // Reemplaza las comas por puntos si es necesario
    return formattedValue.replace(/,/g, '.');
  }

  openImageModal(image: string) {
    this.selectedImage = image;
    this.isModalOpen = true;
  }

  closeModal() {
    this.isModalOpen = false;
  }


  @HostListener('document:keydown.escape', ['$event'])
  handleEscapeKey(event: KeyboardEvent) {
    if (this.isModalOpen) {
      this.closeModal();
    }
  }




  
    
  get paddedDescription(): string {
    const maxLength = 200;
    const description = this.selectedHouse?.Descripcion?.trim() || ''; // Elimina espacios al inicio y al final
  
    if (description.length >= maxLength) {
      // Si la descripción es más larga o igual a 100, la truncamos
      return description.substring(0, maxLength);
    }
  
    // Si la descripción es más corta, rellenamos con espacios
    return description + ' '.repeat(maxLength - description.length);
  }
  
  
  
  detectMobileDevice(): boolean {
    const userAgent = navigator.userAgent || navigator.vendor ;
    return /android|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(userAgent.toLowerCase());
}



}

interface Anuncio {
  id: string;
  imagen: string;
  categoria: string;
  precio: string;
  titulo: string;
  estrellas: number;
  usuario: {
    nombre: string;
    logo: string;
  };
}

interface Video {
  RutaVideo: string;
}

interface VideoList {
  videos: Video[];
}

