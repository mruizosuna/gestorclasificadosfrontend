import { Component, HostListener, OnInit, ViewChild } from '@angular/core';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common'; // Asegúrate de importar esto
import { BuscadordePublicaciones, Publicaciones, PublicacionesBuscador, Vencimientos } from '../../../models/publicaciones.model';
import { PublicacionesService } from '../../../services/publicaciones.service';
import { environment } from '../../../environments/environment';
import { PuntuacionPublicacion } from '../../../models/puntuacionpublicacion.model';
import { PuntuacionVendedor } from '../../../models/puntuacionvendedor.model';
import { VistasPublicacion } from '../../../models/vistaspublicacion.model';
import { ImagenesPublicacion } from '../../../models/imagenespublicacion.model';
import { CategoriasSecundarias, ListaCategoriasSecundarias } from '../../../models/categoriassecundarias.model';
import { CategoriasPrincipales } from '../../../models/categoriasprincipales.model';
import { Zonas } from '../../../models/zonas.model';
import { Conjuntos } from '../../../models/conjuntos.model';
import { CategoriaPublicacion } from '../../../models/categoriapublicacion.model';
import { House, Service } from '../../shared/services/app.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TiposPublicacionService } from '../../../services/tipospublicacion.service';
import { AlcancesService } from '../../../services/alcances.service';
import { PeriodosPublicacionService } from '../../../services/periodospublicacion.service';
import { CategoriasPrincipalesService } from '../../../services/categoriasprincipales.service';
import { CategoriasSecundariasService } from '../../../services/categoriassecundarias.service';
import { VideoService } from '../../../services/video.service';
import { ImagenesPublicacionService } from '../../../services/imagenespublicacion.service';
import { VistasPublicacionService } from '../../../services/vistaspublicacion.service';
import { PuntuacionesPublicacionService } from '../../../services/puntuacionespublicacion.service';
import { ZonasService } from '../../../services/zonas.service';
import { ConjuntosService } from '../../../services/conjuntos.service';
import { Router } from '@angular/router';
import notify from 'devextreme/ui/notify';

@Component({
  templateUrl: 'home.component.html',
  styleUrls: [ './home.component.scss' ],
  providers: [Service],

})

export class HomeComponent implements OnInit {

  async ngOnInit(): Promise<void> {
 
  }

}