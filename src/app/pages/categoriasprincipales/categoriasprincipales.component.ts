import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { CategoriasPrincipales } from '../../../models/categoriasprincipales.model';
import { CategoriasPrincipalesService } from '../../../services/categoriasprincipales.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import notify from 'devextreme/ui/notify';



@Component({
  selector: 'app-categoriasprincipales',
  templateUrl: './categoriasprincipales.component.html',
  styleUrls: ['./categoriasprincipales.component.scss']
})
export class CategoriasPrincipalesComponent implements OnInit {
  @ViewChild('delete') delete: any;
  selectCategoriasPrincipales: Array<CategoriasPrincipales> = new Array<CategoriasPrincipales>;
  categoriasprincipales: CategoriasPrincipales = new CategoriasPrincipales();
  estado: string = 'Add';
  idRecordDelete : number = 0; //Id del Registro a Borrar
  ValorServicio: any; //Valores devueltos por el servicio web

  constructor(

    private categoriasprincipalesservice: CategoriasPrincipalesService,private modalService: NgbModal


  ) { }


  
  ngOnInit(){
    this.CargarCategoriasPrincipales();
    this.InicializarFormulario();
  }
  public async CargarCategoriasPrincipales() {


    



    await this.GetListaCategoriasPrincipales();
    
  }
  InicializarFormulario()
  {
    this.categoriasprincipales.NombreCategoria  = '';
    this.estado = 'Add';
  }


  ValidarCategoriasPrincipales() {

    if (this.categoriasprincipales.NombreCategoria === '') {
      notify("Debe ingresar un valor en la casilla nombre", "warning", 4000);
      return false;
    }
    else
      return true;

  }

  public async AsignarCategoriasPrincipales() {

    if (this.ValidarCategoriasPrincipales()) {
      

      if (this.estado === 'Add') {

        await this.categoriasprincipalesservice.PostCategoriasPrincipales(this.categoriasprincipales).subscribe(async result => {

          
          if (result.success == false) { 
            notify("Error al asignar categoriasprincipales", "error", 4000);
          }
          else 
          {
            notify("Se ha asignado la categoriasprincipales correctamente ", "success", 4000);
            this.InicializarFormulario();
            await this.GetListaCategoriasPrincipales();
          }
        },
          error => {
            console.error(error);
          }
        );
      }
      else if (this.estado === 'Upd') {

        await this.categoriasprincipalesservice.PutCategoriasPrincipalesById(this.categoriasprincipales).subscribe(async result => {

          if (result.success == false) { 
            notify("Error al actualizar la categoriasprincipales", "error", 4000);
          }
          else 
          {
            notify("Se ha actualizado la categoriasprincipales correctamente ", "success", 4000);
            this.InicializarFormulario();
            await this.GetListaCategoriasPrincipales();
          }
        },
          error => {
            console.error(error);
          }
        );
      }




    }
  }

  GetListaCategoriasPrincipalesResult(): any {
    return this.categoriasprincipalesservice.GetListaCategoriasPrincipales().toPromise();
  }

  async GetListaCategoriasPrincipales() {
    try {
      this.ValorServicio = await this.GetListaCategoriasPrincipalesResult();
      
     
      this.selectCategoriasPrincipales = this.ValorServicio.Lista;
    }
    catch (error) {
      console.error('[error en GetListaCategoriasPrincipales] : ' + error);
    }
  }
  
  GetCategoriasPrincipalesByIdResult(id: number) {
    return this.categoriasprincipalesservice.GetCategoriasPrincipalesById(id).toPromise();
  }
  DelCategoriasPrincipalesByIdResult(id: number) {
    return this.categoriasprincipalesservice.DelCategoriasPrincipalesById(id).toPromise();
  }
  async DelCategoriasPrincipalesById(id: number){
    await this.DelCategoriasPrincipalesByIdResult(id);
  }

  async GetCategoriasPrincipalesById(id: number) {
    try {
      
      this.ValorServicio = await this.GetCategoriasPrincipalesByIdResult(id);
      return this.ValorServicio;


    }
    catch (error) {
      console.error('[error en GetListaCategoriasPrincipales] : ' + error);
    }
  }

  async Editar(id: number) {

    let categoriasprincipales: any;
    
    categoriasprincipales = await this.GetCategoriasPrincipalesById(id);

    
    this.categoriasprincipales.CategoriaID = categoriasprincipales.CategoriaID;
    this.categoriasprincipales.NombreCategoria = categoriasprincipales.NombreCategoria;

    this.estado = 'Upd';

    //await this.AsignarDispositivoUsuario(); //Se elimina el llamado a esta función
    await this.GetListaCategoriasPrincipales();

  }

  open(content: any) {
    this.modalService.open(content, { backdrop: 'static', keyboard: false, size: 'lg', windowClass: 'modal-dialog-centered'  });
  }

  ConfirmacionEliminar(id: number)
  {
    this.open(this.delete);
    this.idRecordDelete = id;
  }


    async Eliminar() {

    await this.DelCategoriasPrincipalesById(this.idRecordDelete);
    
    await this.GetListaCategoriasPrincipales();

  }
}