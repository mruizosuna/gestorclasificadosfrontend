import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import notify from 'devextreme/ui/notify';
import { PublicacionesService } from '../../../services/publicaciones.service';
import { Publicaciones } from '../../../models/publicaciones.model';
import { ListaPagos, Pagos } from '../../../models/pagos.model';

import { PagosService } from '../../../services/pagos.service';



@Component({
  selector: 'app-pagos',
  templateUrl: './pagos.component.html',
  styleUrls: ['./pagos.component.scss']
})
export class PagosComponent implements OnInit {
  @ViewChild('delete') delete: any;
  selectPagos: Array<ListaPagos> = new Array<ListaPagos>;
  pagos: Pagos = new Pagos();
  estado: string = 'Add';
    
  selectPublicaciones: Array<Publicaciones> = [];
  idRecordDelete : number = 0; //Id del Registro a Borrar
  ValorServicio: any; //Valores devueltos por el servicio web

  constructor(

    private pagosservice: PagosService,private modalService: NgbModal,
    private publicacionesservice: PublicacionesService 

  ) { }


  
  async ngOnInit(){
    
  await this.CargarPublicaciones();
  await this.CargarPagos();
  await this.InicializarFormulario();
  }
  
  public async CargarPagos() {

    await this.GetListaPagos();
    
  }
  public async CargarPublicaciones() {

    await this.GetListaPublicaciones();
    
  }

  InicializarFormulario()
  {
    this.pagos.FechaPago  = '';
    
    this.estado = 'Add';
  }

  GetListaPagosResult(): any {
    return this.pagosservice.GetListaPagos().toPromise();
  }

  async GetListaPublicaciones() {
    try {
      
      this.ValorServicio = await this.GetListaPagosResult();
      this.selectPublicaciones = this.ValorServicio.Lista;
  
      var objPublicaciones: Publicaciones = new Publicaciones();
      objPublicaciones.PublicacionID = 0;
      objPublicaciones.UsuarioID = 0;
      objPublicaciones.FechaCreacion = "";
      objPublicaciones.FechaVencimiento = "";
      objPublicaciones.TipoPublicacionID = 0;
      objPublicaciones.AlcanceID = 0;
      objPublicaciones.PeriodoID = 0;
      objPublicaciones.Titulo = "";
      objPublicaciones.Descripcion = "";
      objPublicaciones.EstadoAprobacionID = 0;
      objPublicaciones.PagoRealizado = false;

      this.selectPublicaciones.push(objPublicaciones);

    }
    catch (error) {
      console.error('[error en GetListaCategoriasPrincipal] : ' + error);
    }
  }
  
  ValidarPagos() {

    if (this.pagos.FechaPago === '') {
      notify("Debe ingresar un valor en la casilla nombre", "warning", 4000);
      return false;
    }
    else if(this.pagos.PagoID === 0){
      notify("Debe seleccionar un valor de la casilla categoria principal", "warning", 4000);
      return false;
    }
    else
      return true;

  }

  

  public async AsignarPagos() {

    if (this.ValidarPagos()) {
      

      if (this.estado === 'Add') {

        await this.pagosservice.PostPagos(this.pagos).subscribe(async result => {

          
          if (result.success == false) { 
            notify("Error al asignar pagos", "error", 4000);
          }
          else 
          {
            notify("Se ha asignado la pagos correctamente ", "success", 4000);
            this.InicializarFormulario();
            await this.GetListaPagos();
          }
        },
          error => {
            console.error(error);
          }
        );
      }
      else if (this.estado === 'Upd') {
        
        await this.pagosservice.PutPagosById(this.pagos).subscribe(async result => {

          if (result.success == false) { 
            notify("Error al actualizar la pagos", "error", 4000);
          }
          else 
          {
            notify("Se ha actualizado la pagos correctamente ", "success", 4000);
            this.InicializarFormulario();
            await this.GetListaPagos();
          }
        },
          error => {
            console.error(error);
          }
        );
      }




    }
  }


  async GetListaPagos() {
    try {
      this.ValorServicio = await this.GetListaPagosResult();
      

      this.selectPagos = this.ValorServicio.Lista;
    }
    catch (error) {
      console.error('[error en GetListaPagos] : ' + error);
    }
  }
  GetPagosByIdResult(id: number) {
    return this.pagosservice.GetPagosById(id).toPromise();
  }
  DelPagosByIdResult(id: number) {
    return this.pagosservice.DelPagosById(id).toPromise();
  }
  async DelPagosById(id: number){
    await this.DelPagosByIdResult(id);
  }

  async GetPagosById(id: number) {
    try {
      
      this.ValorServicio = await this.GetPagosByIdResult(id);
      return this.ValorServicio;


    }
    catch (error) {
      console.error('[error en GetListaPagos] : ' + error);
    }
  }

  async Editar(id: number) {

    let pagos: any;
    
    pagos = await this.GetPagosById(id);

    
    this.pagos.PagoID = pagos.PagoID;
    this.pagos.PublicacionID = pagos.PublicacionID;
    this.pagos.FechaPago = pagos.FechaPago;
    this.pagos.MontoaPagar = pagos.MontoaPagar;

    this.estado = 'Upd';

    //await this.AsignarDispositivoUsuario(); //Se elimina el llamado a esta función
    await this.GetListaPagos();

  }

  open(content: any) {
    this.modalService.open(content, { backdrop: 'static', keyboard: false, size: 'lg', windowClass: 'modal-dialog-centered'  });
  }

  ConfirmacionEliminar(id: number)
  {
    this.open(this.delete);
    this.idRecordDelete = id;
  }


    async Eliminar() {

    await this.DelPagosById(this.idRecordDelete);
    
    await this.GetListaPagos();

  }
}