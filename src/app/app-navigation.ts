

  export const navigation = [

  
/*    {
      text: 'Inicio',
      path: '/home',
      icon: 'home',
      roles : 'Administrador,Publicador'
    },*/
    {
      text: 'Inicio',
      path: 'home',
      icon: 'home',
      roles : 'Administrador,Publicador,Consulta'
    },
   
    {
      text: 'Anuncios',
      path: '/busqueda/Anuncios',
      icon: 'product',
      roles : 'Administrador,Publicador,Consulta'
    
    },
    {
      text: 'Servicios',
      path: '/busqueda/Servicios',
      icon: 'card',
      roles : 'Administrador,Publicador,Consulta'
    
    },
    {
      text: 'Eventos',
      path: '/busqueda/Eventos',
      icon: 'event',
      roles : 'Administrador,Publicador,Consulta'
    
    },
    {
      text: 'Empleos',
      path: '/busqueda/Empleos',
      icon: 'card',
      roles : 'Administrador,Publicador,Consulta'
    
    },
    {
      text: 'Mis Publicaciones',
      path: '/publicaciones',
      roles : 'Administrador,Publicador',
      icon : 	'checklist'
    },
    {
      text: 'Ayuda y Soporte',
      path: '/ayuda',
      icon: 'tel',
      roles : 'Administrador,Publicador,Consulta'
    
    },
    {
      text: 'Parametrización',
      icon: 'folder',
      items: [
        {
          text: 'Gestión de Usuarios',
          path: '/usuarios',
          roles : 'Administrador'
        
        },
        {
          text: 'Zonas',
          path: '/zonas',
          roles : 'Administrador'
        
        },
       {
          text: 'Alcances',
          path: '/alcances',
          roles : 'Administrador'
        
        },
        {
          text: 'CategoriasPrincipales',
          path: '/categoriasprincipales',
          roles : 'Administrador'
        
        },
        {
          text: 'CategoriasSecundarias',
          path: '/categoriassecundarias',
          roles : 'Administrador'
        
        },
        {
          text: 'ValorPublicacion',
          path: '/valorpublicacion',
          roles : 'Administrador'
        
        },
        {
          text: 'Localizaciones',
          path: '/localizaciones',
          roles : 'Administrador'
        
        },
        {
          text: 'PeriodosPublicacion',
          path: '/periodospublicacion',
          roles : 'Administrador'
        
        },
        {
          text: 'TiposPublicacion',
          path: '/tipospublicacion',
          roles : 'Administrador'
        
        },
        
       
        {
          text: 'Pagos',
          path: '/pagos',
          roles : 'Administrador'
        
        },
        
      ],
      roles : 'Administrador'
    },
    {
      text: 'Cerrar Sesión',
      icon: 'runner',
      roles: 'Administrador,Publicador',
      action: 'logout'  // Agrega un identificador de acción
    }
    

  ];
 







