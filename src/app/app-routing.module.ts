import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginFormComponent, ResetPasswordFormComponent, CreateAccountFormComponent, ChangePasswordFormComponent } from './shared/components';
import { AuthGuardService } from './shared/services';
import { HomeComponent } from './pages/home/home.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { DxDataGridModule, DxFormModule } from 'devextreme-angular';
import { ZonasComponent } from './pages/zonas/zonas.component';
import { AlcancesComponent } from './pages/alcances/alcances.component';
import {CategoriasPrincipalesComponent} from './pages/categoriasprincipales/categoriasprincipales.component';
import { PerfilComponent } from './pages/perfil/perfil.component';
import { PeriodosPublicacionComponent } from './pages/periodospublicacion/periodospublicacion.component';
import { TiposPublicacionComponent } from './pages/tipospublicacion/tipospublicacion.component';
import { ConjuntosComponent } from './pages/conjuntos/conjuntos.component';
import { ValorPublicacionComponent } from './pages/valorpublicacion/valorpublicacion.component';
import { UsuariosComponent } from './pages/usuarios/usuarios.component';
import { PublicacionesComponent } from './pages/publicaciones/publicaciones.component';
import { CategoriasSecundariasComponent } from './pages/categoriassecundarias/categoriassecundarias.component';
import { BusquedaComponent } from './pages/busqueda/busqueda.component';
import { PagosComponent } from './pages/pagos/pagos.component';
import { PagopayuComponent } from './pages/pagopayu/pagopayu.component';
import { ConfirmacionPayuComponent } from './pages/confirmacion-payu/confirmacion-payu.component';
import { LandingPageComponent } from './shared/components/landing-page/landing-page.component';
import { AyudaComponent } from './pages/ayuda/ayuda.component';
import { CommonModule } from '@angular/common'; // Asegúrate de importar esto
import { DestacadosComponent } from './pages/destacados/destacados.component';



const routes: Routes = [
 
  
  {
    path: 'zonas',
    component: ZonasComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'alcances',
    component: AlcancesComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'categoriasprincipales',
    component: CategoriasPrincipalesComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'categoriassecundarias',
    component: CategoriasSecundariasComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'valorpublicacion',
    component: ValorPublicacionComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'localizaciones',
    component: ConjuntosComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'periodospublicacion',
    component: PeriodosPublicacionComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'tipospublicacion',
    component: TiposPublicacionComponent,
    canActivate: [AuthGuardService]
  },
  {
  path: 'usuarios',
    component: UsuariosComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'perfil',
      component: PerfilComponent,
      canActivate: [AuthGuardService]
    },
  {
  path: 'publicaciones',
    component: PublicacionesComponent,
    canActivate: [AuthGuardService]
  },
  {
  path: 'pagos',
    component: PagosComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'busqueda/:tipo',
      component: BusquedaComponent,
      canActivate: [AuthGuardService]
    },
    
  {
    path: 'perfil',
    component: PerfilComponent,
    canActivate: [AuthGuardService]
  },
  
  {
    path: 'ayuda',
    component: AyudaComponent,
    canActivate: [AuthGuardService]
  },
  /*{
    path: 'home',
    component: HomeComponent,
    canActivate: [AuthGuardService]
  },*/
  {
    path: 'home',
    component: DestacadosComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'login-form',
    component: LoginFormComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'landing-page',
    component: LandingPageComponent,
    //canActivate: [AuthGuardService]
  },
  {
    path: 'reset-password',
    component: ResetPasswordFormComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'create-account',
    component: CreateAccountFormComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'change-password/:recoveryCode',
    component: ChangePasswordFormComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'pagar-payu',
    component: PagopayuComponent,
    canActivate: [AuthGuardService]
  },

  { path: 'confirmacion-payu', 
    component: ConfirmacionPayuComponent ,
    canActivate: [AuthGuardService]
  },

  { path: 'detalle-publicacion', 
    //component: DetallepublicacionComponent ,
    component: BusquedaComponent
  },
  {
    path: '**',
    redirectTo: 'home'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true }), DxDataGridModule, DxFormModule,CommonModule],
  providers: [AuthGuardService],
  exports: [RouterModule],
  declarations: [HomeComponent, ProfileComponent],
  
})
export class AppRoutingModule { }
