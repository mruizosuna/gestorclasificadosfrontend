import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot } from '@angular/router';

const defaultPath = '/';

interface User {
  email: string | null;
  avatarUrl: string | null;
}

@Injectable()
export class AuthService {
  public defaultUser: User = {
    email: null,
    avatarUrl: null
  };
  private _user: User | null = this.defaultUser;
  public loggedIn: boolean = false;

  private _lastAuthenticatedPath: string = defaultPath;
  set lastAuthenticatedPath(value: string) {
    this._lastAuthenticatedPath = value;
  }

  constructor(private router: Router) { }

  async logIn(email: string, password: string) {
    try {
      // Send request
      this._user = { ...this.defaultUser, email };
      this.router.navigate([this._lastAuthenticatedPath]);

      return {
        isOk: true,
        data: this._user
      };
    } catch {
      return {
        isOk: false,
        message: "Authentication failed"
      };
    }
  }

  async getUser() {
    try {
      // Send request

      return {
        isOk: true,
        data: this._user
      };
    }
    catch {
      return {
        isOk: false
      };
    }
  }

  async createAccount(email: string, password: string) {
    try {
      // Send request

      this.router.navigate(['/create-account']);
      return {
        isOk: true
      };
    }
    catch {
      return {
        isOk: false,
        message: "Failed to create account"
      };
    }
  }

  async changePassword(email: string, recoveryCode: string) {
    try {
      // Send request

      return {
        isOk: true
      };
    }
    catch {
      return {
        isOk: false,
        message: "Failed to change password"
      }
    }
  }

  async resetPassword(email: string) {
    try {
      // Send request

      return {
        isOk: true
      };
    }
    catch {
      return {
        isOk: false,
        message: "Failed to reset password"
      };
    }
  }

  async logOut() {
    this._user = null;
    this.loggedIn = false;
    this.router.navigate(['/login-form']);
  }
}

@Injectable()
export class AuthGuardService implements CanActivate {
  private hasRedirectedForPago: boolean = false;
  constructor(private router: Router, private authService: AuthService) { }

  canActivate(route: ActivatedRouteSnapshot): boolean {
    const isLoggedIn = this.authService.loggedIn;
    const routeConfigPath = route.routeConfig?.path ?? '';
    const isAuthForm = [
      'login-form',
      'reset-password',
      'create-account',
      'change-password/:recoveryCode'
    ].includes(routeConfigPath);


    // Verificar la variable 'pago' en el localStorage
    const pago = localStorage.getItem('pagoPayu');
  

    if (pago && pago !== null)
        this.authService.loggedIn = true;

      
    // Redirigir a publicaciones si la variable 'pago' tiene valor y no estamos ya en publicaciones
    
    if (pago && pago !== null && !this.hasRedirectedForPago && routeConfigPath !== 'publicaciones') {
      this.hasRedirectedForPago = true;
      this.router.navigate(['/publicaciones']);
      return false;
    }
  
      
    if (isLoggedIn && isAuthForm) {
      this.authService.lastAuthenticatedPath = defaultPath;
      this.router.navigate([defaultPath]);
      return false;
    }
  
    if (!isLoggedIn && !isAuthForm) {
    
      this.router.navigate(['/login-form']);
    }
  
    if (isLoggedIn) {
      this.authService.lastAuthenticatedPath = routeConfigPath || defaultPath;
    }
  
    return isLoggedIn || isAuthForm;
  }
}
