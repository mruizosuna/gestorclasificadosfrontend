import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot } from '@angular/router';
import { EmailService } from '../../../services/email.service';
import { EmailContactenos } from '../../../models/emailparametros.model';
import { UsuarioService } from '../../../services/usuarios.service';
import { UsuarioData } from '../../../models/usuario.model';
import { lastValueFrom } from 'rxjs';
const defaultPath = '/';

interface User {
  email: string | null;
  avatarUrl: string | null;
}

@Injectable()
export class AuthService {
  public defaultUser: User = {
    email: null,
    avatarUrl: null
  };
  private _user: User | null = this.defaultUser;
  public loggedIn: boolean = false;
  ValorServicio: any; //Valores devueltos por el servicio web
  usuarios: UsuarioData = new UsuarioData();

  private _lastAuthenticatedPath: string = defaultPath;
  set lastAuthenticatedPath(value: string) {
    this._lastAuthenticatedPath = value;
  }

  constructor(private router: Router, private emailservice: EmailService, private usuariosservice: UsuarioService,) {

  }

  async logIn(email: string, password: string) {
    try {
      // Send request
      this._user = { ...this.defaultUser, email };
      this.router.navigate([this._lastAuthenticatedPath]);

      return {
        isOk: true,
        data: this._user
      };
    } catch {
      return {
        isOk: false,
        message: "Authentication failed"
      };
    }
  }

  async getUser() {
    try {
      // Send request

      return {
        isOk: true,
        data: this._user
      };
    }
    catch {
      return {
        isOk: false
      };
    }
  }

  async createAccount(email: string, password: string) {
    try {
      // Send request

      this.router.navigate(['/create-account']);
      return {
        isOk: true
      };
    }
    catch {
      return {
        isOk: false,
        message: "Failed to create account"
      };
    }
  }



  GetUsuariosByIdResult(id: number) {
    return this.usuariosservice.GetUsuarioGeneralById(id).toPromise();
  }

  async GetUsuariosById(id: number) {
    try {

      this.ValorServicio = await this.GetUsuariosByIdResult(id);
      return this.ValorServicio;


    }
    catch (error) {
      console.error('[error en GetUsuarioData] : ' + error);
    }
  }



  async changePassword(password: string, recoveryCode: string, userId: number) {
    try {
      // Send request

      //1) Se consulta si el codigo si existe
      let existeCodigo = await this.GetUsuariosById(userId);

      if (existeCodigo.CodigoCambioPassword) {

        if (existeCodigo.CodigoCambioPassword === recoveryCode) // Los codigos son iguales
        {

          this.usuarios.UserID = userId;
          this.usuarios.Contrasena = password;

          await this.usuariosservice.PutUsuarioContrasenaById(this.usuarios).subscribe(async result => {
            if (result.success == false) {
              return {
                isOk: false,
                message: "Error cambiando la contraseña. Si requiere soporte, comuniquese a soporte@vecinoemprende.com"
              }
            } else {
              return {
                isOk: false,
                message: "Error cambiando la contraseña. Si requiere soporte, comuniquese a soporte@vecinoemprende.com"
              }

            }
          }, error => {
            console.error(error);
            return {
              isOk: false,
              message: "Error cambiando la contraseña. Si requiere soporte, comuniquese a soporte@vecinoemprende.com"
            }

          });

        }
        else {
          return {
            isOk: false,
            message: "El codigo de Verificación no coincide. Por favor realice el proceso desde la opción Olvidé mi contraseña"
          }
        }
      }
      else {
        return {
          isOk: false,
          message: "El codigo de Verificación no ha sido generado. Por favor realice el proceso desde la opción Olvidé mi contraseña"
        }
      }

      //2) Si el codigo existe entonces se procede a hacer el cambio de contraseña

      return {
        isOk: true
      };
    }
    catch {
      return {
        isOk: false,
        message: "Failed to change password"
      }
    }
  }

  async resetPassword(email: string) {
    try {
      let emailRespuesta = new EmailContactenos();
      emailRespuesta.Email = email;
  
      // Convertimos la suscripción en una promesa
      const result = await lastValueFrom(this.emailservice.EnviarEmailRecuperacionContrasena(emailRespuesta));
  
      if (result.OperacionExitosa) {
        return {
          isOk: true
        };
      } else {
        console.log(result.Mensaje);
        return {
          isOk: false,
          message: "El correo suministrado podría no ser válido o no se encuentra registrado"
        };
      }
    } catch (error) {
      console.error("Error al enviar correo:", error);
      return {
        isOk: false,
        message: "El correo suministrado podría no ser válido o no se encuentra registrado"
      };
    }
  }

  async logOut() {
    this._user = null;
    this.loggedIn = false;
    localStorage.clear();
    this.router.navigate(['/login-form']);
  }
}

@Injectable()
export class AuthGuardService implements CanActivate {
  private hasRedirectedForPago: boolean = false;
  constructor(private router: Router, private authService: AuthService) { }

  canActivate(route: ActivatedRouteSnapshot): boolean {
    const isLoggedIn = this.authService.loggedIn;
    const routeConfigPath = route.routeConfig?.path ?? '';
    const isAuthForm = [
      'login-form',
      'reset-password',
      'create-account',
      'change-password/:recoveryCode'
    ].includes(routeConfigPath);


    // Verificar la variable 'pago' en el localStorage
    const pago = localStorage.getItem('pagoPayu');


    if (pago && pago !== null)
      this.authService.loggedIn = true;


    // Redirigir a publicaciones si la variable 'pago' tiene valor y no estamos ya en publicaciones

    if (pago && pago !== null && !this.hasRedirectedForPago && routeConfigPath !== 'publicaciones') {
      this.hasRedirectedForPago = true;
      this.router.navigate(['/publicaciones']);
      return false;
    }


    if (isLoggedIn && isAuthForm) {
      this.authService.lastAuthenticatedPath = defaultPath;
      this.router.navigate([defaultPath]);
      return false;
    }

    if (!isLoggedIn && !isAuthForm) {

      this.router.navigate(['/login-form']);
    }

    if (isLoggedIn) {
      this.authService.lastAuthenticatedPath = routeConfigPath || defaultPath;
    }

    return isLoggedIn || isAuthForm;
  }
}
