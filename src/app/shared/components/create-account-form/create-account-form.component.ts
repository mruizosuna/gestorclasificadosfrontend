import { CommonModule } from '@angular/common';
import { Component, Inject, NgModule, ViewChild } from '@angular/core';
import { Router, RouterModule } from '@angular/router';
import { DxFormModule } from 'devextreme-angular/ui/form';
import { DxLoadIndicatorModule } from 'devextreme-angular/ui/load-indicator';
import notify from 'devextreme/ui/notify';
import { AuthService } from '../../services';
import { HttpClient } from '@angular/common/http';
import { UsuarioService } from '../../../../services/usuarios.service'
import { Usuario, UsuarioData } from '../../../../models/usuario.model'

import { Pagos } from '../../../../models/pagos.model';
import { Descuentos } from '../../../../models/descuentos.model';
import { Publicaciones } from '../../../../models/publicaciones.model';
import { PagosService } from '../../../../services/pagos.service';
import { PublicacionesService } from '../../../../services/publicaciones.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { trigger } from 'devextreme/events';
import { EmailParametros } from '../../../../models/emailparametros.model';
import { EmailService } from '../../../../services/email.service';
import { environment } from '../../../../environments/environment';
import { LoggerService } from '../../../../services/logger.service';


@Component({
  selector: 'app-create-account-form',
  templateUrl: './create-account-form.component.html',
  styleUrls: ['./create-account-form.component.scss']
})
export class CreateAccountFormComponent {
  @ViewChild('creacionCuenta') creacionCuenta: any;

  // Variables para controlar el tooltip
  tooltipVisible = false;
  tooltipMessage = '';
  tooltipTarget: any = null;


  showAlias = true;
  showPhone = true;


  loading = false;
  formData: any = {};
  displayPassword = true;
  showMe: boolean = true;
  error: any; //Devolución de mensaje de error

  usuario: UsuarioData = new UsuarioData();
  ValorServicio: any; //Valores devueltos por el servicio web

  AcceptPolicy: boolean = false;

  CodigoBienvenida: string | undefined;

  pagos: Pagos = new Pagos();
  descuentos: Descuentos = new Descuentos();
  publicaciones: Publicaciones = new Publicaciones();

  displayNextStepActivateAccount: boolean = false;
  contrasena: string = '';
  whatsappLink: string = '';

  constructor(
    private usuarioservice: UsuarioService,
    private authService: AuthService,
    private router: Router,
    private pagosservice: PagosService,
    private modalService: NgbModal,
    private emailservice: EmailService,
    private logger: LoggerService,


    @Inject(HttpClient) http: HttpClient
  ) { 

    localStorage.setItem('AcceptPolicy', "false");
  }


  // Métodos para mostrar y ocultar el tooltip
  showTooltip(target: any, message: string) {
    this.tooltipTarget = target;
    this.tooltipMessage = message;
    this.tooltipVisible = true;
  }

  hideTooltip() {
    this.tooltipVisible = false;
    this.tooltipTarget = null;
    this.tooltipMessage = '';
  }


  onProfileChange = (e: any) => {
    const selectedProfile = e.value;
    if (selectedProfile === 3) { // Perfil 'Consulta'
      this.showAlias = false;
      this.showPhone = false;

      // Limpiar valores si no son visibles
      this.formData.NombreSocial = '';
      this.formData.TelefonoCelular = '';
    } else {
      this.showAlias = true;
      this.showPhone = true;
    }
  };


  onPlGroupValueChanged(e: any) {
    //notify('selection changed '+e.value);
    // this.showMe = !this.showMe;

  }
  onValueChanged(e: any): void {
    notify('selection changed');
    this.showMe = !this.showMe;
  }

  async ValidarEmail(e: any) {

    this.displayPassword = true;
    notify('Visible');

  }


  GetUsuarioByCorreoResult(correo: string) {
    return this.usuarioservice.GetUsuarioGeneralByCorreo(correo).toPromise();
  }

  async GetUsuarioByCorreo(correo: string) {
    try {

      
      this.ValorServicio = await this.GetUsuarioByCorreoResult(correo);
      return this.ValorServicio.UserID;

    } catch (error) {
      this.logger.log('error en GetUsuarioByCorreo: ' + error);
      //console.error('[error en GetUsuarioByCorreo]: ' + error);
      return null; // Devuelve null en caso de error
    }
  }

  async verificarCorreoExistente(correo: string) {

    const usuarioExistente = await this.GetUsuarioByCorreo(correo);

    if (usuarioExistente) {
      // Si existen datos, muestra un mensaje
      console.warn('El correo ya existe. No se puede crear otra cuenta con el mismo correo.');
      return false; // Indica que no debe continuar
    } else {
      // Si no hay datos, puede continuar
      return true; // Indica que puede continuar
    }
  }

  async onSubmit(e: any) {


    e.preventDefault();
    const { CorreoElectronico, Contrasena } = this.formData;
    this.loading = true;

    this.usuario.Nombre = this.formData.Nombre;
    this.usuario.CorreoElectronico = this.formData.CorreoElectronico;
    this.usuario.Contrasena = this.formData.Contrasena;
    this.usuario.PerfilUsuarioID = this.formData.PerfilUsuarioID;
//    this.usuario.CodigoBienvenida = this.formData.CodigoBienvenida;
    this.usuario.CodigoBienvenida = '';//Se deja vacio temporalmente

    this.usuario.TelefonoCelular = this.formData.TelefonoCelular;
    this.usuario.NombreSocial = this.formData.NombreSocial;
    


    let aceptaPoliticas = localStorage.getItem('AcceptPolicy');

    if(aceptaPoliticas === "false")
      this.usuario.AceptaPoliticas = false;
    else  if( aceptaPoliticas === "true")
      this.usuario.AceptaPoliticas = true;
    else
    this.usuario.AceptaPoliticas = false;



    const puedeContinuar = await this.verificarCorreoExistente(this.usuario.CorreoElectronico);

    if (puedeContinuar) {

      if ( this.usuario.AceptaPoliticas === true) {

        localStorage.setItem('codigobienvenida', this.usuario.CodigoBienvenida);





       if(this.usuario.CodigoBienvenida !== '')
       {

        if(await this.ValidarCodigoDescuento(this.usuario.CodigoBienvenida) === true)

          {
            this.usuarioservice.PostUsuarioGeneral(this.usuario).subscribe(
              async result => {
    
                if (result.OperacionExitosa == false) { //Si falla la creación de la cuenta de usuario
                  this.loading = false;
                  notify(result.Mensaje, 'error', 2000);
                }
                else // La creación de la cuenta fue satisfactoria
                {
                  let usuarioCreado: any;
    
                  if (result.Result.UserID !== undefined) {
                    usuarioCreado = Number(result.Result.UserID);
    
                    const codigoBienvenida = localStorage.getItem('codigobienvenida');
  
                 
                    this.descuentos.UserID = usuarioCreado;
    
    
                    if (codigoBienvenida != undefined && codigoBienvenida != "undefined" && codigoBienvenida!=='') {
                      // Ejecuta solo si tiene un valor válido
                      await this.AplicarCodigoDescuento();
                    }
    

                    /* Codigo para Mostrar mensaje y envio de correo de activacion (Se comentarea por ahora)
                    this.displayNextStepActivateAccount = true;
                    this.generarEnlaceWhatsApp();
    
                    let emailParametros = new EmailParametros();
                    emailParametros.PublicacionID = 0;
                    emailParametros.Tipo = 3; //Correo de Activacion Cuenta
                    emailParametros.Url = environment.urlFront;
                    emailParametros.UserID = usuarioCreado;
    
                    //Enviar Correo de Bienvenida Creación de Publicación
                    await this.emailservice.EnviarEmailActivacionCuenta(emailParametros).subscribe({
                      next: (result) => {
                        if (result.Exito) {
                          this.logger.log("Correo enviado correctamente");
                        } else {
                          //notify(result.Mensaje, "error", 8000);
                          this.logger.log(result.Mensaje);
                          this.logger.log(result.Error);
                        }
                      },
                      error: (error) => {
                        //notify("Error al enviar correo: " + error.message, "error", 8000);
                        this.logger.log("Error al enviar correo:"+ error);
//                        console.error("Error al enviar correo:", error);
  //                      console.error("Error al enviar correo:", error.message);
    
                      }
                    });
    
*/

            notify("¡Registro Exitoso!  Tu cuenta ha sido creada exitosamente... Ya puedes iniciar sesión", 'success', 5000);
            this.router.navigateByUrl('/login-form');

                  }
    
                }
              },
              error => {
                this.error = error.error.mensaje;
                this.loading = false;
                notify(this.error, 'error', 2000);
    
              }
            );
  
          }
        
    
       }
       else
       {

        this.usuarioservice.PostUsuarioGeneral(this.usuario).subscribe(
          async result => {

            if (result.OperacionExitosa == false) { //Si falla la creación de la cuenta de usuario
              this.loading = false;
              notify(result.Mensaje, 'error', 2000);
            }
            else // La creación de la cuenta fue satisfactoria
            {
              let usuarioCreado: any;

              if (result.Result.UserID !== undefined) {
                usuarioCreado = Number(result.Result.UserID);

               /* Se comentarea este código temporalmente hasta que se valide que el envio de correo funcione correctamente
                this.displayNextStepActivateAccount = true;
                this.generarEnlaceWhatsApp();

                let emailParametros = new EmailParametros();
                emailParametros.PublicacionID = 0;
                emailParametros.Tipo = 3; //Correo de Activacion Cuenta
                emailParametros.Url = environment.urlFront;
                emailParametros.UserID = usuarioCreado;

                //Enviar Correo de Bienvenida Creación de Publicación
                await this.emailservice.EnviarEmailActivacionCuenta(emailParametros).subscribe({
                  next: (result) => {
                    if (result.Exito) {
                      this.logger.log("Correo enviado correctamente");
                    } else {
                      //notify(result.Mensaje, "error", 8000);
                      this.logger.log(result.Mensaje);
                      this.logger.log(result.Error);
                    }
                  },
                  error: (error) => {
                    //notify("Error al enviar correo: " + error.message, "error", 8000);
                    this.logger.log("Error al enviar correo:"+ error);
  //                  console.error("Error al enviar correo:", error);
    //                console.error("Error al enviar correo:", error.message);

                  }
                });

                */

                notify("¡Registro Exitoso!  Tu cuenta ha sido creada exitosamente... Ya puedes iniciar sesión", 'success', 5000);
                this.router.navigateByUrl('/login-form');
              }

            }
          },
          error => {
            this.error = error.error.mensaje;
            this.loading = false;
            notify(this.error, 'error', 2000);

          }
        );
      }
       }
        
      else {
        this.loading = false;
        notify("Para continuar, debe aceptar la política de tratamiento de datos", 'warning', 3000);
      }

    }
    else {
      this.loading = false;
      // Detén el proceso si el correo ya existe
      notify("No se puede crear el usuario porque el correo ya está en uso.", 'error', 2000);
      console.warn('No se puede crear el usuario porque el correo ya está en uso.');
    }
  }







  validatePassword = (e: any) => {
    const password = e.value;
    const regex = /^(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*()\-_=+{};:,<.>]).{8,12}$/;
    return regex.test(password);
  };

  /*  confirmPassword = (e: any) => {
      const confirmPassword = e.value;
      return confirmPassword === this.contrasena;
    };*/

  confirmPassword = (e: { value: string }) => {
    return e.value === this.formData.Contrasena;
  }

  open(content: any) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-title' });
  }

  cerrarModales() {
    this.modalService.dismissAll();
  }


  public async  ValidarCodigoDescuento(codigobienvenida: string) {

    //Se valida si el codigo de Descuento Existe y si está Vigente o No este siendo utilizado

    let codigopublicion: any;

    codigopublicion = await this.GetDescuentoBienvenidaById(codigobienvenida, 0);

    if (codigopublicion != null) //Existe el Codigo de Descuento
    {
      this.descuentos.DescuentoCodeID = codigopublicion.DescuentoCodeID;
      this.descuentos.FechaGeneracion = codigopublicion.FechaGeneracion;
      this.descuentos.FechaVencimiento = codigopublicion.FechaVencimiento;
      this.descuentos.Utilizado = codigopublicion.Utilizado;
      this.descuentos.UserID = 0;
      this.descuentos.PendienteUso = codigopublicion.PendienteUso == null ? false : codigopublicion.PendienteUso;
      this.descuentos.TipoUtilizacion = codigopublicion.TipoUtilizacion;


      // Obtener la fecha actual
      const fechaActual = new Date();
      // Convertir FechaVencimiento a tipo Date
      const fechaVencimiento = new Date(this.descuentos.FechaVencimiento);


      //Si el codigo existe, validar si no ha sido utilizado o haya expirado

      if (this.descuentos.Utilizado === true) {
        notify({
          message: 'El Código de Descuento ya fue utilizado. Ingrese un nuevo Código',
          type: 'error',
          displayTime: 6000,
          position: {
            my: 'center bottom',
            at: 'center bottom',
            of: window,
            collision: 'fit',
            offset: '0 -50',  // Desplazar la segunda notificación hacia arriba para que no se solapen.
          }
        });
        this.descuentos.DescuentoCodeID = '';
        this.loading = false;
        return false;
      }
      else if (fechaActual > fechaVencimiento) {
        notify({
          message: 'El Código de Descuento ya ha expirado. Ingrese un nuevo Código',
          type: 'error',
          displayTime: 6000,
          position: {
            my: 'center bottom',
            at: 'center bottom',
            of: window,
            collision: 'fit',
            offset: '0 -50',  // Desplazar la segunda notificación hacia arriba para que no se solapen.
          }
        });

        this.loading = false;
        return false;
      }
      else if (this.descuentos.PendienteUso === true && this.descuentos.TipoUtilizacion === 'Bienvenida') {
        notify({
          message: 'El Código de Descuento de Bienvenida ya ha sido asignado. Ingrese un nuevo Código. Si reqiere uno, solicitelo a través de soporte@vecinoemprende.com',
          type: 'error',
          displayTime: 10000,
          position: {
            my: 'center bottom',
            at: 'center bottom',
            of: window,
            collision: 'fit',
            offset: '0 -50',  // Desplazar la segunda notificación hacia arriba para que no se solapen.
          }
        });
        this.descuentos.DescuentoCodeID = '';
        this.loading = false;
        return false;
      }
      else {
        //Se aplica el Codigo de Descuento y se registra el Pago correspondiente y se emite mensaje de Aceptación del Codigo

return true;

      }

    }
    else {
      notify({
        message: 'El Código de Descuento No Existe. Solicitelo enviando a través de soporte@vecinoemprende.com',
        type: 'error',
        displayTime: 6000,
        position: {
          my: 'center bottom',
          at: 'center bottom',
          of: window,
          collision: 'fit',
          offset: '0 -50',  // Desplazar la segunda notificación hacia arriba para que no se solapen.
        }
      });
      this.descuentos.DescuentoCodeID = '';
      this.loading = false;
      return false;
    }

  }

  public async AplicarCodigoDescuento() {

    await this.pagosservice.PutDescuentoBienvenidaById(this.descuentos).subscribe(async result => {

      if (result.success == false) {
        notify("Error al actualizar descuento. Comuníquese con soporte@vecinoemprende.com y con gusto validaremos su código de Bienvenida", "error", 4000);
        this.descuentos.DescuentoCodeID = '';
      }
      else {

        notify({
          message: 'Ud ha ingresado un código de descuento válido, que puede redimir en la creación de su primera Publicación.',
          type: 'success',
          displayTime: 10000,
          position: {
            my: 'center bottom',
            at: 'center bottom',
            of: window,
            collision: 'fit',
            offset: '0 -50',  // Desplazar la segunda notificación hacia arriba para que no se solapen.
          }
        });
        localStorage.clear();
        this.loading = false;
        //  this.router.navigateByUrl('/login-form');
        //Se establece el código de descuento en Pendiente Uso
      }

    },
      error => {
//        console.error(error);
        this.logger.log(error);
      }
    );
  }


  async GetDescuentoBienvenidaById(id: string, userid: number) {
    try {
      this.ValorServicio = await this.GetDescuentoBienvenidaByIdResult(id, userid);
      return this.ValorServicio;
    }
    catch (error) {
//      console.error('[error en GetDescuento] : ' + error);
      this.logger.log('[error en GetDescuento] : ' + error);
    }
  }


  async GetDescuentoBienvenidaByIdResult(id: string, userid: number) {
    return this.pagosservice.GetDescuentoBienvenidaById(id, 0).toPromise();
  }

  onCheckboxClick(e: any) {
    const isChecked = e.value; // `true` si está marcado, `false` si no.


    // Aquí puedes añadir cualquier lógica adicional
    if (isChecked) {

      
      this.AcceptPolicy = true;
      localStorage.setItem('AcceptPolicy', this.AcceptPolicy.toString());


    } else {

      this.AcceptPolicy = false;
      localStorage.setItem('AcceptPolicy', this.AcceptPolicy.toString());

    }


  }


  generarEnlaceWhatsApp() {
    
    const mensaje = `Solicito mi enlace de activación. Mi correo es ${this.usuario.CorreoElectronico}`;
    this.whatsappLink = `https://wa.me/573105853786?text=${encodeURIComponent(mensaje)}`;
  }

}

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    DxFormModule,
    DxLoadIndicatorModule
  ],
  declarations: [CreateAccountFormComponent],
  exports: [CreateAccountFormComponent]
})
export class CreateAccountFormModule { }
