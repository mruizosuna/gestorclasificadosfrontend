import { CommonModule } from '@angular/common';
import { Component, NgModule, OnInit } from '@angular/core';
import { ActivatedRoute, Router, RouterModule } from '@angular/router';
import { DxFormModule } from 'devextreme-angular/ui/form';
import { DxLoadIndicatorModule } from 'devextreme-angular/ui/load-indicator';
import notify from 'devextreme/ui/notify';
import { CookieService } from 'ngx-cookie-service';
import { AutenticacionUsuario, UsuarioData, UsuarioLogin } from '../../../../models/usuario.model'
import { UsuarioService } from '../../../../services/usuarios.service';
import { AuthService } from '../../services';
const defaultPath = '/home';
import jwt_decode from 'jwt-decode';
import { environment } from '../../../../environments/environment';
import { LoggerService } from '../../../../services/logger.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss'],
  providers: [UsuarioService]
})


export class LoginFormComponent implements OnInit {

  pubParam: string | null = null;
  showMe: boolean = true;
  loading = false;
  formData: any = {};

  error: any; //Devolución de mensaje de error


  //usuario: UsuarioAutenticacion = new UsuarioAutenticacion();
  usuario: AutenticacionUsuario = new AutenticacionUsuario();
  ValorServicio: any;
  nombrePerfil: string = '';
  urlBlob: string;

  constructor(private usuarioservice: UsuarioService, private authService: AuthService, private router: Router, private cookieService: CookieService, private route: ActivatedRoute, private logger: LoggerService) {

    this.urlBlob = environment.urlBlob;
  }



  ngOnInit(): void {
    // Obtener el parámetro 'pub' de la URL
    this.route.queryParams.subscribe(params => {
      this.pubParam = params['pub']; // Aquí capturamos el valor de 'pub'

      if (this.pubParam) {
        // Si 'pub' tiene un valor (no es null ni vacío)
        // let idPublicacion = this.extraerNumeroCompleto(Number( this.pubParam));

        // Almacenar en localStorage
        localStorage.setItem('pubId', this.pubParam.toString());



        const token = localStorage.getItem('token');
        if (token) {
          // Autenticar usuario automáticamente
        
          this.authService.loggedIn = true;


          if (localStorage.getItem('pubId')) {
            this.router.navigate(['/busqueda/Anuncios']); // Redirigir si pubId existe en localStorage
          } else {
            this.router.navigate([defaultPath]); // Redirigir a un path por defecto si no hay pubId
          }


        }
      } else {
        // Si no hay valor para 'pub', no se hace nada
        this.logger.log('No se ha recibido el parámetro pub');
      }
    });
  }

  extraerNumeroCompleto(numero: number) {
    // Convierte el número a una cadena y elimina los primeros 8 caracteres
    return parseInt(numero.toString().substring(8));
  }


  async onSubmit(e: any) {
    e.preventDefault();
    const { email, password } = this.formData;
    this.loading = true;

    //Aqui se instanciara el servicio de autenticación para determinar si se permite el ingreso del usuario
    this.usuario.correo = email;
    this.usuario.password = password;


    await this.getUsuario(this.usuario);

  }

  onCreateAccountClick = () => {
    this.router.navigate(['/create-account']);
  }

  /*
    //Nueva versión de autenticación
    public async getUsuario(usuario: AutenticacionUsuario) {
  
  
  
  
  
      this.usuarioservice.PostUsuarioAutorizado(usuario).subscribe(
        async (result: { success: boolean, message: string, data: any }) => {
  
  
          if (result.success == false) { //Si falla la autenticación
            this.loading = false;
            notify(result.message, 'error', 2000);
          }
          else // La autenticación fue satisfactoria
          {
  
            this.cookieService.set('token', result.data);
            notify(result.message, 'success', 2000);
  
  
            //Se obtiene la información del JWT
            const token = result.data; // tu token JWT
            const decodedToken = jwt_decode(token) as DecodedToken;
           // this.logger.log(decodedToken); // Output: 1234567890
  
  
            const sub = decodedToken.sub; // id del usuario
            const correo = decodedToken.correo; // correo del usuario
            const perfil = decodedToken.perfil; // perfil del usuario
            const username = decodedToken.username; // nombre del usuario
            localStorage.setItem('usuarioId', sub);
  
            //Se obtiene el nombre del perfil
            this.nombrePerfil = await this.GetUsuarioPerfilById(Number(perfil));
  
            localStorage.setItem('emailLogged', correo);
  
            localStorage.setItem('usuarioSesionado', username);
            localStorage.setItem('perfilUsuario', perfil);
            localStorage.setItem('perfilUsuarioNombre', this.nombrePerfil);
  
  
  
  
            this.authService.loggedIn = true;
            this.router.navigate([defaultPath]);
          }
  
          
        },
        (error: { error: { mensaje: any; }; }) => {
  
         // this.error = error.error.mensaje;
         
       
         // console.error(error);
         console.error(JSON.stringify(error));
          this.loading = false;
          //notify(this.error, 'error', 2000);
          notify(JSON.stringify(error), 'error', 2000);
          
        }
      );
    }*/

  public async getUsuario(usuario: AutenticacionUsuario) {

    let usuarioLogin = new UsuarioLogin();
    usuarioLogin.correoElectronico = usuario.correo;
    usuarioLogin.contrasena = usuario.password;



    this.usuarioservice.PostLoginUsuario(usuarioLogin).subscribe(
      async (result: UsuarioData) => {


        if (result.OperacionExitosa == false) { //Si falla la autenticación
          this.loading = false;
          notify(result.Mensaje, 'error', 5000);
        }
        else // La autenticación fue satisfactoria
        {

          notify(result.Mensaje, 'success', 2000);

          const usuarioGlobal = new UsuarioData();
          usuarioGlobal.UserID = result.UserID;
          usuarioGlobal.CorreoElectronico = result.CorreoElectronico;
          usuarioGlobal.DescripcionPerfil = result.DescripcionPerfil;
          usuarioGlobal.Nombre = result.Nombre;
          usuarioGlobal.PerfilUsuarioID = result.PerfilUsuarioID;
          usuarioGlobal.ImagenUsuario = result.ImagenUsuario;


          if (result.JWTToken) {

            localStorage.setItem('token', result.JWTToken); // Guardar token
          }



          localStorage.setItem('userid', usuarioGlobal.UserID.toString());
          localStorage.setItem('emailLogged', usuarioGlobal.CorreoElectronico);
          localStorage.setItem('usuarioSesionado', usuarioGlobal.Nombre);
          localStorage.setItem('userImageUrl', (usuarioGlobal.ImagenUsuario === null || usuarioGlobal.ImagenUsuario === undefined || usuarioGlobal.ImagenUsuario === '') ? this.urlBlob + 'SinImagen.png' : this.urlBlob + usuarioGlobal.ImagenUsuario);

          localStorage.setItem('perfilUsuario', usuarioGlobal.PerfilUsuarioID.toString());
          localStorage.setItem('perfilUsuarioNombre', usuarioGlobal.DescripcionPerfil);


          if (result.NumeroDocumento === null || result.NumeroDocumento === '' || result.TelefonoCelular === null || result.TelefonoCelular === '' || result.TipoDocumentoID === null || result.TipoDocumentoID === 0) {
            localStorage.setItem('perfilActualizado', "false");
          }
          else {
            localStorage.setItem('perfilActualizado', "true");
          }

          this.authService.loggedIn = true;



          if (localStorage.getItem('pubId')) {
            this.router.navigate(['/busqueda/Anuncios']); // Redirigir si pubId existe en localStorage
          } else {
            this.router.navigate([defaultPath]); // Redirigir a un path por defecto si no hay pubId
          }


        }


      },
      (error: { error: { mensaje: any; }; }) => {

        // this.error = error.error.mensaje;


        // console.error(error);
        console.error(JSON.stringify(error));
        this.loading = false;
        //notify(this.error, 'error', 2000);
        notify(JSON.stringify(error), 'error', 2000);

      }
    );
  }



}

interface DecodedToken {
  sub: string;
  correo: string;
  perfil: string;
  username: string
}




@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    DxFormModule,
    DxLoadIndicatorModule
  ],
  declarations: [LoginFormComponent],
  exports: [LoginFormComponent]
})





export class LoginFormModule { }
