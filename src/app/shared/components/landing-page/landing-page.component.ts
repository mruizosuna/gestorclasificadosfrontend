import { AfterViewInit, Component, ElementRef, ViewChild } from '@angular/core';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.scss']
})
export class LandingPageComponent implements AfterViewInit {

  @ViewChild('menu', { static: true }) menu: ElementRef;
  @ViewChild('carousel', { static: true }) carousel: ElementRef;

  currentIndex = 0;
  productIndex = 0;

  ngAfterViewInit() {
   // this.setupMenuToggle();
    this.setupCarousel();
    this.setupProductAnimation();
  }

  setupMenuToggle() {
    const menu = this.menu.nativeElement;
    // Lógica de toggle del menú
  }

  setupCarousel() {
   
    const images = this.carousel.nativeElement.querySelectorAll('img');
    const indicators = this.carousel.nativeElement.querySelectorAll('.carousel-indicators div');

    const showImage = (index: number) => {
      images[this.currentIndex].classList.remove('active');
      indicators[this.currentIndex].classList.remove('active');
      this.currentIndex = index;
      images[this.currentIndex].classList.add('active');
      indicators[this.currentIndex].classList.add('active');
    };

    const showNextImage = () => {
      let nextIndex = (this.currentIndex + 1) % images.length;
      showImage(nextIndex);
    };

    const showPrevImage = () => {
      let prevIndex = (this.currentIndex - 1 + images.length) % images.length;
      showImage(prevIndex);
    };

    this.carousel.nativeElement.querySelector('#next').addEventListener('click', showNextImage);
    this.carousel.nativeElement.querySelector('#prev').addEventListener('click', showPrevImage);

    setInterval(showNextImage, 3000); // Cambiar imagen cada 3 segundos
  }

  setupProductAnimation() {
    const productLists = document.querySelectorAll('.product-list');

    const showNextProducts = () => {
      productLists.forEach(list => {
        const products = list.querySelectorAll('.product, .service');
        products.forEach((product, i) => {
          // Convierte 'product' a 'HTMLElement'
          const productElement = product as HTMLElement;
          productElement.style.display = (i >= this.productIndex && i < this.productIndex + 4) ? 'block' : 'none';
        });
      });
      this.productIndex = (this.productIndex + 4) % 8;
    };
    

    showNextProducts();
    setInterval(showNextProducts, 3000);
  }

}
