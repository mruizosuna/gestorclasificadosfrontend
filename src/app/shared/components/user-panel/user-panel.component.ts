import { Component, NgModule, Input, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DxListModule } from 'devextreme-angular/ui/list';
import { DxContextMenuModule } from 'devextreme-angular/ui/context-menu';

@Component({
  selector: 'app-user-panel',
  templateUrl: 'user-panel.component.html',
  styleUrls: ['./user-panel.component.scss']
})

export class UserPanelComponent implements OnInit {
  @Input()
  menuItems: any;

  @Input()
  menuMode: string = '';

  @Input()
  email : string = '';

  userImageUrl: string = ''; // URL de la imagen dinámica del usuario

  constructor() {
    
  }

  ngOnInit(): void {
    //this.email = localStorage.getItem('emailLogged') ?? '';
    this.email = localStorage.getItem('usuarioSesionado') ?? '';
    
    this.userImageUrl = localStorage.getItem('userImageUrl') || 'default-image-url.png';
    
  }
}





@NgModule({
  imports: [
    DxListModule,
    DxContextMenuModule,
    CommonModule
  ],
  declarations: [ UserPanelComponent ],
  exports: [ UserPanelComponent ]
})
export class UserPanelModule { }


