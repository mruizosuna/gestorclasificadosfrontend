import { Component, NgModule, Output, Input, EventEmitter, ViewChild, ElementRef, AfterViewInit, OnDestroy, OnInit } from '@angular/core';
import { DxTreeViewModule, DxTreeViewComponent } from 'devextreme-angular/ui/tree-view';
import { navigation } from '../../../app-navigation';

import * as events from 'devextreme/events';
import { AuthService } from '../../services';
import { Router } from '@angular/router';

@Component({
  selector: 'app-side-navigation-menu',
  templateUrl: './side-navigation-menu.component.html',
  styleUrls: ['./side-navigation-menu.component.scss']
})
export class SideNavigationMenuComponent implements AfterViewInit, OnDestroy, OnInit {
  navigation = navigation;

  @ViewChild(DxTreeViewComponent, { static: true })
  menu: DxTreeViewComponent | undefined;

  @Output()
  selectedItemChanged = new EventEmitter<string>();

  @Output()
  openMenu = new EventEmitter<any>();

  private _selectedItem: String = '';
  @Input()
  set selectedItem(value: String) {
    this._selectedItem = value;
    if (!this.menu?.instance) {
      return;
    }

    this.menu.instance.selectItem(value);
  }

  private _items: any[] | undefined;

  ngOnInit() {

    //Se incorpora esta validación para obtener las opciones de menú acordes al rol del usuario sesionado
    let perfilSesionado = localStorage.getItem("perfilUsuarioNombre") ?? '';

    if (perfilSesionado !== undefined && perfilSesionado !== null) {
      if (!this._items) {
        this._items = navigation.map((item) => {
          if (item.path && !(/^\//.test(item.path))) {
            item.path = `/${item.path}`;
          }
          if (item.roles !== undefined) {
            if (item.roles.includes(perfilSesionado)) {
              return { ...item, expanded: !this._compactMode };
            }
          }
          return undefined;
        });
      }
    }


    this._items = this._items?.filter(function (dato) {
      return dato != undefined
    });

    return this._items;

  }

  get items() {

    if (!this._items) {
      this._items = navigation.map((item) => {
        if (item.path && !(/^\//.test(item.path))) {
          item.path = `/${item.path}`;
        }
        return { ...item, expanded: !this._compactMode }
      });
    }
    return this._items;
  }

  private _compactMode = false;
  @Input()
  get compactMode() {
    return this._compactMode;
  }
  set compactMode(val) {
    this._compactMode = val;

    if (!this.menu?.instance) {
      return;
    }

    if (val) {
      this.menu.instance.collapseAll();
    } else {
      this.menu.instance.expandItem(this._selectedItem);
    }
  }

  constructor(private elementRef: ElementRef, private authService: AuthService, private router: Router) { }



  onItemClick(event: any) {

    if (event.node.itemData.action === 'logout')
      this.authService.logOut();
    else
      this.selectedItemChanged.emit(event);


  }

  ngAfterViewInit() {
    events.on(this.elementRef.nativeElement, 'dxclick', (e: any) => {
      this.openMenu.next(e);
    });
  }

  ngOnDestroy() {
    events.off(this.elementRef.nativeElement, 'dxclick');
  }
}

@NgModule({
  imports: [DxTreeViewModule],
  declarations: [SideNavigationMenuComponent],
  exports: [SideNavigationMenuComponent]
})
export class SideNavigationMenuModule { }
