import { CommonModule } from '@angular/common';
import { Component, NgModule, OnInit } from '@angular/core';
import { ActivatedRoute, Router, RouterModule } from '@angular/router';
import { DxFormModule } from 'devextreme-angular/ui/form';
import { DxLoadIndicatorModule } from 'devextreme-angular/ui/load-indicator';
import notify from 'devextreme/ui/notify';
import { AuthService } from '../../services';


@Component({
  selector: 'app-change-passsword-form',
  templateUrl: './change-password-form.component.html'
})
export class ChangePasswordFormComponent implements OnInit {
  loading = false;
  formData: any = {};
  recoveryCode: string[] = [];
  userId: number = 0;
  recovery: string = '';

  constructor(private authService: AuthService, private router: Router, private route: ActivatedRoute) { }

 

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.recoveryCode = params.get('recoveryCode')?.split('_') ?? [];
  
      if (this.recoveryCode.length === 2) { //Tiene las dos partes
        localStorage.setItem("recovery", this.recoveryCode[0]); // Corrección
        localStorage.setItem("userId", this.recoveryCode[1]);   // Corrección
      }
    });
  }
  
  async onSubmit(e: any) {
    e.preventDefault();
    const { password } = this.formData;
    this.loading = true;
  
    // Se asegura de obtener los valores correctos desde localStorage
    this.recovery = localStorage.getItem("recovery") ?? ''; 
    this.userId = Number(localStorage.getItem("userId")) || 0;
  
  
    const result = await this.authService.changePassword(password, this.recovery, this.userId);
    this.loading = false;
  
    if (result.isOk) {
      notify("Se ha cambiado la contraseña correctamente", 'success', 3000);
      this.router.navigate(['/login-form']);
    } else {
      notify(result.message, 'error', 2000);
    }
  }
  

  confirmPassword = (e: { value: string }) => {
    return e.value === this.formData.password;
  }
}
@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    DxFormModule,
    DxLoadIndicatorModule
  ],
  declarations: [ChangePasswordFormComponent],
  exports: [ChangePasswordFormComponent]
})
export class ChangePasswordFormModule { }
