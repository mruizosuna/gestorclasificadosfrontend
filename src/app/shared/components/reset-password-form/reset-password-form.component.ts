import { CommonModule } from '@angular/common';
import { Component, NgModule } from '@angular/core';
import { Router, RouterModule } from '@angular/router';
import { DxFormModule } from 'devextreme-angular/ui/form';
import { DxLoadIndicatorModule } from 'devextreme-angular/ui/load-indicator';
import notify from 'devextreme/ui/notify';
import { AuthService } from '../../services';



const notificationText = 'Hemos enviado un correo para crear su contraseña. Chequee su correo en la bandejade entrada, o en la bandeja de correo no deseado.';

@Component({
  selector: 'app-reset-password-form',
  templateUrl: './reset-password-form.component.html',
  styleUrls: ['./reset-password-form.component.scss']
})
export class ResetPasswordFormComponent {
  loading = false;
  formData: any = {};

  constructor(private authService: AuthService, private router: Router) { }

  async onSubmit(e: any) {
    e.preventDefault();
    const { email } = this.formData;
    this.loading = true;

    

 // Generar una clave automática (puedes usar cualquier lógica que necesites)
 const generatedKey = this.generateRandomKey();

 // Envío del correo electrónico
 const subject = 'Nueva clave de restablecimiento de contraseña';
 const message = `Tu nueva clave es: ${generatedKey}`;



    const result = await this.authService.resetPassword(email);
    this.loading = false;

    if (result.isOk) {
      this.router.navigate(['/login-form']);
      notify(notificationText, 'success', 5000);
    } else {
      notify(result.message, 'error', 2000);
    }
  }

  generateRandomKey(): string {
    // Lógica para generar una clave aleatoria
    // Puedes adaptar esta función según tus necesidades
    return Math.random().toString(36).slice(-8);
  }
}
@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    DxFormModule,
    DxLoadIndicatorModule
  ],
  declarations: [ResetPasswordFormComponent],
  exports: [ResetPasswordFormComponent]
})
export class ResetPasswordFormModule { }
