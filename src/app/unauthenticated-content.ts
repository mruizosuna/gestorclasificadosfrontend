import { CommonModule } from '@angular/common';
import { Component, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SingleCardModule } from 'src/app/layouts';
import { Router } from '@angular/router';

@Component({
  selector: 'app-unauthenticated-content',
  template: `
    <app-single-card [title]="title" [description]="description">
      <router-outlet></router-outlet>
    </app-single-card>
  `,
  styles: [`
    :host {
      width: 100%;
      height: 100%;
    }
  `]
})
export class UnauthenticatedContentComponent {

  constructor(private router: Router) { }

  get title() {
    const path = this.router.url.split('/')[1];
    switch (path) {
      case 'login-form': return 'Autenticación';
      case 'reset-password': return 'Cambiar Contraseña';
      case 'create-account': return 'Creación de Cuenta';
      case 'change-password': return 'Cambiar Contraseña';
      default :  return 'Autenticación';
    }
  }

  get description() {
    const path = this.router.url.split('/')[1];
    switch (path) {
      case 'reset-password': return 'Por favor, ingrese la dirección de correo electrónico que utilizó para registrarse, y se le enviará un link para cambiar su contraseña';
      default : return 'Por favor, ingrese la dirección de correo electrónico que utilizó para registrarse, y se le enviará un link para cambiar su contraseña';
    }
  }
}
@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    SingleCardModule,
  ],
  declarations: [UnauthenticatedContentComponent],
  exports: [UnauthenticatedContentComponent]
})
export class UnauthenticatedContentModule { }
