export interface ScreenSizes {
    xs: boolean;
    sm: boolean;
    md: boolean;
    lg: boolean;
    xl: boolean;
  }
  
  export class ScreenSizes implements ScreenSizes {
    xs = false;
    sm = false;
    md = false;
    lg = false;
    xl = false;
  }
  