import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Injectable, NgModule, NgZone } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';


import { SideNavOuterToolbarModule, SideNavInnerToolbarModule, SingleCardModule } from './layouts';
import { FooterModule, ResetPasswordFormModule, CreateAccountFormModule, ChangePasswordFormModule, LoginFormModule } from './shared/components';
import { AuthService, ScreenService, AppInfoService } from './shared/services';
import { UnauthenticatedContentModule } from './unauthenticated-content';
import { AppRoutingModule } from './app-routing.module';
import { DevExtremeModule, DxButtonModule, DxCheckBoxModule, DxDataGridModule, DxDropDownButtonModule, DxFileUploaderModule, DxFormModule, DxListModule, DxSchedulerModule, DxSelectBoxModule, DxValidatorModule } from 'devextreme-angular';
import { FormsModule } from '@angular/forms';
import { JwtInterceptorInterceptor } from './jwt-interceptor.interceptor';
import { CookieService } from 'ngx-cookie-service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ZonasComponent } from './pages/zonas/zonas.component';
import { AlcancesComponent } from './pages/alcances/alcances.component';
import { CategoriasPrincipalesComponent } from './pages/categoriasprincipales/categoriasprincipales.component';
import { DxChartModule } from 'devextreme-angular';
import { DxPieChartModule } from 'devextreme-angular';
import { CarouselModule } from 'ngx-bootstrap/carousel';

import { LayoutModule } from '@angular/cdk/layout';
import { AppComponent } from './app.component';

import { PerfilComponent } from './pages/perfil/perfil.component';

import { PeriodosPublicacionComponent } from './pages/periodospublicacion/periodospublicacion.component';
import { TiposPublicacionComponent } from './pages/tipospublicacion/tipospublicacion.component';
import { ConjuntosComponent } from './pages/conjuntos/conjuntos.component';
import { ValorPublicacionComponent } from './pages/valorpublicacion/valorpublicacion.component';
import { UsuariosComponent } from './pages/usuarios/usuarios.component';
import { PublicacionesComponent } from './pages/publicaciones/publicaciones.component';
import { CategoriasSecundariasComponent } from './pages/categoriassecundarias/categoriassecundarias.component';
import { BusquedaComponent } from './pages/busqueda/busqueda.component';
import { PagosComponent } from './pages/pagos/pagos.component';
import { PagopayuComponent } from './pages/pagopayu/pagopayu.component';

import { ReactiveFormsModule } from '@angular/forms';
import { VideoItemComponent } from './pages/video-item/video-item.component';

import { ConfirmacionPayuComponent } from './pages/confirmacion-payu/confirmacion-payu.component';
import { LandingPageComponent } from './shared/components/landing-page/landing-page.component';

import { registerLocaleData } from '@angular/common';
import localeEsCo from '@angular/common/locales/es-CO';
import { AyudaComponent } from './pages/ayuda/ayuda.component';
import { DestacadosComponent } from './pages/destacados/destacados.component';
import { DetallepublicacionComponent } from './pages/detallepublicacion/detallepublicacion.component';

registerLocaleData(localeEsCo);

@NgModule({
  declarations: [
    AppComponent,
    ZonasComponent,
    AlcancesComponent,
    CategoriasPrincipalesComponent,
    CategoriasSecundariasComponent,
    BusquedaComponent,
    ValorPublicacionComponent,
    ConjuntosComponent,
    PeriodosPublicacionComponent,
    TiposPublicacionComponent,
    UsuariosComponent,
    PublicacionesComponent,
    PagosComponent,
    AppComponent,
    PerfilComponent,
    PagopayuComponent,
    VideoItemComponent,
    ConfirmacionPayuComponent,
    LandingPageComponent,
    AyudaComponent,
    DestacadosComponent,
    DetallepublicacionComponent,


  ],
  imports: [
    BrowserModule,
    SideNavOuterToolbarModule,
    SideNavInnerToolbarModule,
    SingleCardModule,
    FooterModule,
    ResetPasswordFormModule,
    CreateAccountFormModule,
    ChangePasswordFormModule,
    LoginFormModule,
    UnauthenticatedContentModule,
    AppRoutingModule,
    HttpClientModule,
    DxDataGridModule,
    FormsModule,
    DxDropDownButtonModule,
    DxValidatorModule,
    DxFormModule,
    DxSelectBoxModule,
    DxButtonModule,
    DevExtremeModule,
    NgbModule,
    DxSelectBoxModule,
    DxListModule,
    DxSchedulerModule,
    DxFileUploaderModule,
    NgbModule, // Agregar NgbModule
    DxChartModule,
    DxPieChartModule,
    LayoutModule,
    ReactiveFormsModule,
    FormsModule,


    CarouselModule.forRoot()
  ],
  providers: [
    AuthService,
    AppInfoService,
    CookieService,
    ScreenService,
    BreakpointObserver,

    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptorInterceptor,
      multi: true
    }
  ],

  bootstrap: [AppComponent]
})
export class AppModule { }
