import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

if (environment.production) {
  enableProdMode();
}
/*
platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.error(err));
*/

  const currentHostname = window.location.href;
  if ((currentHostname === environment.urlOrigen1 || currentHostname === environment.urlOrigen2) && window.location.pathname === '/') {
    window.location.replace(environment.urlRedireccion1);
  } else {
    if (environment.production) {
      enableProdMode();
    }
    platformBrowserDynamic().bootstrapModule(AppModule).catch(err => console.error(err));
  }