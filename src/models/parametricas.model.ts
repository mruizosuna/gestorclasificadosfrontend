export class Localidad 
{
public id : number = 0;
public localidad : string = '';
}

export class TipoAccion 
{
public id : number = 0;
public accion : string = '';
}

export class TipoServicio
{
public id : number = 0;
public servicio : string = '';
}


export class Perfil 
{
public id : number = 0;
public nombre : string = '';
}

export class Generic
{
    public id : number = 0;
    public text : string = '';
    
}