import { JsonObjectExpression } from "typescript";

//19/01/2023 Edgar Ruiz - se agrega class Caso
export class Pagos {
    public PagoID: number = 0;
    public PublicacionID: number = 0;
    public FechaPago: string = "";
    public MontoaPagar: number = 0;
    public Descuentos: number = 0;
    public DescuentoCodeID: string = "";
    public MontoPagado: number = 0;
    public NumeroReferencia: string = "";
    public TipoPago: string | null;
    public RazonSocial: string = "";
    public TipoDocumentoID: number = 0;
    public TipoDocumentoIDs: string = '';
    public NumeroDocumento: string = "";
    public TelefonoCelular: string = "";
    public Email : string = "";
    public Estado : number = 0;


}

export class ListaPagos {
    public PagoID: number = 0;
    public Titulo: string = "";
    public FechaPago: string = "";
    public MontoaPagar: number = 0;
    public Descuentos: number = 0;
    public DescuentoCodeID: string = "";
    public MontoPagado: number = 0;
    public NumeroReferencia: string = "";
    public TipoPago: string = "";
    public RazonSocial: string = "";
    public TipoDocumentoID: number = 0;
    public NumeroDocumento: string = "";
    public TelefonoCelular: string = "";
    public Email : string = "";
    public Estado : boolean = true;


}
