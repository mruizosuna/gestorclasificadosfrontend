//19/01/2023 Edgar Ruiz - se agrega class Caso
export class EmailParametros {
    public PublicacionID: number = 0;
    public Tipo: number = 0;
    public Url : string = '';
    public UserID : number = 0;
    public Consecutivo : string = '';
}

export class EmailContactenos {
    public Nombre: string = '';
    public Email: string = '';
    public Asunto : string = '';
    public Contenido : string = '';
}
