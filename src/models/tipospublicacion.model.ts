import { JsonObjectExpression } from "typescript";

//19/01/2023 Edgar Ruiz - se agrega class Caso
export class TiposPublicacion {
    public TipoPublicacionID: number = 0;
    public NombreTipoPublicacion: string = '';
    public DescripcionTipoPublicacion: string = '';

}

export class ListaTipoPublicacion {
    public TipoPublicacionID: number = 0;
    public NombreTipoPublicacion: string = '';
    public DescripcionTipoPublicacion: string = '';
    public Texto : boolean = false;
    public Imagenes : number = 0;
    public Video : number = 0;
}
