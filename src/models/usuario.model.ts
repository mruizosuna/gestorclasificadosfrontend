export class UsuarioAutenticacion
{
    public correo : string = '';
    public password : string = '';
}

export class AutenticacionUsuario   //Para la nueva forma de autenticación
{   
    public correo : string = '';
    public password : string = '';
}

export class Usuario
{
    public id : number = 0;
    public activo : boolean = true;
    public nombres : string = '';
    public apellidos : string = '';
    public nombresApellidos : string = '';
    public correo : string = '';
    public password : string = '';
    public telefono : string = '';
    public usuarioCrea : string = '';
    public usuarioModifica: string = '';
    public localidad_id: number;
    

}

export class UsuarioGestion
{
    public id : number = 0;
    public activo : boolean = true;
    public nombres : string = '';
    public apellidos : string = '';
    public nombresApellidos : string = '';
    public correo : string = '';
    public password : string = '';
    public telefono : string = '';
    public usuarioCrea : string = '';
    public usuarioModifica: string = '';
    public contrato: string = '';
    public perfilId : number = 0;
    public localidadId : number = 0;

}

export class LocalidadUsuario
{
    public localidadId : number = 0;
    public usuarioId : number = 0;

}

/*22-12-2022 Wilmer Mogollón -- se agrega class PerfilUsuario*/

export class PerfilUsuario
{
    public perfilId : number = 0;
    public usuarioId : number = 0;

}

//27-12--2022 Edgar Ruiz -- se agrega class DispositivoUsuario
export class DispositivoUsuario
{
    public id : number = 0;
    public activo : boolean = true;
    public telefono : string = '';
    public propiedad : string = '';
    public nserie : string = '';
    public usuarioId : number = 0;
    public usuarioCrea : string = '';
    public usuarioModifica: string = '';
    public usuarioName : string = '';
    public direccion : string = '';
    public lat : string = '';
    public lng : string = '';



}

//11/01/2023 Edgar Ruiz - se agrega class Disponibilidad Usuario
export class DisponibilidadUsuario
{
    public id : number = 0;
    public estado : boolean = true;
    public startDate : Date | undefined ;
    public horaInicio : string = '' ;
    public endDate : Date | undefined  ;
    public horaFin : string = '' ;
    public localidadId : number = 0;
    public usuarioId : number = 0;
    public text : string = '' ;
    public usuarioCrea : string = '';
    public usuarioModifica: string = '';


}



export class UsuarioLocalizacion
{
    public id : number = 0;
    public lat : string = '' ;
    public long : string = '' ;
    public dispositivoId : number = 0;    
    public usuarioCrea : string = '';
    public usuarioModifica: string = '';
  


}
//Nueva clase para autenticar usuario
export class UsuarioLogin   //Para la nueva forma de autenticación
{   
    public correoElectronico : string = '';
    public contrasena : string = '';
    public token : string = '';
}

export class UsuarioData
{
    public UserID : number = 0; 
    public Nombre : string = "";
    public CorreoElectronico : string = "";
    public Contrasena : string = "";
    public ConfirmedContrasena: string = "";
    public PerfilUsuarioID : number = 0;
    public Activo : boolean = true;
    public RazonSocial : string = '';
    public NombreSocial : string = '';
    public TipoDocumentoID : number = 0;
    public TipoDocumentoIDS : string = '';
    
    public NumeroDocumento : string = '';
    public TelefonoCelular : string = '';
    public ImagenUsuario : string ;
    public Imagen64: string = '';
    public DescripcionPerfil : string = "";
    public Mensaje : String = "";
    public OperacionExitosa : boolean = false;

    public CodigoBienvenida : string = "";
    public AceptaPoliticas: boolean = false;

    public EsCuentaVerificada : boolean = false;

    public UrlActivacionCuenta : string = "";

    public JWTToken : any = "";
}
