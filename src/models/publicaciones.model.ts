import { JsonObjectExpression } from "typescript";

//19/01/2023 Edgar Ruiz - se agrega class Caso
export class Publicaciones {

	public PublicacionID: number;
	public UsuarioID: number;
	public Nombre: string;
	public FechaCreacion: string;
	public FechaVencimiento: string;
	public TipoPublicacionID: number;
	public NombreTipoPublicacion: string;
	public AlcanceID: number;
	public NombreAlcance: string;
	public PeriodoID: number;
	public NombrePeriodo: string;
	public Titulo: string;
	public Descripcion: string;
	public EstadoAprobacionID: number;
	public NombreEstado: string;
	public PagoRealizado: boolean = false;
	public NombreZonaLocalidad: string;
	public NombreConjunto: string;
	public NombreSubcategoria: string;
	public NombreCategoria: string;
	public RutaImagen: string;
	public Puntuacion: number;
	public PuntuacionPromedio: number;
	public Comentario: string;
	public RutaVideo: string;
	public ValorProductoServicio: number = 0;
	public MostrarValorProductoServicio: boolean = false;
	public CorreoElectronico: string = '';
	public RazonSocial : string = '';
	public NombreSocial : string = '';
	public TelefonoCelular : string = '';
	public ImagenUsuario : string = '';


	public DiasHastaVencimiento: number = 0;
	public ProximoAVencer: boolean = false;
	public AplicoDescuento : boolean = false;
	public TipoDescuento : string = '';

	public RangoPrecios : boolean = false;
	public ValorProductoServicioDesde : number = 0;
	public ValorProductoServicioHasta : number = 0;
	public MostrarUrlSitioWeb : boolean = false ;
	public UrlSitioWeb : string = '';
	public UrlPublicacion : string = '';



}


export class PublicacionRenovacionDetalle {

	public RenovacionID : number;
	public PublicacionID: number;
	public UsuarioID: number;
	public FechaRenovacion: string;
	public FechaVencimiento: string;
	public TipoPublicacionID: number;
	public AlcanceID: number;
	public NombreAlcance: string;
	public PeriodoID: number;
	public EstadoRenovacion: boolean = false;
	public PagoRenovacionRealizado: boolean = false;
	public AplicoDescuento: boolean = false;
	public EsRenovacion: boolean = false;
	public EsCambioPlan: boolean = false;
	public AplicoInmediato: boolean = false;
	public AplicoFinVigencia: boolean = false;
	public TipoDescuento: string = '';

}

export class PublicacionesHistoricas {
	public HistorialID : number;
	public PublicacionID: number;
	public UsuarioID: number;
	public Nombre: string;
	public FechaCreacion: string;
	public FechaVencimiento: string;
	public TipoPublicacionID: number;
	public AlcanceID: number;
	public PeriodoID: number;
	public Titulo: string;
	public Descripcion: string;
	public EstadoAprobacionID: number;
	public PagoRealizado: boolean = false;
	public ValorProductoServicio: number = 0;
	public MostrarValorProductoServicio: boolean = false;
	public AplicoDescuento : boolean = false;
	public TipoDescuento : string = '';
	public MostrarUrlSitioWeb : boolean = false ;
	public UrlSitioWeb : string = '';
	public RangoPrecios : boolean = false;
	public ValorProductoServicioDesde : number = 0;
	public ValorProductoServicioHasta : number = 0;
	public UrlPublicacion : string = '';
	public Renovado : boolean = false;
	public UltimaRenovacion : string = '';
	public UrlRenovacion : string = '';
	public FechaRegistro : string = '';


}


export class PublicacionesBuscador {
	public PublicacionID: number;
	public UsuarioID: number;
	public Nombre: string;
	public FechaCreacion: string;
	public FechaVencimiento: string;
	public TipoPublicacionID: number;
	public NombreTipoPublicacion: string;
	public AlcanceID: number;
	public NombreAlcance: string;
	public PeriodoID: number;
	public NombrePeriodo: string;
	public Titulo: string;
	public Descripcion: string;
	public EstadoAprobacionID: number;
	public NombreEstado: string;
	public PagoRealizado: number;
	public NombreZonaLocalidad: string;
	public NombreConjunto: string;
	public NombreSubcategoria: string;
	public NombreCategoria: string;
	public RutaImagen: string;
	public Puntuacion: number;
	public PuntuacionPromedio: number;
	public Comentario: string;
	public RutaVideo: string;
	public ValorProductoServicio: number = 0;
	public MostrarValorProductoServicio: boolean = false;
	public CorreoElectronico: string = '';
	public RazonSocial : string = '';
	public NombreSocial : string = '';
	public TelefonoCelular : string = '';
	public ImagenUsuario : string = '';

	public RangoPrecios : boolean = false;
	public ValorProductoServicioDesde : number = 0;
	public ValorProductoServicioHasta : number = 0;
	public MostrarUrlSitioWeb : boolean = false;
	public UrlSitioWeb : string = '';
	public UrlPublicacion : string = '';

}


export class BuscadordePublicaciones {
	CategoriaID: number = 0;
	SubCategoriaID: number = 0;
	ZonasLocalidadID: number = 0;
	ConjuntoID: number = 0;
	PalabrasClave: string | null;
	IDPublicacion : number ;
	ValorProductoServicioDesde : number | null;
	ValorProductoServicioHasta : number | null;
}

export class Vencimientos {
	Fecha: string  = '';
}

export class DuplicacionPublicacion {
	public PublicacionID: number = 0;
}