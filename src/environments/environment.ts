// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,

  apiUrl: 'https://localhost:5001', //Desarrollo
  apiUrlGestor: 'https://localhost:44381', //Desarrollo
  //apiUrlGestor: 'http://backendclasificados.veconparking.com' , //Pruebas
  token: '97221cdc42-8661-4ab9-a04e-51785baa88da39',
  //urlBlob : "http://backendclasificados.veconparking.com/Imagenes/" ,
  urlBlob : "https://localhost:44381/Imagenes/" ,

apiKey: "4Vj8eK4rloUd272L48hsrarnUA", 
apiLogin : "pRRXKOl8ikMmt9u",
llavePublica : "PK789916aBq2quqa6BD9jL5qCh",
merchantId: "508029",
accountId : "512321",
referenceCode : "PagoPayU-0000",
currency : "COP",
mode : "1",
//responseUrl : "https://localhost:4200/#/confirmacion-payu" ,
//confirmationUrl : "https://localhost:4200/#/confirmacion-payu" ,
responseUrl : "https://localhost:44381/publicaciones/confirmacion-payu" ,
confirmationUrl : "https://localhost:44381/publicaciones/confirmacion-payu" ,

ProduccionPasarela: "https://sandbox.checkout.payulatam.com/ppp-web-gateway-payu/",
test : 1,
urlFront : 'https://activacion.vecinoemprende.com/',
urlDetallePublicacion: 'https://landing.vecinoemprende.com?pub=',
urlRedireccion1 : 'https://www.landing.vecinoemprende.com/',
urlRedireccion2 : 'https://landing.vecinoemprende.com/',

urlOrigen1 : 'https://localhost:4200/',
urlOrigen2 : 'https://localhost:4200/'



};

//Comentario Prueba ERUIZ 18 Marzo 2024 543pm

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
