function toggleMenu() {
    var x = document.getElementById("menu");
    if (x.className === "menu") {
        x.className += " responsive";
    } else {
        x.className = "menu";
    }
}

// Carrusel
const images = document.querySelectorAll('.carousel img');
const indicators = document.querySelectorAll('.carousel-indicators div');
let currentIndex = 0;

function showImage(index) {
    images[currentIndex].classList.remove('active');
    indicators[currentIndex].classList.remove('active');
    currentIndex = index;
    images[currentIndex].classList.add('active');
    indicators[currentIndex].classList.add('active');
}

function showNextImage() {
    let nextIndex = (currentIndex + 1) % images.length;
    showImage(nextIndex);
}

function showPrevImage() {
    let prevIndex = (currentIndex - 1 + images.length) % images.length;
    showImage(prevIndex);
}

document.getElementById('next').addEventListener('click', showNextImage);
document.getElementById('prev').addEventListener('click', showPrevImage);

setInterval(showNextImage, 3000); // Cambiar imagen cada 3 segundos

// Animación para las secciones de productos y servicios
const productLists = document.querySelectorAll('.product-list');
let productIndex = 0;

function showNextProducts() {
    productLists.forEach(list => {
        const products = list.querySelectorAll('.product, .service');
        for (let i = 0; i < products.length; i++) {
            products[i].style.display = 'none';
        }
        for (let i = productIndex; i < productIndex + 4; i++) {
            if (products[i]) {
                products[i].style.display = 'block';
            }
        }
    });
    productIndex = (productIndex + 4) % 8;
}

showNextProducts();
setInterval(showNextProducts, 3000);
